import arrow
from loguru import logger


def get_now_str_kst():
    return arrow.now(tz='Asia/Seoul').format('YYYYMMDD_HHmmss_SSS') + '_KST'


def get_now_str():
    return arrow.now(tz='Asia/Seoul').format('YYYYMMDD_HHmmss_SSS')


def get_y_m_d_path(now_str: str):
    y, m, d = now_str[0:4], now_str[4:6], now_str[6:8]
    # logger.debug(f'y={y}|m={m}|d={d}')
    return f'{y}/{m}/{d}'


if __name__ == '__main__':
    logger.info(f'get_now_str()={get_now_str()}')
    logger.info(f'get_now_str_kst()={get_now_str_kst()}')

    now = get_now_str_kst()
    logger.info(f'get_y_m_d_path(now)={get_y_m_d_path(now)}')
    logger.info(f'get_y_m_d_path(12345678)={get_y_m_d_path("12345678")}')

# 20200403_142900_996_KST
