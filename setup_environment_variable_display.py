import signal

import os


def setup_environment_variable_display():
    os.environ['SVGA_VGPU10'] = '0'
    os.environ['DISPLAY'] = ':0.0'
