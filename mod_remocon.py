import sys
from enum import Enum

from PyQt5.QtCore import QObject, pyqtSignal, QTimer, pyqtSlot
from PyQt5.QtNetwork import QLocalSocket
from PyQt5.QtWidgets import QApplication
from loguru import logger

from mod_beep import beep
from mod_device_screen import set_resolution
from mod_lg_tv import LGTV
from mod_setup_signal import setup_signal

SOCKPATH = "/var/run/lirc/lircd"


class RemoconKeyCode(Enum):
    KEY_IBK_POWER = 'KEY_IBK_POWER'
    KEY_IBK_BACK = 'KEY_IBK_BACK'
    KEY_IBK_UP = 'KEY_IBK_UP'
    KEY_IBK_DOWN = 'KEY_IBK_DOWN'
    KEY_IBK_LEFT = 'KEY_IBK_LEFT'
    KEY_IBK_RIGHT = 'KEY_IBK_RIGHT'
    KEY_IBK_OK = 'KEY_IBK_OK'
    KEY_IBK_F1 = 'KEY_IBK_F1'
    KEY_IBK_F2 = 'KEY_IBK_F2'
    KEY_IBK_F3 = 'KEY_IBK_F3'
    KEY_IBK_M1 = 'KEY_IBK_M1'
    KEY_IBK_M2 = 'KEY_IBK_M2'
    KEY_IBK_M3 = 'KEY_IBK_M3'


ALL_REMOCON_VALUES = [i.value for i in RemoconKeyCode]


class QRemocon(QObject):
    key_pressed = pyqtSignal(RemoconKeyCode, int)

    def __init__(self):
        super(QRemocon, self).__init__()
        self.m3_counter = 0
        self.client = QLocalSocket()
        self.client.readyRead.connect(self.read_data_from_server)
        self.client.disconnected.connect(self.try_connect)
        self.try_connect()

    @pyqtSlot()
    def try_connect(self):
        if self.client.isOpen():
            self.client.close()

        self.client.connectToServer(SOCKPATH)
        if self.client.isOpen():
            logger.info("connected")
        else:
            logger.info("fail to connect retry")
            QTimer.singleShot(2000, self.try_connect)

    def reset_m3_key(self):
        logger.info("m3 counter reset")
        self.m3_counter = 0

    def read_data_from_server(self):
        # logger.debug(f"read")
        data = self.client.readAll().data().decode("utf-8")
        # logger.debug(f"data={data}")

        # noinspection PyBroadException
        try:
            code = data.strip().split()[-2]

            if code in ALL_REMOCON_VALUES:
                r = RemoconKeyCode(code)
                if r == RemoconKeyCode.KEY_IBK_M3:
                    self.m3_counter += 1
                logger.info(f"r={r}|self.m3_counter={self.m3_counter}")
                self.key_pressed.emit(r, self.m3_counter)
                QTimer.singleShot(10000, self.reset_m3_key)
            else:
                logger.error(f"UNKNOWN CODE : code={code}")
        except Exception:
            logger.exception("fail decode code")


def main():
    setup_signal()
    qapp = QApplication(sys.argv)

    def keypress_log(c: RemoconKeyCode, n_m3counter: int):
        beep()
        logger.info(f"keypress_log={c}|n_m3counter={n_m3counter}")

    def process_power(c: RemoconKeyCode, n_m3counter: int):

        if c == RemoconKeyCode.KEY_IBK_POWER:
            try:
                LGTV().toggle()
            except:
                logger.error("fail toggle tv")

            set_resolution()

    c = QRemocon()
    c.key_pressed.connect(keypress_log)
    c.key_pressed.connect(process_power)

    qapp.exec()


if __name__ == '__main__':
    main()
    logger.info([i.value for i in RemoconKeyCode])

    r = RemoconKeyCode('KEY_IBK_POWER')
    logger.info(r)
