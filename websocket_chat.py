import asyncio
import json
import multiprocessing
import ssl
import sys
import time

import websockets
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import (QApplication, QWidget, QVBoxLayout, QGroupBox, QLineEdit, QListView)

import aioprocessing

# MIXER_URI = 'wss://chat.mixer.com:443'
MIXER_URI = 'wss://echo.websocket.org'


class WebsocketThread(QThread):
    on_mesage = pyqtSignal(object)

    def __init__(self):
        super(WebsocketThread, self).__init__()
        self.outputqueue = None

    def run(self):
        while True:
            time.sleep(.01)
            message = self.outputqueue.get()
            try:
                self.on_mesage.emit(message)
            except Exception as e:
                print(f'Exception in updatemaingui: {e}')


async def consumer_handler(websocket, outputqueue):
    while True:
        try:
            message = await websocket.recv()
            print(f'Received message: {message}')
            outputqueue.put(message)
        except Exception as e:
            print(f'Consumer exception: {e}')


async def producer_handler(websocket, inputqueue):
    while True:
        message = await inputqueue.coro_get()
        await websocket.send(message)
        await asyncio.sleep(.05)


async def websockets_handler(outputqueue, inputqueue):
    print('Starting connection...')
    async with websockets.connect(MIXER_URI) as websocket:
        await websocket.send('{"type": "method", "method": "auth", "arguments": [160788], "id": 0}')
        print(f'Connected to websocket: {websocket}')
        auth_result = await websocket.recv()
        print(auth_result)
        consumer_task = asyncio.ensure_future(consumer_handler(websocket, outputqueue))
        producer_task = asyncio.ensure_future(producer_handler(websocket, inputqueue))
        done, pending = await asyncio.wait(
            [consumer_task, producer_task],
            return_when=asyncio.FIRST_COMPLETED, )
        for task in pending:
            task.cancel()


def start_websockets(outputqueue, inputqueue):
    loop = asyncio.get_event_loop()
    loop.run_until_complete(websockets_handler(outputqueue, inputqueue))
    # from fastapi import FastAPI
    #
    # app = FastAPI()
    #
    # @app.get("/")
    # def read_root():
    #     return {"Hello": "World"}
    #
    # @app.get("/items/{item_id}")
    # def read_item(item_id: int, q: str = None):
    #     return {"item_id": item_id, "q": q}
    #
    # uvicorn.run(app, host='0.0.0.0', port=8000)


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.inputqueue = aioprocessing.AioQueue()
        self.outputqueue = aioprocessing.AioQueue()
        self.websocket_process = None
        self.setup_websockets()
        self.layout = QVBoxLayout()
        self.groupBox = QGroupBox(self)
        self.view = QListView()
        self.view.setWordWrap(True)
        self.model = QStandardItemModel()
        self.view.setModel(self.model)
        self.layout.addWidget(self.view)
        self.lineEdit = QLineEdit(self)
        self.layout.addWidget(self.lineEdit)
        self.lineEdit.returnPressed.connect(self.send_message)
        self.model.appendRow(QStandardItem('Welcome to MixerChat!'))
        self.setLayout(self.layout)

    def setup_websockets(self):
        wsh = WebsocketThread()
        wsh.outputqueue = self.outputqueue
        wsh.on_mesage.connect(self.updategui)
        wsh.start()
        self.websocket_process = multiprocessing.Process(target=start_websockets,
                                                         args=(self.outputqueue, self.inputqueue))
        self.websocket_process.start()

    def closeEvent(self, event):
        if self.websocket_process:
            self.websocket_process.kill()
        event.accept()

    @pyqtSlot()
    def send_message(self):
        msg = f'{str(self.lineEdit.text())}'
        if len(msg) > 0:
            self.model.appendRow(QStandardItem(f'>> {msg}'))
            print(f'Sent message: {msg}')
            self.lineEdit.clear()
            self.inputqueue.put(msg)

    @pyqtSlot(object)
    def updategui(self, msg):
        try:
            msg = json.loads(msg)
            if msg['type'] == 'event' and msg['event'] == 'ChatMessage':
                data = msg['data']
                message = ''
                for m in data['message']['message']:
                    message += m['text']
                message = message.encode('ascii', 'ignore').decode('ascii')
                if len(message) > 0:
                    self.model.appendRow(QStandardItem(f'{data["user_name"]}[{data["user_level"]}] << {message}'))
            else:
                pass
            if self.model.rowCount() > 100:
                self.model.removeRows(0, 1)
            self.view.scrollToBottom()
        except Exception as e:
            print(f'Parse error: {e}')


if __name__ == '__main__':
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
