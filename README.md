sudo cp ~/settop/app/settop_net/ntpdate_linux/ntpdate /usr/local/bin/
sudo chmod +x /usr/local/bin/ntpdate
sudo ntpdate 172.18.141.23

nano ~/0.sh
PYTHONPATH=~/settop/app/settop_net python ~/settop/app/settop_net/app.py

------------------------

nano ~/settop/app/settop/bash_script/app.sh

beep -f 5000 -l 50

source ~/settop/app/settop/bash_script/boot.sh

bash -i ~/0.sh


-------------------------------

#set -e




# TODO

stb_play_stat 구현

# 프로토콜 문서

https://docs.google.com/document/d/1vrsXBBNJ75hlf3wT8KRHI3r8FhCmikNw_E7P3eAo9Fc/edit

# MAC only
    screeninfo의 경우 아래 프로그램 설치해야함
    conda install -y cython -c conda-forge 

    pip install git+https://github.com/kivy/pyobjus.git

# TODO

- 다운로드가 free 인 요일 정할수 있게...
- 다운로드 시간 대역이 PM 06 ~ AM 06 인 경우에 대한 표현은? 어떻게 정리할것인가? 
- 버전 업데이트 테스트 할것 

# 설치

- miniconda 설치

- conda create -y -n settop python=3.7 cython ipython uvicorn psutil pyqt pillow pyzmq netifaces openpyxl pyserial pandas pycurl -c conda-forge

- conda install -y          python=3.7 cython ipython uvicorn psutil pyqt pillow pyzmq netifaces openpyxl pyserial pandas pycurl -c conda-forge
- pip install -r requirements.txt
- pip install -e .

# 실행

- 서버

    stbs

- 클라이언트

        stbc --id 0 --url ws://127.0.0.1:8000/ws/client
        stbc --id abcde
        stbc --help

- 샘플

    stb_sample

- 프로토콜 협의

    stb_protocol
    
# 웹 서버 접속

<http://127.0.0.1:8000>

# 개념

- id : 각 클라이언트의 고유 값
- uuid : 접속에 대한 고유 값 (동일한 아이디로 접속해도 고유한 UUID를 가짐)
- group : id의 집합
- 서버 설정
    - group_dict 의 키 : group 이름
    - group_dict 의 값 :
        - default_schedule : 해당 그룹의 스케쥴 dictionary를 리턴
        - ids : 해당 그룹에 속하는 id들

                {
                    "group_dict": {
                        "a": {
                            "default_schedule": {
                                "a": 1,
                                "sample": "abc"
                            },
                            "ids": [
                                "0",
                                "1"
                            ]
                        },
                        "b": {
                            "default_schedule": {
                                "b": 1
                            },
                            "ids": [
                                "3",
                                "2"
                            ]
                        }
                    }
                } 

# 예시

- 현재 서버 접속

    http http://127.0.0.1:8000/connections
    
        {
            "id_group_dict": {
                "0": "a",
                "1": "a",
                "2": "b",
                "3": "b"
            },
            "id_uuids_dict": {
                "0": [
                    "8daf3fba-4c5c-415a-842f-69d6c2a75724"
                ]
            },
            "uuids": [
                "8daf3fba-4c5c-415a-842f-69d6c2a75724"
            ]
        }

- 현재 서버 설정

    http http://127.0.0.1:8000/config
    
        {
            "group_dict": {
                "a": {
                    "default_schedule": {
                        "a": 1,
                        "sample": "abc"
                    },
                    "ids": [
                        "0",
                        "1"
                    ]
                },
                "b": {
                    "default_schedule": {
                        "b": 1
                    },
                    "ids": [
                        "3",
                        "2"
                    ]
                }
            }
        }
        
# 명령보내기

    --------------------------------------------------------------------------------
    show_all_samples
    --------------------------------------------------------------------------------
    {"command": {"cmd": "stb_on", "date": "2019-06-11T01:04:22.860358+09:00"}}
    {"command": {"cmd": "stb_off", "date": "2019-06-11T01:04:22.860621+09:00"}}
    {"command": {"cmd": "stb_reboot", "date": "2019-06-11T01:04:22.860720+09:00"}}
    {"command": {"cmd": "stb_auto_on_off", "date": "2019-06-11T01:04:22.860823+09:00", "start_time": "09:00:00", "end_time": "18:00:00"}}
    {"command": {"cmd": "stb_download_time", "date": "2019-06-11T01:04:22.860982+09:00", "start_time": "00:00:00", "end_time": "05:00:00"}}
    {"command": {"cmd": "stb_stop_download", "date": "2019-06-11T01:04:22.861125+09:00"}}
    {"command": {"cmd": "tv_on", "date": "2019-06-11T01:04:22.861247+09:00"}}
    {"command": {"cmd": "tv_off", "date": "2019-06-11T01:04:22.861341+09:00"}}
    {"command": {"cmd": "stb_play_stat", "date": "2019-06-11T01:04:22.861419+09:00"}}
    {"command": {"cmd": "stb_screen_image", "date": "2019-06-11T01:04:22.861495+09:00"}}
    {"command": {"cmd": "stb_log", "date": "2019-06-11T01:04:22.861572+09:00", "url": "ftp://a.com/sub_path"}}
    {"command": {"cmd": "stb_update_sw", "date": "2019-06-11T01:04:22.861651+09:00", "url": "ftp://a.com/sub_path"}}
    {"command": {"cmd": "stb_live_on", "date": "2019-06-11T01:04:22.861733+09:00", "url": "udp://1.2.3.4:8089"}}
    {"command": {"cmd": "stb_live_off", "date": "2019-06-11T01:04:22.861811+09:00"}}
    {"command": {"cmd": "stb_add_schedule", "date": "2019-06-11T01:04:22.861927+09:00", "ftps": ["ftp://a.com/sub_path", "ftp://b.com/sub_path"], "schedule": {"date": "2019-06-11", "url": "http://127.0.0.1:8000/schedule/a"}}}
    {"command": {"cmd": "keep_alive", "date": "2019-06-11T01:04:22.862127+09:00"}}
    --------------------------------------------------------------------------------
    == FOR LINUX/MAC ==
    echo '{"command": {"cmd": "stb_on", "date": "2019-06-11T01:04:22.860358+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_off", "date": "2019-06-11T01:04:22.860621+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_reboot", "date": "2019-06-11T01:04:22.860720+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_auto_on_off", "date": "2019-06-11T01:04:22.860823+09:00", "start_time": "09:00:00", "end_time": "18:00:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_download_time", "date": "2019-06-11T01:04:22.860982+09:00", "start_time": "00:00:00", "end_time": "05:00:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_stop_download", "date": "2019-06-11T01:04:22.861125+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "tv_on", "date": "2019-06-11T01:04:22.861247+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "tv_off", "date": "2019-06-11T01:04:22.861341+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_play_stat", "date": "2019-06-11T01:04:22.861419+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_screen_image", "date": "2019-06-11T01:04:22.861495+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_log", "date": "2019-06-11T01:04:22.861572+09:00", "url": "ftp://a.com/sub_path"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_update_sw", "date": "2019-06-11T01:04:22.861651+09:00", "url": "ftp://a.com/sub_path"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_live_on", "date": "2019-06-11T01:04:22.861733+09:00", "url": "udp://1.2.3.4:8089"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_live_off", "date": "2019-06-11T01:04:22.861811+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "stb_add_schedule", "date": "2019-06-11T01:04:22.861927+09:00", "ftps": ["ftp://a.com/sub_path", "ftp://b.com/sub_path"], "schedule": {"date": "2019-06-11", "url": "http://127.0.0.1:8000/schedule/a"}}}' | http post http://127.0.0.1:8000/send_cmd_all
    echo '{"command": {"cmd": "keep_alive", "date": "2019-06-11T01:04:22.862127+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_all
    --------------------------------------------------------------------------------
    echo '{"command": {"cmd": "stb_on", "date": "2019-06-11T01:04:22.860358+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_off", "date": "2019-06-11T01:04:22.860621+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_reboot", "date": "2019-06-11T01:04:22.860720+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_auto_on_off", "date": "2019-06-11T01:04:22.860823+09:00", "start_time": "09:00:00", "end_time": "18:00:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_download_time", "date": "2019-06-11T01:04:22.860982+09:00", "start_time": "00:00:00", "end_time": "05:00:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_stop_download", "date": "2019-06-11T01:04:22.861125+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "tv_on", "date": "2019-06-11T01:04:22.861247+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "tv_off", "date": "2019-06-11T01:04:22.861341+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_play_stat", "date": "2019-06-11T01:04:22.861419+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_screen_image", "date": "2019-06-11T01:04:22.861495+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_log", "date": "2019-06-11T01:04:22.861572+09:00", "url": "ftp://a.com/sub_path"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_update_sw", "date": "2019-06-11T01:04:22.861651+09:00", "url": "ftp://a.com/sub_path"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_live_on", "date": "2019-06-11T01:04:22.861733+09:00", "url": "udp://1.2.3.4:8089"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_live_off", "date": "2019-06-11T01:04:22.861811+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "stb_add_schedule", "date": "2019-06-11T01:04:22.861927+09:00", "ftps": ["ftp://a.com/sub_path", "ftp://b.com/sub_path"], "schedule": {"date": "2019-06-11", "url": "http://127.0.0.1:8000/schedule/a"}}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo '{"command": {"cmd": "keep_alive", "date": "2019-06-11T01:04:22.862127+09:00"}}' | http post http://127.0.0.1:8000/send_cmd_by_group/a
    == FOR WINDOWS ==
    echo {"command": {"cmd": "stb_on", "date": "2019-06-11T01:04:22.860358+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_off", "date": "2019-06-11T01:04:22.860621+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_reboot", "date": "2019-06-11T01:04:22.860720+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_auto_on_off", "date": "2019-06-11T01:04:22.860823+09:00", "start_time": "09:00:00", "end_time": "18:00:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_download_time", "date": "2019-06-11T01:04:22.860982+09:00", "start_time": "00:00:00", "end_time": "05:00:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_stop_download", "date": "2019-06-11T01:04:22.861125+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "tv_on", "date": "2019-06-11T01:04:22.861247+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "tv_off", "date": "2019-06-11T01:04:22.861341+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_play_stat", "date": "2019-06-11T01:04:22.861419+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_screen_image", "date": "2019-06-11T01:04:22.861495+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_log", "date": "2019-06-11T01:04:22.861572+09:00", "url": "ftp://a.com/sub_path"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_update_sw", "date": "2019-06-11T01:04:22.861651+09:00", "url": "ftp://a.com/sub_path"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_live_on", "date": "2019-06-11T01:04:22.861733+09:00", "url": "udp://1.2.3.4:8089"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_live_off", "date": "2019-06-11T01:04:22.861811+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "stb_add_schedule", "date": "2019-06-11T01:04:22.861927+09:00", "ftps": ["ftp://a.com/sub_path", "ftp://b.com/sub_path"], "schedule": {"date": "2019-06-11", "url": "http://127.0.0.1:8000/schedule/a"}}} | http post http://127.0.0.1:8000/send_cmd_all
    echo {"command": {"cmd": "keep_alive", "date": "2019-06-11T01:04:22.862127+09:00"}} | http post http://127.0.0.1:8000/send_cmd_all
    --------------------------------------------------------------------------------
    echo {"command": {"cmd": "stb_on", "date": "2019-06-11T01:04:22.860358+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_off", "date": "2019-06-11T01:04:22.860621+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_reboot", "date": "2019-06-11T01:04:22.860720+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_auto_on_off", "date": "2019-06-11T01:04:22.860823+09:00", "start_time": "09:00:00", "end_time": "18:00:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_download_time", "date": "2019-06-11T01:04:22.860982+09:00", "start_time": "00:00:00", "end_time": "05:00:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_stop_download", "date": "2019-06-11T01:04:22.861125+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "tv_on", "date": "2019-06-11T01:04:22.861247+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "tv_off", "date": "2019-06-11T01:04:22.861341+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_play_stat", "date": "2019-06-11T01:04:22.861419+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_screen_image", "date": "2019-06-11T01:04:22.861495+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_log", "date": "2019-06-11T01:04:22.861572+09:00", "url": "ftp://a.com/sub_path"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_update_sw", "date": "2019-06-11T01:04:22.861651+09:00", "url": "ftp://a.com/sub_path"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_live_on", "date": "2019-06-11T01:04:22.861733+09:00", "url": "udp://1.2.3.4:8089"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_live_off", "date": "2019-06-11T01:04:22.861811+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "stb_add_schedule", "date": "2019-06-11T01:04:22.861927+09:00", "ftps": ["ftp://a.com/sub_path", "ftp://b.com/sub_path"], "schedule": {"date": "2019-06-11", "url": "http://127.0.0.1:8000/schedule/a"}}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    echo {"command": {"cmd": "keep_alive", "date": "2019-06-11T01:04:22.862127+09:00"}} | http post http://127.0.0.1:8000/send_cmd_by_group/a
    == BOTH ==
    http http://127.0.0.1:8000/connections
    http http://127.0.0.1:8000/config
    http post http://127.0.0.1:8000/config @sample.json
