import json

from loguru import logger

from models import ResponsePlayStat

s = '{"cmd": "stb_play_stat", "date": "2020-04-03T18:24:29.158399+09:00", "result": true, "reason": "", "id": "2c:94:64:02:e1:5c", "dirSizes": {}, "cpu": 23.3, "memory": {"used": 1024, "total": 4096}, "disk": {"used": 1073741824, "total": 10737418240}, "tv": [{"status": "none", "title": "", "screen_no": 0, "tv_on": true, "resolution": {"width": 1920, "height": 1080}, "volume": 100, "controlMethod": "serial"}]}'

d = json.loads(s)

json.dumps(d,ensure_ascii=False,indent=2)

logger.debug(json.dumps(d,ensure_ascii=False,indent=2))

logger.debug(ResponsePlayStat(id='x').json(ensure_ascii=False,indent=2))
