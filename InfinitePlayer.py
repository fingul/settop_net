import platform
import sys
from pathlib import Path

import vlc
from PyQt5 import QtWidgets, QtGui
from PyQt5.Qt import PYQT_VERSION_STR
from PyQt5.QtCore import QMargins
from PyQt5.QtWidgets import QApplication, QWidget
from loguru import logger

# def set_media_window(videoframe: QWidget, mediaplayer: vlc.MediaPlayer):
from mod_setup_signal import setup_signal
from setup_environment_variable_display import setup_environment_variable_display
from ticket_server import set_background_color


class IPlayer(object):

    def __init__(self, repeat: bool = True) -> None:

        # os.environ["PULSE_SINK"] = "0"

        super().__init__()
        # Create a basic vlc instance
        # self.instance = vlc.Instance()

        # REPEAT : https://stackoverflow.com/a/13305642

        DEBUG = False

        s_debug = '-vvv' if DEBUG else ''
        s_repeat = '--input-repeat=999999' if repeat else ''

        cmd_start_option = f'{s_repeat} {s_debug}'.strip()

        logger.debug(f'repeat={repeat}|cmd_start_option={cmd_start_option}')

        self.instance = vlc.Instance(cmd_start_option)
        # self.instance = vlc.Instance('-vv')
        # self.instance = vlc.Instance('--input-repeat=999999 --ffmpeg-hw')
        # self.instance = vlc.Instance('--vout macosx --loop')
        # self.instance = vlc.Instance('--vout x11')
        # self.instance = vlc.Instance('--hwdec=none')
        # self.instance = vlc.Instance('--avcodec-hw=none -vv')
        # Create an empty vlc media player
        self.mediaplayer = self.instance.media_player_new()
        # self.mediaplayer.audio_set_volume(0)

        if platform.system() == "Darwin":  # for MacOS
            self.videoframe = QtWidgets.QMacCocoaViewContainer(0)
        else:
            self.videoframe = QtWidgets.QFrame()
        self.palette = self.videoframe.palette()
        self.palette.setColor(QtGui.QPalette.Window, QtGui.QColor(0, 0, 0))
        self.videoframe.setPalette(self.palette)
        self.videoframe.setAutoFillBackground(True)

    def __del__(self):

        self.media = None
        self.mediaplayer.release()
        self.instance.release()
        self.videoframe.destroy()

        logger.info("destroy video")

    def set_media_window(self):
        if platform.system() == "Linux":  # for Linux using the X Server
            self.mediaplayer.set_xwindow(int(self.videoframe.winId()))
        elif platform.system() == "Windows":  # for Windows
            self.mediaplayer.set_hwnd(int(self.videoframe.winId()))
        elif platform.system() == "Darwin":  # for MacOS
            self.mediaplayer.set_nsobject(int(self.videoframe.winId()))

    def open_youtube(self, url):
        logger.info(f'open_youtube : url={url}')
        import pafy
        video = pafy.new(url)
        best = video.getbest(preftype="mp4")
        self.open_file(best.url)

    def stop(self):
        self.mediaplayer.stop()
        # self.media.close()
        # self.mediaplayer.close()

    def open_file(self, path, start_time_second: float = 0.0):

        logger.info(f'open_file : path={path}')

        """Open a media file in a MediaPlayer
        """

        # dialog_txt = "Choose Media File"
        # filename = QtWidgets.QFileDialog.getOpenFileName(self, dialog_txt, os.path.expanduser('~'))
        # if not filename:
        #     return

        # getOpenFileName returns a tuple, so use only the actual file name
        self.media = self.instance.media_new(path)

        logger.debug(f'self.media={self.media}')

        # Put the media in the media player

        self.mediaplayer.set_media(self.media)

        # Parse the metadata of the file

        # self.media.parse()
        r_parse_with_options = self.media.parse_with_options(0, timeout=5)
        logger.debug(f'r_parse_with_options={r_parse_with_options}')

        # Set the title of the track as window title
        # self.setWindowTitle(self.media.get_meta(0))

        # The media player has to be 'connected' to the QFrame (otherwise the
        # video would be displayed in it's own window). This is platform
        # specific, so we must give the ID of the QFrame (or similar object) to
        # vlc. Different platforms have different functions for this

        self.set_media_window()

        # self.mediaplayer.set_time(10)

        # 시작 시간

        self.mediaplayer.play()

        # self.mediaplayer.set_volume(0)

        duration_micro_second: int = self.media.get_duration()
        duration_second: float = float(duration_micro_second) / 1000.0
        logger.info(f'duration_second={duration_second}|start_time_second={start_time_second}')

        if start_time_second and duration_micro_second:

            f_position_percent: float = (start_time_second * 1000.0) / duration_micro_second
            logger.debug(f'pos={f_position_percent}')

            if start_time_second < duration_second:

                self.mediaplayer.set_position(f_position_percent)
            else:
                logger.warning(f'NOT start_time_second < duration_second, skip start_time_second seeking')

        # logger.warning("MUTE!!!!!!!!!!!!!!!!!!!!!!!!")
        # self.mediaplayer.audio_set_mute(True)

        # self.play_pause()


class MPlayer(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        self.player = IPlayer()

        # self.widget = QtWidgets.QWidget()

        # set_background_color(self)

        self.vboxlayout = QtWidgets.QVBoxLayout()
        self.vboxlayout.addWidget(self.player.videoframe)
        self.vboxlayout.setContentsMargins(QMargins())

        # self.web1 = QWebEngineView()
        # # self.web1.load(QUrl('abot'))
        #
        # self.vboxlayout.addWidget(self.web1, stretch=1)

        # self.vboxlayout.addWidget(self.positionslider)
        # self.vboxlayout.addLayout(self.hbuttonbox)

        self.setLayout(self.vboxlayout)

        # /home/m/settop/media/epg/60.mp4

        # self.player.open_file(str(Path('~/settop/media/epg/60.mp4').expanduser()), start_time_second=0.0)
        self.player.open_file(str(Path('~/0.mp4').expanduser()), start_time_second=0.0)
        # self.player.open_file(str(Path('~/media_settop/3.mp4').expanduser()))

        # self.player.stop()

        # self.resize(250, 150)
        # self.move(300, 300)
        # self.setWindowTitle('Simple')
        self.showFullScreen()
        # self.show()

        # QWindowsWindowFunctions.setHasBorderInFullScreen(mainWindow.windowHandle(), true);

        # QWindowsWindowFunctions


# def __init__(self, parent=None,
#              flags=None) -> None:
#     super().__init__(parent, flags)


def main():
    """Entry point for our simple vlc player
    """

    setup_environment_variable_display()
    setup_signal()

    print(f"PyQt version: {PYQT_VERSION_STR}")

    app = QApplication(sys.argv)

    # noinspection PyUnusedLocal
    m = MPlayer()

    m.setStyleSheet("border: 1px solid red;")

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
