import os
from pathlib import Path

import pyscreenshot
from PIL import Image
from loguru import logger


def get_screenshot() -> Image:
    # im = ImageGrab.grab(backend='gnome-screenshot')
    os.environ['DISPLAY'] = ':0.0'
    im = pyscreenshot.grab()
    return im


def save_screenshot(path: Path, percent: float = 100.0):
    percent = float(percent)
    im = get_screenshot()

    if percent != 100.0:
        fpercent: float = percent / 100.0
        x = int(float(im.width) * fpercent)
        y = int(float(im.height) * fpercent)

        im_resize = im.resize((x, y), Image.ANTIALIAS)
        im = im_resize

    # https://github.com/python-pillow/Pillow/issues/2609#issuecomment-313841918
    im = im.convert('RGB')

    if not path.parent.is_dir():
        path.parent.mkdir(exist_ok=True)

    im.save(str(path))


if __name__ == '__main__':
    # im = get_screenshot()
    #
    # # Path('~/')
    #
    # im.save()

    # im.show()

    # shot_save()

    # Image()

    path = Path('/tmp/0.jpg')
    percent = 11.0
    save_screenshot(path=path, percent=percent)
    logger.info(f'path={path}|percent={percent}')
    os.system(f'open {path}')

    path = Path('/tmp/100.jpg')
    percent = 100.0
    save_screenshot(path=path, percent=percent)
    logger.info(f'path={path}|percent={percent}')
    os.system(f'open {path}')

    pass

    # print('')
    # shot_save()
