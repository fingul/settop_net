import uvicorn

import asyncio
import os
import uuid
from typing import Dict
from typing import List
from uuid import UUID

from fastapi import FastAPI, HTTPException
from loguru import logger
from starlette.endpoints import WebSocketEndpoint
from starlette.responses import RedirectResponse
from starlette.types import Scope, Receive, Send
from starlette.websockets import WebSocket

from models import CmdEnum, RequestKeepAlive, ServerResponseStbStart, \
    RequestStbAddSchedule, RequestStbStart, ServerRequest, GroupConfig, PerGroup, Command, GroupSendCommand, \
    RequestStbAddTransfer, SAMPLE_TRANSFER_URL1, SAMPLE_TRANSFER_URL2, SAMPLE_SCHEDULE_URL, \
    SAMPLE_VERSION_URL, SAMPLE_CONFIG_URL, RequestStbAddConfig
from models_file import PlaySet, TransferSet, get_sample_transfer_set
from sample_config import get_sample_config
from scheduler import get_sample_playset, get_sample_ticket_playset
from setup_log import setup_log

setup_log()

app = FastAPI()

BASE_URL = os.environ.get('BASE_URL', 'http://127.0.0.1:8000')
logger.info(f'BASE_URL={BASE_URL}')
logger.info(f'SAMPLE_TRANSFER_URL1={SAMPLE_TRANSFER_URL1}')
logger.info(f'SAMPLE_TRANSFER_URL2={SAMPLE_TRANSFER_URL2}')
logger.info(f'SAMPLE_SCHEDULE_URL={SAMPLE_SCHEDULE_URL}')
logger.info(f'SAMPLE_VERSION_URL={SAMPLE_VERSION_URL}')
logger.info(f'SAMPLE_CONFIG_URL={SAMPLE_CONFIG_URL}')


############################################################


class Manager:

    def __init__(self) -> None:

        self.control_wss: List[WebSocket] = []

        # KEY : uuid | VALUE : ws
        self.uuid_ws_dict: Dict[UUID, WebSocket] = dict()

        # KEY : id | VALUE : uuid
        self.id_uuids_dict: Dict[str, List[UUID]] = dict()

        self.group_config: GroupConfig = GroupConfig(group_dict=
        dict(
            a=PerGroup(ids={"0", "1", }, default_schedule=dict(a=1)),
            b=PerGroup(ids={"2", "3", }, default_schedule=dict(b=1)),
        )
        )
        self._calc_id_to_group()

    ######################
    # status
    ######################

    def _calc_id_to_group(self):

        self.id_group_dict = {}
        group_dict = self.group_config.group_dict

        for group_name in group_dict:
            ids = group_dict[group_name].ids
            for id in ids:
                self.id_group_dict[id] = group_name

        logger.debug(f'self.id_group_dict={self.id_group_dict}')

    def set_group_config(self, group_config: GroupConfig):
        self.group_config = group_config
        self._calc_id_to_group()

    def get_connections(self) -> dict:
        return dict(
            id_uuids_dict=self.id_uuids_dict,
            uuids=[str(i) for i in self.uuid_ws_dict.keys()],
            id_group_dict=self.id_group_dict,
        )

    ######################
    # control websocket
    ######################
    def control_ws_add(self, ws: WebSocket):
        self.control_wss.append(ws)

    def control_ws_remove(self, ws: WebSocket):
        if ws in self.control_wss:
            self.control_wss.remove(ws)

    ######################
    # client websocket
    ######################

    def ws_add(self, ws: WebSocket, uuid: UUID):
        self.uuid_ws_dict[uuid] = ws
        pass

    def ws_remove(self, uuid: UUID, id: str):
        self.uuid_ws_dict.pop(uuid)
        if id:
            uuids = self.id_uuids_dict.get(id, [])
            if uuids:
                if uuid in uuids:
                    uuids.remove(uuid)
            if not uuids:
                self.id_uuids_dict.pop(id)

    def ws_update(self, uuid: UUID, id: str):
        uuids = self.id_uuids_dict.get(id, [])
        if uuid not in uuids:
            uuids.append(uuid)
        self.id_uuids_dict[id] = uuids

    def dump(self):

        logger.info(f'manager.uuid_ws_dict={self.uuid_ws_dict}')
        logger.info(f'manager.id_uuids_dict={self.id_uuids_dict}')
        # logger.info(f'manager.group_id_dict={self.group_id_dict}')

    def get_group_by_id(self, id: str):
        return self.id_group_dict[id]

    def get_per_group_by_id(self, id: str) -> PerGroup:
        group_name = self.get_group_by_id(id)
        return self.group_config.group_dict[group_name]

    def get_schedule_by_group_name(self, group_name: str) -> PlaySet:

        # return get_sample_playset()

        return get_sample_ticket_playset()


        # return p.dict()

        # return self.group_config.group_dict[group_name].default_schedule

    def get_transfer_by_group_name(self, group_name: str) -> TransferSet:
        return get_sample_transfer_set()

        # return transferSet.dict()
        # return self.group_config.group_dict[group_name].default_schedule

    async def send_cmd_by_uuid(self, uuid: UUID, command: Command):
        ws: WebSocket = self.uuid_ws_dict.get(uuid)
        await ws.send_text(command.json())

    async def send_cmd_by_id(self, id: str, command: Command):
        uuids = self.id_uuids_dict.get(id, [])
        for uuid in uuids:
            await self.send_cmd_by_uuid(uuid, command)

    async def send_cmd_all(self, command: Command):

        for ws in self.uuid_ws_dict.values():
            ws: WebSocket = ws
            await ws.send_text(command.json())

    async def send_cmd_by_group(self, cmd: GroupSendCommand):
        logger.info(f'manager.group_send_command={cmd}')

        group_name: str = cmd.group_name

        per_group: PerGroup = self.group_config.group_dict.get(group_name)
        if per_group:
            for id in per_group.ids:
                await self.send_cmd_by_id(id, cmd.command)

    def get_remote_info(self, uuid: UUID) -> dict:
        ws: WebSocket = self.uuid_ws_dict.get(uuid)
        return dict(
            # client_state = ws.client_state,
            host=ws.client.host,
            port=ws.client.port,

        )


manager = Manager()


@app.get("/")
def read_root():
    return RedirectResponse(url='/docs')


@app.get('/update/version.json')
async def update_version():
    return dict(
        app=dict(
            version="20200131_112350",
            path="dist/app.20200131_112350.tgz",
            size=1024 * 1024,
        )
    )


@app.get("/config")
async def config():
    return get_sample_config()


@app.get("/schedule/{group_name}", name='schedule_by_group_name', response_model=PlaySet)
async def schedule_by_group_name(group_name: str):
    try:
        return manager.get_schedule_by_group_name(group_name)
    except KeyError:
        raise HTTPException(status_code=404)


@app.get("/transfer/{group_name}", name='transfer_by_group_name', response_model=TransferSet)
async def transfer_by_group_name(group_name: str):
    try:
        return manager.get_transfer_by_group_name(group_name)
    except KeyError:
        raise HTTPException(status_code=404)


@app.get("/group_names")
async def group_names() -> List[str]:
    return list(manager.group_config.group_dict.keys())


@app.get("/config")
async def group_config_read() -> GroupConfig:
    return manager.group_config


@app.post("/config")
async def group_config_write(g: GroupConfig):
    manager.set_group_config(g)
    # manager.dump()


@app.get("/connections")
async def get_connections() -> dict:
    return manager.get_connections()


@app.get("/get_remote_info/{uuid}")
def get_remote_info(uuid: UUID) -> dict:
    try:
        return manager.get_remote_info(uuid)
    except KeyError:
        raise HTTPException(status_code=404)


@app.post("/send_cmd_all")
async def send_cmd_all(server_request: ServerRequest):
    command = server_request.command
    logger.info(f'send_cmd_all : server_request={server_request}')
    await manager.send_cmd_all(command=command)


@app.post("/send_cmd_by_id/{id}")
async def send_cmd_by_id(id: str, server_request: ServerRequest):
    command = server_request.command
    logger.info(f'send_cmd_by_id : id={id} | server_request={server_request}')
    await manager.send_cmd_by_id(id, command)


@app.post("/send_cmd_by_group/{group_name}")
async def send_cmd_by_group(group_name: str, server_request: ServerRequest):
    command = server_request.command
    logger.info(f'send_cmd_by_group : group_name={group_name} | server_request={server_request}')
    await manager.send_cmd_by_group(GroupSendCommand(group_name=group_name, command=command))


@app.post("/send_cmd_by_uuid/{uuid}")
async def send_cmd_by_group(uuid: UUID, server_request: ServerRequest):
    command = server_request.command
    logger.info(f'send_cmd_by_group : uuid={uuid} | server_request={server_request}')
    await manager.send_cmd_by_uuid(uuid=uuid, command=command)


@app.post("/check_play_set")
async def check_playset(playSet: PlaySet):
    response = f'check_playSet : playSet={playSet}\nplaySet={playSet.json()}'
    logger.info(response)
    return response


@app.post("/check_tranfer_set")
async def check_playset(transferSet: TransferSet):
    response = f'check_transferSet : transferSet={transferSet}\ntransferSet.json()={transferSet.json()}'
    logger.info(response)
    return response


############################################################

@app.websocket_route("/ws/control")
class SessionClient(WebSocketEndpoint):
    encoding = "json"

    async def on_connect(self, websocket, **kwargs):
        logger.info(f'control:on_connect:websocket={websocket}')
        return await super().on_connect(websocket)

    async def on_disconnect(self, websocket, close_code):
        logger.info(f'control:on_disconnect:websocket={websocket}|close_code={close_code}')
        return await super().on_disconnect(websocket, close_code)

    async def on_receive(self, websocket, data):
        logger.info(f'control:on_receive:websocket={websocket}|data={data}|typeof data={type(data)}')


@app.websocket_route("/ws/client")
class SessionClient(WebSocketEndpoint):
    encoding = "json"

    def __init__(self, scope: Scope, receive: Receive, send: Send) -> None:
        super().__init__(scope, receive, send)
        self.task_tick_keep_alive = None
        self.uuid = uuid.uuid4()
        self.id = None
        self.send_keep_alive = False

    async def on_connect(self, websocket, **kwargs):
        logger.info(f'client:on_connect:uuid={self.uuid}|id={self.id}')
        await super().on_connect(websocket)
        # await websocket.accept()
        manager.ws_add(websocket, self.uuid)
        manager.dump()
        # await websocket.send_json(CommandPing().dict())
        if self.send_keep_alive:
            self.task_tick_keep_alive = asyncio.create_task(self.tick_keep_alive(websocket))
        else:
            self.task_tick_keep_alive = None

    async def on_disconnect(self, websocket, close_code):
        logger.info(f'client:on_disconnect:uuid={self.uuid}|id={self.id}|close_code={close_code}')
        manager.ws_remove(self.uuid, self.id)
        manager.dump()
        if self.task_tick_keep_alive:
            self.task_tick_keep_alive.cancel()

        return await super().on_disconnect(websocket, close_code)

    async def log_and_send(self, websocket, s: str):
        pass

    async def on_receive(self, websocket, data):

        assert type(data) == dict
        cmd = data.get('cmd')

        logger.debug(f'client:on_receive:uuid={self.uuid}|id={self.id}|data={data}|typeof data={type(data)}')

        # if cmd in [CmdEnum.keep_alive, CmdEnum.stb_add_policy, CmdEnum.stb_add_schedule]:
        #     logger.info(f'cmd processed cmd={cmd}')
        if cmd == CmdEnum.stb_start:

            # 셋탑 시작 메시지 처리
            request_stb_start = RequestStbStart(**data)
            self.id = request_stb_start.id
            manager.ws_update(self.uuid, self.id)
            manager.dump()

            # schedule_url: Optional[str] = None
            # result: bool = True
            # reason: Optional[str] = ''

            # try:
            #     group_name = manager.get_group_by_id(self.id)
            #     schedule_url: str = app.url_path_for(name="schedule_by_group_name",
            #                                          group_name=group_name).make_absolute_url(BASE_URL)
            # except KeyError:
            #     logger.error(f'have no group :uuid={self.uuid}|id={self.id}')
            #     result = False
            #     reason = 'have no group'

            # schedule_url: str = app.url_for(name="schedule_by_group_name", group_name=group_name)

            await websocket.send_text(ServerResponseStbStart().json())

            await websocket.send_text(RequestStbAddConfig().json())

            await websocket.send_text(RequestStbAddSchedule().json())

            await websocket.send_text(RequestStbAddTransfer().json())

            # if schedule_url:
            #     await websocket.send_text(RequestStbAddSchedule(schedule=Schedule(url=schedule_url)).json())
        # elif cmd == CmdEnum.stb_transfer_list:
        #     pass
        else:
            logger.info(f"on_receive : command not processed command|cmd={cmd}")

    ######################################################################

    @staticmethod
    async def tick_keep_alive(websocket):
        counter = 0
        while True:
            await websocket.send_text(RequestKeepAlive().json())
            counter += 1

            KEEP_ALIVE_SECOND = 60
            logger.debug(f'wait keep_alive : KEEP_ALIVE_SECOND={KEEP_ALIVE_SECOND}')
            await asyncio.sleep(KEEP_ALIVE_SECOND)

# uvicorn main:app --reload

#     websocat ws://127.0.0.1:8000/ws/client -E
#     websocat ws://127.0.0.1:8000/ws/control -E


# /usr/local/miniconda3/envs/net/bin/uvicorn
# main:app --reload

# {"cmd": "login", "payload":{"id":"0"}}



def main():
    uvicorn.run('ws_server:app', host="0.0.0.0", port=8000, log_level="info", reload=True)


if __name__ == "__main__":
    main()
