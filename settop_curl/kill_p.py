import multiprocessing
import time

from loguru import logger


def slow_worker():
    logger.info('Starting worker')
    time.sleep(0.1)
    logger.info('end worker')


if __name__ == '__main__':
    p = multiprocessing.Process(target=slow_worker)
    logger.info(f'before|p={p}|p.is_alive()={p.is_alive()}')

    p.start()
    logger.info(f'start|p={p}|p.is_alive()={p.is_alive()}')

    p.terminate()
    logger.info(f'terminate|p={p}|p.is_alive()={p.is_alive()}')

    p.join()
    logger.info(f'join p={p}|p.is_alive()={p.is_alive()}')
