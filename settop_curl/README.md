- install

    conda install -y pyopenssl pycurl ipython  -c conda-forge
    pip install -r requirements.txt

- 인증서 만들기

    openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 36500 -out certificate.pem
    
    openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
    -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" \
    -keyout www.example.com.key  -out www.example.com.cert

    
- 다운로드 테스트    
    
    curl -v --insecure --ftp-ssl --ssl-reqd  ftp://0.0.0.0:21000/10b.txt -o /tmp/10b.txt    