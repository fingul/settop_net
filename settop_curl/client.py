from pathlib import Path
from typing import Optional

import attr
import pycurl
from etaprogress.progress import ProgressBarWget
from loguru import logger

# from url_change import change_url, URLConnection
from settop_curl.url_change import URLConnection, change_url


# As long as the file is opened in binary mode, both Python 2 and Python 3
# can write response body to it without decoding.

# TIMEOUT_CURL_SECOND = 30


def down(url, dest_path: Path):
    dest_path.parent.mkdir(exist_ok=True, parents=True)

    with open(str(dest_path), 'wb') as f:
        c = pycurl.Curl()
        # self.c = c

        # c.setopt(c.URL, 'ftps://127.0.0.1:21000/1mb.txt')
        c.setopt(c.URL, url)
        c.setopt(c.WRITEDATA, f)
        c.perform()
        c.close()


@attr.s
class CurlClient:
    # url: str = attr.ib()
    # need_tls: bool = attr.ib(default=False)
    url_connection: URLConnection = attr.ib()
    path: str = attr.ib()
    byte_per_sec: int = attr.ib(default=0)
    bar: Optional[ProgressBarWget] = attr.ib(default=None)
    debug: bool = attr.ib(default=False)
    passive: bool = attr.ib(default=True)

    def run(self):

        with open(self.path, 'wb') as f:

            # debug = False

            byte_per_sec = 0
            # byte_per_sec = 1024 * 100

            logger.info(f'pycurl.version={pycurl.version}')

            # url_connection: URLConnection = change_url('ftp://0.0.0.0:21000/1mb.txt', id='0', password='*')

            logger.info(f'url_connection={self.url_connection}')

            c = pycurl.Curl()
            # self.c = c

            # c.setopt(c.URL, 'ftps://127.0.0.1:21000/1mb.txt')
            c.setopt(c.URL, self.url_connection.url)
            # c.setopt(pycurl.TIMEOUT, TIMEOUT_CURL_SECOND)
            # c.setopt(c.URL, 'ftp://0.0.0.0:21000/1mb.txt')
            # c.setopt(c.URL, 'ftp://0.0.0.0:21000/10b.txt')

            c.setopt(c.SSL_VERIFYPEER, False)  # equivalent to curl's --insecure
            c.setopt(c.SSL_VERIFYHOST, False)  # 0 -OR- 2

            # c.setopt(c.FTP_USE_EPSV, int(self.passive))
            # c.setopt(c.FTP_USE_EPSV, 1)
            # c.setopt(c.FTP_USE_EPSV, 0)
            if not self.passive:
                c.setopt(c.FTPPORT, '-')
            # c.setopt(c.FTP_USE_EPSV, 0)

            if self.url_connection.need_tls:
                c.setopt(c.USE_SSL, c.FTPSSL_TRY)

            # c.setopt(c.CURLOPT_USE_SSL, True)
            # c.setopt(c.USESSL_ALL, True)
            # c.setopt(c.FTPSSLAUTH, True)

            # c.setopt(c.SSL_VERIFYSTATUS, True)
            # c.setopt(c.SSLVERSION, c.SSLVERSION_TLSv1_2)
            # c.setopt(c.SSLVERSION, c.SSLVERSION_SSLv3)
            c.setopt(c.NOPROGRESS, False)
            c.setopt(c.XFERINFOFUNCTION, self.progress)
            c.setopt(c.MAX_RECV_SPEED_LARGE, self.byte_per_sec)

            # self.client.setopt(pycurl.SSL_VERIFYPEER, False)
            # self.client.setopt(pycurl.SSL_VERIFYHOST, False)

            if self.debug:
                c.setopt(pycurl.VERBOSE, 1)
                c.setopt(pycurl.DEBUGFUNCTION, self.callback_debug)

            # http://pycurl.io/docs/latest/quickstart.html#writing-to-a-file
            c.setopt(c.WRITEDATA, f)
            c.perform()
            c.close()

        pass

    def callback_debug(self, debug_type, debug_msg):
        # print "debug(%d): %s" % (debug_type, debug_msg)

        logger.debug(f'type:{debug_type}|{debug_msg}')

    def progress(self, download_t, download_d, upload_t, upload_d):

        # return
        # logger.info(f'total:{download_t}, current:{download_d}, upload_t, upload_d')

        # global bar
        if not self.bar and download_t:
            self.bar = ProgressBarWget(download_t, max_width=10, eta_every=1000)
            self.percent_last = 0

        bar = self.bar

        if bar and download_d:
            # logger.info(f'total:{download_t}, current:{download_d}, upload_t, upload_d')
            try:
                bar.numerator = download_d
                if int(bar.percent) != self.percent_last:
                    logger.info(f'bar={bar}|{bar.str_eta}|{bar.str_rate}|{bar.percent}')

                    self.percent_last = int(bar.percent)

                # logger.info(f'bar={bar}')
            except ZeroDivisionError:
                pass


if __name__ == '__main__':
    # url_connection: URLConnection = change_url('ftps://0.0.0.0:21000/1000mb.txt', id='0', password='*')
    # url_connection: URLConnection = change_url('ftp://0.0.0.0:21000/1000mb.txt', id='0', password='*')
    # url_connection: URLConnection = change_url('ftp://0.0.0.0:21000/1b.txt', id='0', password='*')
    # url_connection: URLConnection = change_url('ftps://0.0.0.0:21000/1000mb.txt', id='0', password='*')
    # c = CurlClient(path='/tmp/0.txt', url_connection=url_connection, byte_per_sec=0, debug=True, passive=False)
    # c = CurlClient(path='/tmp/0.txt', url_connection=url_connection, byte_per_sec=10000000, debug=False, passive=True)
    # c = CurlClient(path='/tmp/0.txt', url_connection='url_connection', byte_per_sec=10000000, debug=False, passive=True)
    # c.run()

    down(url='ftp://ibksch:test@10.10.10.103/2020/02/18/10232.MP4', dest_path=Path('/tmp/0.mp4'))
