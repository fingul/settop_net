import multiprocessing

import redis
from loguru import logger

from settop_curl.client import CurlClient
from settop_curl.url_change import URLConnection, change_url


def slow_worker():
    # print('Starting worker')
    # time.sleep(0.1)
    # print('Finished worker')
    url_connection: URLConnection = change_url('ftp://0.0.0.0:21000/1000mb.txt', id='0', password='*')
    # url_connection: URLConnection = change_url('ftp://0.0.0.0:21000/1b.txt', id='0', password='*')
    # url_connection: URLConnection = change_url('ftps://0.0.0.0:21000/1000mb.txt', id='0', password='*')
    # c = CurlClient(path='/tmp/0.txt', url_connection=url_connection, byte_per_sec=0, debug=True, passive=False)
    c = CurlClient(path='/tmp/0.txt', url_connection=url_connection, byte_per_sec=10000000, debug=False, passive=True)
    c.run()


if __name__ == '__main__':

    r = redis.Redis(port=8989)

    connected = False
    logger.info("wait connect")

    while 1:

        try:

            v = r.get('key')
            logger.info("redis connected")
            connected = True

            p = multiprocessing.Process(target=slow_worker)



        except (SystemExit, KeyboardInterrupt):
            logger.info("shutdown")
            break

        except redis.exceptions.ConnectionError:
            if connected:
                logger.info("wait connect")

        if v:
            p = multiprocessing.Process(target=slow_worker)
            print('BEFORE:', p, p.is_alive())

            p.start()
            print('DURING:', p, p.is_alive())

            # p.terminate()
            # print('TERMINATED:', p, p.is_alive())

            p.join()
            print('JOINED:', p, p.is_alive())
