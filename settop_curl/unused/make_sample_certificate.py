import os

from loguru import logger

PATH_CERT = '~/up/certificate'


def make_sample_cert():
    p = os.path.expanduser(PATH_CERT)
    os.makedirs(p, exist_ok=True)

    path_key = f'{p}/sample.key'
    path_cert = f'{p}/sample.cert'
    logger.info(f'make_sample_cert : p={p} | path_key={path_key} | path_cert={path_cert}')

    if not os.path.exists(path_key) or not os.path.exists(path_cert):
        logger.info(f'cert not exist create : path_key={path_key}|path_cert={path_cert}')

        cmd = f'''openssl req -new -newkey rsa:4096 -days 36500 -nodes -x509 \
            -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" \
            -keyout {path_key}  -out {path_cert}
        '''

        logger.info(cmd)

        os.system(cmd)

    return [path_key, path_cert]


if __name__ == '__main__':
    make_sample_cert()

    os.system(f'open {PATH_CERT}')
