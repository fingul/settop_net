import os
from pathlib import Path

from OpenSSL.SSL import TLSv1_2_METHOD
from loguru import logger

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler, TLS_FTPHandler
from pyftpdlib.handlers import TLS_FTPHandler
from pyftpdlib.servers import FTPServer

from make_sample_certificate import make_sample_cert

p = Path.home().joinpath('up')

if not p.exists():
    logger.info(f'dir not exist, create : p={p}')
    os.makedirs(str(p))

files = [dict(name='1b.txt', size=1), dict(name='10b.txt', size=10), dict(name='1mb.txt', size=1024 * 1024),
         dict(name='1000mb.txt', size=1024 * 1024 * 1024)]

for file in files:
    name = file['name']
    size = file['size']

    f = p.joinpath(name)
    if not f.exists():
        logger.info(f'file not exist, create : f={f}')
        with open(f, 'wb') as ff:
            ff.write(b'0' * size)

logger.info(f'p={p}')

# home = os.path

authorizer = DummyAuthorizer()
authorizer.add_user("0", "*", str(p), perm="elradfmwMT")
authorizer.add_anonymous(str(p))
# >>>

##################

[path_key, path_cert] = make_sample_cert()

handler = TLS_FTPHandler
# handler.certfile = 'keycert.pem'


handler.certfile = path_cert
handler.keyfile = path_key
# handler.ssl_protocol = TLSv1_2_METHOD


handler.authorizer = authorizer
# >>>
server = FTPServer(("0.0.0.0", 21000), handler)
server.serve_forever()
