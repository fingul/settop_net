import pycurl
from loguru import logger

# As long as the file is opened in binary mode, both Python 2 and Python 3
# can write response body to it without decoding.

import time
from etaprogress.progress import ProgressBar, ProgressBarWget

from url_change import change_url, URLConnection

total = 5
# bar = ProgressBar(total, max_width=40)
# for i in range(total + 1):
#     bar.numerator = i
#     print bar
#     time.sleep(1)

bar = None


def progress(download_t, download_d, upload_t, upload_d):
    # logger.info(f'total:{download_t}, current:{download_d}, upload_t, upload_d')

    global bar
    if not bar and download_t:
        bar = ProgressBarWget(download_t, max_width=10)
    if bar and download_d:
        # logger.info(f'total:{download_t}, current:{download_d}, upload_t, upload_d')
        try:
            bar.numerator = download_d
            logger.info(f'bar={bar}')
        except ZeroDivisionError:
            pass

    # print "Total to download", download_t
    # print "Total downloaded", download_d
    # print "Total to upload", upload_t
    # print "Total uploaded", upload_d


def callback_debug(debug_type, debug_msg):
    # print "debug(%d): %s" % (debug_type, debug_msg)

    logger.debug(f'type:{debug_type}|{debug_msg}')


with open('/tmp/0.txt', 'wb') as f:
    debug = False

    byte_per_sec = 0
    byte_per_sec = 1024 * 100

    logger.info(f'pycurl.version={pycurl.version}')

    # u: URLConnection = change_url('ftp://0.0.0.0:21000/1mb.txt', id='0', password='*')
    u: URLConnection = change_url('ftps://0.0.0.0:21000/1000mb.txt', id='0', password='*')

    logger.info(f'u={u}')

    c = pycurl.Curl()

    # c.setopt(c.URL, 'ftps://127.0.0.1:21000/1mb.txt')
    c.setopt(c.URL, u.url)
    # c.setopt(c.URL, 'ftp://0.0.0.0:21000/1mb.txt')
    # c.setopt(c.URL, 'ftp://0.0.0.0:21000/10b.txt')

    c.setopt(c.SSL_VERIFYPEER, False)  # equivalent to curl's --insecure
    c.setopt(c.SSL_VERIFYHOST, False)  # 0 -OR- 2

    if u.need_tls:
        c.setopt(c.USE_SSL, c.FTPSSL_TRY)

    # c.setopt(c.CURLOPT_USE_SSL, True)
    # c.setopt(c.USESSL_ALL, True)
    # c.setopt(c.FTPSSLAUTH, True)

    # c.setopt(c.SSL_VERIFYSTATUS, True)
    # c.setopt(c.SSLVERSION, c.SSLVERSION_TLSv1_2)
    # c.setopt(c.SSLVERSION, c.SSLVERSION_SSLv3)
    c.setopt(c.NOPROGRESS, False)
    c.setopt(c.XFERINFOFUNCTION, progress)
    c.setopt(c.MAX_RECV_SPEED_LARGE, byte_per_sec)

    # self.client.setopt(pycurl.SSL_VERIFYPEER, False)
    # self.client.setopt(pycurl.SSL_VERIFYHOST, False)

    if debug:
        c.setopt(pycurl.VERBOSE, 1)
        c.setopt(pycurl.DEBUGFUNCTION, callback_debug)

    # http://pycurl.io/docs/latest/quickstart.html#writing-to-a-file
    c.setopt(c.WRITEDATA, f)
    c.perform()
    c.close()
