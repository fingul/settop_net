import multiprocessing
import time

from loguru import logger

from client import CurlClient
from url_change import URLConnection, change_url


def slow_worker():
    logger.info('Starting worker')
    url_connection: URLConnection = change_url('ftps://0.0.0.0:21000/1000mb.txt', id='0', password='*')
    c = CurlClient(path='/tmp/0.txt', url_connection=url_connection, byte_per_sec=1000)
    c.run()
    logger.info('end worker')


if __name__ == '__main__':
    p = multiprocessing.Process(target=slow_worker)
    logger.info(f'before|p={p}|p.is_alive()={p.is_alive()}')

    p.start()
    logger.info(f'start|p={p}|p.is_alive()={p.is_alive()}')

    p.terminate()
    logger.info(f'terminate|p={p}|p.is_alive()={p.is_alive()}')

    p.join()
    logger.info(f'join p={p}|p.is_alive()={p.is_alive()}')
