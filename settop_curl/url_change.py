from typing import Optional
from urllib.parse import urlparse, ParseResult, quote

import attr
from loguru import logger


@attr.s
class URLConnection:
    url: str = attr.ib()
    need_tls: bool = attr.ib(default=False)


def change_url(s_url: str, id: Optional[str] = None, password: Optional[str] = None) -> URLConnection:
    r: ParseResult = urlparse(s_url)
    logger.info(f'r={r}')

    r1 = r
    if id or password:
        netloc = r.netloc
        id = id or ''
        password = password or ''
        r1 = r._replace(netloc=f'{quote(id)}:{quote(password)}@{netloc}')

    need_tls = False

    r2 = r1
    if r.scheme == 'ftps':
        r2 = r1._replace(scheme='ftp')
        need_tls = True
    return URLConnection(r2.geturl(), need_tls=need_tls)

    # if r.scheme


if __name__ == '__main__':
    pass
    s_url = 'ftps://1.2.3.4?a=1&b=2'
    logger.info(change_url(s_url=s_url))

    s_url = 'ftps://1.2.3.4?a=1&b=2'
    logger.info(change_url(s_url=s_url, id='0', password='*'))

    s_url = 'ftp://1.2.3.4?a=1&b=2'
    logger.info(change_url(s_url=s_url))
