import datetime
import json
from enum import Enum
from pathlib import Path
from typing import Dict, Optional
from typing import List

from PyQt5.QtCore import QRect, QPoint, QSize
from loguru import logger
from pydantic import BaseModel


class TVControlMethodEnum(str, Enum):
    HDMI = 'hdmi'
    SERIAL = 'serial'
    NETWORK = 'network'


class TVModeEnum(str, Enum):
    SCHEDULE = 'sch'
    ALWAYS_ON = 'alwayson'


class RegionTypeEnum(str, Enum):
    MAIN = 'main'
    BOTTOM = 'bottom'
    SIDE = 'side'
    TOP = 'top'


class TVOnEnum(str, Enum):
    ON = 'on'
    OFF = 'off'


class ContentTypeEnum(str, Enum):
    VIDEO = 'video'
    IMAGE = 'image'
    URL = 'url'
    TEXT = 'text'

    APP_TICKET = 'app/ticket'

    APP_WEATHER = 'app/weather'

    APP_NEWS = 'app/news'
    APP_STOCK = 'app/stock'
    APP_EXCHANGERATE = 'app/exchangerate'
    APP_WEB = 'app/web'


URL_CONTENTS_TYPES = [
    ContentTypeEnum.URL,
    ContentTypeEnum.APP_WEATHER,
    ContentTypeEnum.APP_NEWS,
    ContentTypeEnum.APP_STOCK,
    ContentTypeEnum.APP_EXCHANGERATE,
    ContentTypeEnum.APP_WEB,
]


class TicketURLEnum(str, Enum):
    URL_TICKET_TOP = 'app://ticket_top'
    URL_TICKET_RIGHT = 'app://ticket_right'


'''
 https://docs.google.com/document/d/1bLLpkB4Q_VJ2taxIU_Y-C6w4K95xLDdStu72swFZhm4/edit
 
 순번대기표 : app/ticket
    - 상단 바
        - 좌표 : 기술 예정
        - url : app://ticket_top
    - 우측 바
        - 좌표 : 기술 예정
        - url : app://ticket_right
 
 뉴스 : app/news
 날씨 : app/weather
 주가 : app/stock
 환율 : app/exchangerate
 일반 : app/web
 파일 크기는 0으로 표시함.
 
'''


class DownloadModeEnum(str, Enum):
    EPG = 'epg'
    URGENT = 'urgent'
    PREDIST = 'predist'
    HIT = 'hit'
    LOCAL = 'local'


def get_sub_dir_for_download_mode(download_mode_enum: DownloadModeEnum):
    if download_mode_enum in [DownloadModeEnum.EPG, DownloadModeEnum.URGENT, DownloadModeEnum.PREDIST]:
        return 'epg'
    elif download_mode_enum == DownloadModeEnum.HIT:
        return 'hit'
    elif download_mode_enum == DownloadModeEnum.LOCAL:
        return 'local'
    raise Exception("unknown ")


'''

# https://docs.google.com/spreadsheets/d/1tKP1zo0aauqIRCMKpTz98AYuDSbLt_oRGnZ0t8Dmfsw/edit#gid=0

epg / urgent / predist => epg 폴더에 저장
hit => hit 폴더에 저장
local => local 폴더에 저장
'''


# sch : 스케쥴 방송
# live : 라이브 방송
# vod : VOD 재생
# none : 아무것도 안함
# urgent : 긴급 방송


class TVStatus(str, Enum):
    SCHEDULE = 'sch'
    LIVE = 'live'
    VOD = 'vod'
    NONE = 'none'
    URGENT = 'urgent'


class TVOnOffTime(BaseModel):
    type: TVOnEnum
    time: datetime.time


class Screen(BaseModel):
    screen_no: int
    fullsize: List[int]


class Size(BaseModel):
    width: int
    height: int


class Point(BaseModel):
    x: int
    y: int


class Rect(BaseModel):
    size: Size
    point: Point

    @property
    def qrect(self) -> QRect:
        return QRect(QPoint(self.point.x, self.point.y), QSize(self.size.width, self.size.height))

    @classmethod
    def fromQRect(cls, qrect: QRect):
        return cls(size=Size(width=qrect.width(), height=qrect.height()), point=Point(x=qrect.left(), y=qrect.top()))


class Content(BaseModel):
    title: str = None  # META
    file: str
    size: int = 0
    type: ContentTypeEnum


class Layout(BaseModel):
    name: str
    rect: Rect
    defaultContent: Content
    type: RegionTypeEnum


class PlayItem(BaseModel):
    no: int = None  # META
    title: str = None  # META
    type: ContentTypeEnum
    fileName: str
    start: datetime.time
    end: datetime.time
    fileSize: int = 0


# class PlayerItem(BaseModel):
#     type: ContentTypeEnum
#     url: str
#     path: Path
#
#     def __init__(self, type: ContentTypeEnum, url: str = None, path: Path = None, fileSize=0,
#                  **data: Any) -> None:
#         super().__init__(**data)
#         self.type = type
#         self.url = url
#         self.path = path
#
#     def get_video_or_image_path(self)->Optional[Path]:
#
#
#
#     @property
#     def isOk(self):
#         pass
#
#     @classmethod
#     def fromPlayItem(cls, playItem: PlayItem):
#         pass
#
#     @classmethod
#     def fromContent(cls, content: Content):
#         type = content.type
#         if content.type == ContentTypeEnum.URL:
#             return cls(url=content.file, type=type)
#         elif content.type == ContentTypeEnum.VIDEO:
#             return cls()
#

# @@
class PlaySet(BaseModel):
    updateTime: datetime.datetime
    schVer: str = None  # META
    groupId: str = None  # META
    tvMode: TVModeEnum = TVModeEnum.SCHEDULE
    tvOnOffTime: List[TVOnOffTime]
    screenSize: Size
    layouts: List[Layout]
    playItemsByLayout: Dict[str, List[PlayItem]]


class NoFile(BaseModel):
    cid: str
    mode: DownloadModeEnum
    fileName: str
    fileSize: int


class File(BaseModel):
    no: int = 0
    type: ContentTypeEnum
    path: str
    # fileName: str
    fileSize: int = 0
    cid: str
    mode: DownloadModeEnum
    onDate: datetime.date = None  # META

    @property
    def realPath(self) -> Path:
        '''
        실제 파일 시스템 경로를 리턴
        :return:
        '''

        path_base = Path("~/settop/media").expanduser()

        subdir = get_sub_dir_for_download_mode(self.mode)

        name = Path(self.path).name

        return path_base.joinpath(subdir, name)

    def get_no_file(self) -> Optional[NoFile]:
        realPath = self.realPath
        exist = realPath.is_file()
        realSize = None
        file_name = realPath.name
        if exist:
            realSize = self.realPath.stat().st_size

        if exist and realSize == self.fileSize:
            return None
        else:
            logger.info(f"NOT FOUND : realPath={realPath} | exist={exist}|size={realSize}|file={self}")
            return NoFile(cid=self.cid, mode=self.mode, fileName=file_name, fileSize=self.fileSize)


class TransferSet(BaseModel):
    updateTime: datetime.datetime
    transferVer: str
    groupId: str = None  # META
    onDate: datetime.date
    fileVer: str
    files: List[File]

    # # https://github.com/tiangolo/fastapi/issues/563
    # class Config:
    #     json_encoders = {
    #         PosixPath: lambda v: str(v),
    #         WindowsPath: lambda v: str(v)
    #     }


def get_sample_playset() -> PlaySet:
    layout_top = Layout(name='top',
                        rect=Rect(size=Size(width=0, height=0),
                                  point=Point(x=0, y=0)),
                        defaultContent=Content(file="abc/1/2/3.jpg", type=ContentTypeEnum.IMAGE),
                        type=RegionTypeEnum.TOP

                        )

    # time_now = datetime.time(hour=8)

    # time part 만 사용 (timedelta 사용 용)
    datetime_time = datetime.datetime.combine(datetime.date.today(), datetime.time(hour=8, microsecond=500 * 1000))

    playitems_top = [PlayItem(no=1,
                              title='title1',
                              type=ContentTypeEnum.VIDEO,
                              fileName=f'{i}.mp4',
                              start=(datetime_time + datetime.timedelta(minutes=0 + 1 * i)).time(),
                              end=(datetime_time + datetime.timedelta(minutes=1 + 1 * i)).time())

                     for i in range(0, 3)]

    p = PlaySet(
        updateTime=datetime.datetime.utcnow(),
        schVer='schVerSample',
        groupId=' groupIdSample',
        tvMode=TVModeEnum.SCHEDULE,
        tvOnOffTime=[
            TVOnOffTime(type=TVOnEnum.ON, time=datetime.time(hour=9)),
            TVOnOffTime(type=TVOnEnum.OFF, time=datetime.time(hour=12 + 6)),
        ],
        screenSize=Size(width=1920, height=1080),
        layouts=[
            layout_top,
        ],
        playItemsByLayout={"top": playitems_top}
    )

    return p


def get_sample_transfer_set() -> TransferSet:
    files = [
        File(no=0, type=ContentTypeEnum.VIDEO, path='1/2/3/0.mp4', fileSize=5, cid='abcd',
             mode=DownloadModeEnum.EPG, onDate=datetime.date.today()),
        File(no=1, type=ContentTypeEnum.VIDEO, path='1/2/3/2.mp4', fileSize=5, cid='abcd',
             mode=DownloadModeEnum.EPG, onDate=datetime.date.today()),

    ]

    now = datetime.datetime.now()
    transferSet = TransferSet(
        fileVer='00000',
        updateTime=now,
        transferVer='transferVer',
        groupId='groupId',
        files=files,
        onDate=datetime.date.today())

    return transferSet


if __name__ == '__main__':
    sample_dir: Path = Path(__file__).parent.joinpath('sample')

    sample_dir.mkdir(parents=True, exist_ok=True)

    #################
    # play set
    #################

    p = PlaySet(
        updateTime=datetime.datetime.utcnow(),
        schVer='schVer',
        groupId='groupId',
        tvMode=TVModeEnum.SCHEDULE,
        tvOnOffTime=[],
        screenSize=Size(width=1920, height=1080),
        layouts=[],
        playItemsByLayout={}
    )

    logger.info(f'{p.json(indent=2)}')

    ####################################################

    p = get_sample_playset()

    logger.info(f'{p.json(indent=2)}')

    s_json = p.json(indent=2)

    obj = json.loads(s_json)

    p2 = PlaySet.parse_obj(obj)
    logger.info(f'p2={repr(p2)}')

    # 파일 저장
    play_set_sample_path: Path = sample_dir.joinpath('play_set_sample.json')
    play_set_sample_path.write_text(p.json(indent=2), encoding='utf8')

    #################
    # transfer set
    #################

    transferSet = get_sample_transfer_set()
    transferSet.dict()
    logger.info(f'transferSet={transferSet.json(indent=2)}')

    # 파일 저장
    transfer_set_sample_path: Path = sample_dir.joinpath('transfer_set_sample.json')
    transfer_set_sample_path.write_text(transferSet.json(indent=2), encoding='utf8')
    # C37NRHGRG5MQ
    # 3569510618773316

    sample_file_obj = {
        "no": 2,
        "type": "video",
        "path": "/storage/org/video/2020/02/18/10217.mp4",
        "fileSize": 14724401,
        "cid": "000000010155",
        "mode": "epg",
        "onDate": "2020-03-12"
    }

    file = File.parse_obj(sample_file_obj)
    logger.info(f'file={file}|file.realPath={file.realPath}')
    logger.info(f'get_no_file={file.get_no_file()}')
