import asyncio
import gc
import os
import pathlib
import sys
import typing

import PyQt5
from PyQt5 import QtCore
from PyQt5.QtCore import QUrl, Qt, QRect, QCoreApplication
from PyQt5.QtGui import QPixmap
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QStackedWidget
# from app import App
from loguru import logger
from quamash import QEventLoop

from InfinitePlayer import IPlayer
from anycon_old import AnyCon
from mod_setup_signal import setup_signal

EXIT_CODE_REBOOT = -123
def restart_app():
    QCoreApplication.exit(EXIT_CODE_REBOOT)





class App(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def keyPressEvent(self, e):
        logger.info(f'keyPressEvent : e={e} | e.key()={e.key()}| e.text()={e.text()}')
        text = e.text()

        if text == '1':

            self.anycon.set_u()

            pass
        elif text == '2':
            # p = pathlib.Path('~/200x200.jpg').expanduser()
            self.anycon.set_i('~/200x200.jpg')

            pass
        elif text == '3':
            self.anycon.set_v()
        elif text == 'q':
            restart_app()

    def initUI(self):

        rect = PyQt5.QtCore.QRect(0, 0, 200, 200)
        self.anycon = AnyCon(self, rect)

        # self.lbl1 = QLabel('Zetcode', self)
        # self.lbl1.move(15, 10)
        #
        # lbl2 = QLabel('tutorials', self)
        # lbl2.move(35, 40)
        #
        # lbl3 = QLabel('for programmers', self)
        # lbl3.move(55, 70)

        self.setGeometry(PyQt5.QtCore.QRect(0, 0, 400, 400))
        self.setWindowTitle('Absolute')
        self.show()

        QtCore.QTimer.singleShot(0, self.after)

    def after(self):
        logger.info('boom')
        # self.showFullScreen()


if __name__ == '__main__':

    os.environ['DISPLAY'] = ':0.0'

    from PyQt5.Qt import PYQT_VERSION_STR

    logger.info(f"PYQT_VERSION_STR={PYQT_VERSION_STR}")

    setup_signal()

    current_exit_code = EXIT_CODE_REBOOT

    app = QApplication(sys.argv)

    loop = QEventLoop(app)
    asyncio.set_event_loop(loop)  # NEW must set the event loop

    # disable show scrollbar
    settings = QWebEngineSettings.globalSettings()
    settings.setAttribute(QWebEngineSettings.ShowScrollBars, False)

    while current_exit_code == EXIT_CODE_REBOOT:
        logger.info(f'start app')

        app = QApplication.instance()

        ex = App()

        logger.debug(f'app={app}|ex={ex}')

        # ex2 = App()
        current_exit_code = app.exec_()
        logger.info(f'current_exit_code={current_exit_code}')
        gc.collect()

    sys.exit(current_exit_code)
