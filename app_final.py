import datetime
import multiprocessing
import sys
import time

# from requests import Session
from requests_async import Session

import aioprocessing
import uvicorn
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot, QCoreApplication, QRect
from PyQt5.QtWidgets import (QApplication, QMainWindow)
from fastapi import FastAPI
from loguru import logger

from mod_setup_signal import setup_signal


class RestartApp:
    EXIT_CODE_REBOOT = -123

    @staticmethod
    def restart_app():
        logger.info(f'RestartApp:restart_app()')
        QCoreApplication.exit(RestartApp.EXIT_CODE_REBOOT)

    @staticmethod
    def quit_app():
        logger.info(f'RestartApp:quit_app()')
        QCoreApplication.exit()


class WebThread(QThread):
    on_message = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.done = False
        self.q_in = aioprocessing.AioQueue()
        self.q_out = aioprocessing.AioQueue()
        self.process = multiprocessing.Process(target=WebThread.start_web_process, args=(self.q_in, self.q_out))
        self.process.start()

    def run(self):

        logger.info(f'Web::run begin')
        while not self.done:

            time.sleep(.01)
            message = self.q_out.get()
            try:
                logger.info(f'Web::run message={message}')
                self.on_message.emit(message)
            except Exception as e:
                logger.exception("error while Web::run")

    @pyqtSlot()
    def close(self):
        self.done = True
        if self.process:
            logger.debug("kill web")
            self.process.kill()
            self.process = None
            logger.debug("kill web done")

    def __del__(self):
        self.close()

    def start_web_process(q_in: aioprocessing.AioQueue, q_out: aioprocessing.AioQueue):
        # loop = asyncio.get_event_loop()
        # loop.run_until_complete(handler(outputqueue, inputqueue))
        # session = Session()
        # session.trust_env = False

        app = FastAPI()

        @app.get("/")
        async def read_root():
            import requests_async as requests
            logger.info("begin")
            await requests.get('http://chosun.com')
            logger.info("done")

            q_out.put(dict(a=1))
            return {"Hello": "World"}

        @app.get("/sync")
        def read_root():
            q_out.put(dict(coro=1))
            return {"Hello": "World"}

        @app.get("/async")
        async def read_root():
            await q_out.coro_put(dict(coro=1))
            return {"Hello": "World"}

        @app.get("/items/{item_id}")
        def read_item(item_id: int, q: str = None):
            return {"item_id": item_id, "q": q}

        uvicorn.run(app, host='0.0.0.0', port=9999)


class App(QMainWindow):

    def __init__(self) -> None:
        super().__init__()
        # self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)

        now = datetime.datetime.now()
        self.setWindowTitle(f'{now}')
        self.setGeometry(QRect(0, 0, 400, 100))

    @pyqtSlot(object)
    def message(self, msg):
        logger.info(f'App:msg={msg}')
        RestartApp.restart_app()

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        super().closeEvent(a0)
        logger.info("close event")
        RestartApp.quit_app()


def main():
    setup_signal()

    app = QApplication(sys.argv)
    # window = MainWindow()
    # window.show()

    # https://stackoverflow.com/a/6789205
    web = WebThread()
    web.finished.connect(app.exit)
    web.start()

    current_exit_code = RestartApp.EXIT_CODE_REBOOT

    logger.info(f'start app')
    while current_exit_code == RestartApp.EXIT_CODE_REBOOT:
        app = QApplication.instance()

        ex = App()
        web.on_message.connect(ex.message)
        # ex.showFullScreen()
        ex.show()

        logger.debug(f'app={app}|ex={ex}')

        current_exit_code = app.exec_()

    web.close()

    # web.stop()

    # web.on_message.connect()

    sys.exit(current_exit_code)


if __name__ == '__main__':

    setup_signal()
    main()
