import pprint
from typing import List

import attr
import netifaces
from loguru import logger


@attr.s
class NicInfo:
    name: str = attr.ib(default='')
    ip4_address: str = attr.ib(default='')
    ip4_netmask: str = attr.ib(default='')
    ip4_broadcast: str = attr.ib(default='')
    mac: str = attr.ib(default='')

    @property
    def is_valid(self):
        return self.name and self.mac and self.name.startswith('lo') and self.ip4_address != '127.0.0.1'


@attr.s
class NicInfos:
    nic_infos: List[NicInfo] = attr.ib(default=[])

    @property
    def macs(self) -> List[str]:
        return [nic_info.mac for nic_info in self.nic_infos if nic_info.name != 'lo']

    @property
    def device_id(self) -> str:
        # if not self.nic_infos:
        #     raise Exception('No mac found')
        # nic_info: NicInfo = sorted(self.nic_infos, key=lambda i: i.mac)[0]

        return min(self.macs)

        # return nic_info.mac

    @classmethod
    def load(cls):

        i = cls()

        interface_names = netifaces.interfaces()
        gateways = netifaces.interfaces()
        logger.info(f'gateways={gateways}')

        for interface_name in interface_names:
            addresses: dict = netifaces.ifaddresses(interface_name)
            logger.debug(f'interface_name={interface_name}|addresses={addresses}')

        for interface_name in interface_names:
            addresses: dict = netifaces.ifaddresses(interface_name)

            # logger.info(f'interface_name={interface_name}|addresses={addresses}')

            link: dict = addresses.get(netifaces.AF_LINK)
            inet: dict = addresses.get(netifaces.AF_INET)

            mac = ip = netmask = broadcast = None

            if link:
                mac = link[0].get('addr')

            if inet:
                ip = inet[0].get('addr')
                netmask = inet[0].get('netmask')
                broadcast = inet[0].get('broadcast')

            if mac:
                # 소문자로 변경
                mac = mac.lower()

                logger.info(
                    f'interface_name={interface_name}|mac={mac}|ip={ip}|netmask={netmask}|broadcast={broadcast}')

                i.nic_infos.append(
                    NicInfo(name=interface_name, ip4_address=ip, ip4_netmask=netmask, ip4_broadcast=broadcast, mac=mac))

        return i


if __name__ == '__main__':
    nicinfos = NicInfos.load()
    logger.info(f'nicinfos={pprint.pformat(nicinfos)}')
    logger.info(f'nicinfos.device_id={nicinfos.device_id}')
    # logger.info(f'nicinfos.macs={nicinfos.macs}')
    # logger.info(f'nicinfos.device_id={nicinfos.device_id}')

    # 주어진 맥주소 : ['1C:1B:0D:1E:AB:C2', '00:50:56:C0:00:01', '00:50:56:C0:00:08']
    # 대표 맥주소 : 00:50:56:c0:00:01
