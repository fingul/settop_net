import asyncio
import json
import os
import time
from pathlib import Path
from typing import Union, List, Optional, Dict

import click
import requests_async as requests
import websockets
from loguru import logger
from pydantic import BaseModel

import aioprocessing
from mod_datetime_str import get_now_str_kst, get_y_m_d_path
from mod_get_mac import NicInfos
from mod_psutil import get_resource_usage
from mod_screenshot import save_screenshot
from models import Command, RequestStbStart, CmdEnum, ResponseKeepAlive, ResponseStbAddSchedule, \
    RequestStbAddSchedule, RequestStbOff, ResponseStbOff, ResponseStbReboot, \
    RequestStbReboot, RequestStbAutoOnOff, ResponseStbAutoOnOff, RequestTVOn, ResponseTVOn, RequestTVOff, ResponseTVOff, \
    RequestPlayStat, ResponsePlayStat, RequestStbScreenImage, ResponseStbScreenImage, RequestStbLog, ResponseStbLog, \
    RequestStbUpdateSW, ResponseStbUpdateSW, RequestStbLiveOn, ResponseStbLiveOff, RequestStbLiveOff, ResponseStbLiveOn, \
    Response, RequestStbAddTransfer, ResponseStbAddTransfer, \
    RequestStbAddConfig, ResponseStbAddConfig, Schedule, Transfer, \
    RequestStbTransferList, ServerResponseStbTransferList, RequestStbSetVolume, ResponseStbSetVolume, \
    DataScreenShotResult, DataLiveOn, DataTVStat, DataStbLiveOff
from models_file import PlaySet, TransferSet, NoFile
from sample_config import Config

KWARG_JSON = dict(ensure_ascii=False, indent=2)


def log_and_run_cmd(cmd: str, note: str) -> bool:
    logger.info(f'run_cmd : note={note}|cmd={cmd}')
    # noinspection PyBroadException
    try:
        os.system(cmd)
        return True
    except:
        logger.exception(f'error run_cmd')
    return False


class SettopData(BaseModel):
    config: Optional[Config] = None
    schedules: List[Schedule] = []
    transfers: List[Transfer] = []
    fileVer: str = 'NOT_RECEIVED'

    # 스크린별 마지막 상태
    dataTVStats: Dict[int, DataTVStat] = {}


class QItemSchedule:
    schedules: List[Schedule]

    def __init__(self, schedules) -> None:
        super().__init__()
        self.schedules = schedules


class QItemLiveOn:
    dataLiveOn: DataLiveOn

    def __init__(self, dataLiveOn) -> None:
        super().__init__()
        self.dataLiveOn = dataLiveOn


class QItemLiveOff:
    dataLiveOff: DataStbLiveOff

    def __init__(self, dataLiveOff) -> None:
        super().__init__()
        self.dataLiveOff = dataLiveOff


class SettopApp:

    def __init__(self,
                 q_in: aioprocessing.AioQueue = None, q_out: aioprocessing.AioQueue = None) -> None:
        super().__init__()
        self.q_in = q_in if q_in else aioprocessing.AioQueue()
        self.q_out = q_out if q_out else aioprocessing.AioQueue()
        self.data = SettopData()

    def q_put(self, o):
        if self.q_out:
            self.q_out.put(o)

    def getFileVer(self):
        return self.data.fileVer

    def getTVStat(self, screen_no: int) -> DataTVStat:
        v = self.data.dataTVStats.setdefault(screen_no, DataTVStat())
        return v

    def set_config(self, config: Config):
        self.data.config = config
        logger.info('set_config')
        self.save()

    def set_schedules(self, schedules: List[Schedule]):
        self.data.schedules = schedules
        logger.info('set_schedules')
        self.save()

        self.q_put(QItemSchedule(schedules))

    def live_on(self, data_live_on: DataLiveOn):
        self.q_put(QItemLiveOn(data_live_on))

    def live_off(self, data_live_off: DataLiveOn):
        self.q_put(QItemLiveOff(data_live_off))

    def set_transfers(self, transfers: List[Transfer]):
        self.data.transfers = transfers
        if len(self.data.transfers):
            self.data.fileVer = list(self.data.transfers)[-1].transfer_set.fileVer

        logger.info('transfers')
        self.save()

    def get_nofiles(self) -> List[NoFile]:
        no_files = []
        for transfer in self.data.transfers:
            for file in transfer.transfer_set.files:
                no_file = file.get_no_file()
                if no_file:
                    no_files.append(no_file)
        return no_files

    async def async_tv_on(self):
        logger.warning("TODO:IMPLEMENT")

    async def async_tv_off(self):
        logger.warning("TODO:IMPLEMENT")

    def save(self):

        # noinspection PyBroadException
        try:
            now = get_now_str_kst()
            # 20200403_142900_996_KST
            y_m_d_path = get_y_m_d_path(now)

            path = Path(f'~/json/{y_m_d_path}/{now}.json').expanduser()

            path.parent.mkdir(parents=True, exist_ok=True)

            logger.info(f'saved path={path}')

            path.write_text(self.data.json(**KWARG_JSON))
        except:
            logger.exception('error save json')

    def set_auto_on_off(self):
        pass

    def stb_reboot(self):
        log_and_run_cmd(cmd="sudo reboot", note="stb_reboot")

    def stb_off(self):
        log_and_run_cmd(cmd="sudo poweroff", note="stb_off")

    def screen_shot(self, ratio: float) -> DataScreenShotResult:

        now = get_now_str_kst()
        # 20200403_142900_996_KST

        subpath = f'screenshot/{now}.jpg'

        path = Path(f'~/settop/media/{subpath}').expanduser()

        path.parent.mkdir(parents=True, exist_ok=True)

        logger.info(f'saved path={path}')

        save_screenshot(path=path, percent=ratio)

        size = path.stat().st_size

        return DataScreenShotResult(path=subpath, size=size)

    async def async_set_volume(self):

        logger.info("async_set_volume")

        pass


async def send_command(websocket, command: Union[Command, Response]):
    # logger.debug(f'send_command : command={command}|dict={command.json()}')
    cmd = command.cmd
    logger.warning(f'send_command : cmd={cmd}|dict={command.json()}')
    await websocket.send(command.json())


async def recv_command(websocket) -> dict:
    raw = await websocket.recv()

    d = json.loads(raw)

    cmd = d.get('cmd')

    logger.warning(f'recv_command : cmd={cmd}|raw={raw}')

    return d


async def client_loop(url: str, id: str, settopapp: SettopApp):
    logger.info(f'connecting to url={url}')

    # await settopapp.async_set_volume()

    async with websockets.connect(url, timeout=5, close_timeout=3) as websocket:
        # name = input("What's your name? ")

        await send_command(websocket, RequestStbStart(id=id))

        while True:

            data = await recv_command(websocket)

            cmd = data.get('cmd')

            # response
            if cmd == CmdEnum.stb_start:
                # o = ServerResponseStbStart(id=id, **data)
                # logger.info(f'{o.__class__.__name__} | {o}')
                pass
            elif cmd == CmdEnum.stb_add_config:
                # @@
                o = RequestStbAddConfig(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                url = o.url

                logger.info(f'downloading config : url={url}')
                response = await requests.get(url)
                # logger.info(f'config json={response.json()}')

                config = Config.parse_obj(response.json())
                # logger.info(f'config={config.json(ensure_ascii=False, indent=2)}')
                settopapp.set_config(config)

                await send_command(websocket, ResponseStbAddConfig(id=id))
            elif cmd == CmdEnum.stb_off:
                o = RequestStbOff(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                await send_command(websocket, ResponseStbOff(id=id))
                settopapp.stb_off()
            elif cmd == CmdEnum.stb_reboot:
                o = RequestStbReboot(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                await send_command(websocket, ResponseStbReboot(id=id))
                settopapp.stb_reboot()
            elif cmd == CmdEnum.stb_auto_on_off:
                o = RequestStbAutoOnOff(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                settopapp.set_auto_on_off()
                await send_command(websocket, ResponseStbAutoOnOff(id=id))
            elif cmd == CmdEnum.stb_set_volume:
                o = RequestStbSetVolume(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                await settopapp.async_set_volume()
                await send_command(websocket, ResponseStbSetVolume(id=id))
            elif cmd == CmdEnum.tv_on:
                o = RequestTVOn(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                await settopapp.async_tv_on()
                await send_command(websocket, ResponseTVOn(id=id))
            elif cmd == CmdEnum.tv_off:
                o = RequestTVOff(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                await settopapp.async_tv_off()
                await send_command(websocket, ResponseTVOff(id=id))
            elif cmd == CmdEnum.stb_play_stat:
                o = RequestPlayStat(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                ################
                # TODO - TV관련 정보 넣을것!!
                ################

                try:
                    resource_usage = get_resource_usage(paths=o.dirSizes)
                    response_play_stat = ResponsePlayStat(id=id, **resource_usage.__dict__)
                except Exception as e:
                    logger.exception(f'fail get_resource_usage')
                    error_reason = str(e)
                    response_play_stat = ResponsePlayStat(id=id, reason=error_reason, result=False)

                await send_command(websocket, response_play_stat)
            elif cmd == CmdEnum.stb_screen_image:
                o = RequestStbScreenImage(**data)

                logger.info(f'{o.__class__.__name__} | {o}')
                response_stb_screen_image = ResponseStbScreenImage(id=id)
                try:
                    ratio = o.ratio
                    data_screen_shot_result: DataScreenShotResult = settopapp.screen_shot(ratio=ratio)
                    response_stb_screen_image.__dict__.update(data_screen_shot_result.__dict__)
                except Exception as e:
                    logger.exception('fail stb_screen_image')
                    response_stb_screen_image.result = False
                    response_stb_screen_image.reason = str(e)

                await send_command(websocket, response_stb_screen_image)
            elif cmd == CmdEnum.stb_log:
                o = RequestStbLog(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                # TODO
                await send_command(websocket, ResponseStbLog(id=id))
            elif cmd == CmdEnum.stb_update_sw:
                o = RequestStbUpdateSW(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                await send_command(websocket, ResponseStbUpdateSW(id=id))
            elif cmd == CmdEnum.stb_live_on:
                o = RequestStbLiveOn(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                settopapp.live_on(o)
                await send_command(websocket, ResponseStbLiveOn(id=id))
            elif cmd == CmdEnum.stb_live_off:
                o = RequestStbLiveOff(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
                settopapp.live_off(o)
                await send_command(websocket, ResponseStbLiveOff(id=id))
            elif cmd == CmdEnum.stb_add_schedule:

                o = RequestStbAddSchedule(**data)
                logger.info(f'{o.__class__.__name__} | {o}')

                schedules: List[Schedule] = []
                for schedule in o.schedules:
                    url = schedule.url
                    logger.info(f'downloading schedule : url={url}')
                    response = await requests.get(url)
                    # logger.info(f'schedule json={response.json()}')

                    play_set = PlaySet.parse_obj(response.json())
                    # logger.info(f'play_set={play_set}')
                    schedule.playset = play_set
                    schedules.append(schedule)

                settopapp.set_schedules(schedules)

                await send_command(websocket, ResponseStbAddSchedule(id=id))
            elif cmd == CmdEnum.stb_add_transfer:
                o = RequestStbAddTransfer(**data)
                logger.info(f'{o.__class__.__name__} | {o}')

                transfers: List[Transfer] = []

                for transfer in o.transfers:
                    name = transfer.name
                    url = transfer.url
                    logger.info(f'downloading transfer : name={name}|url={url}')
                    response = await requests.get(url)
                    logger.info(f'transfer json={response.json()}')

                    transfer_set = TransferSet.parse_obj(response.json())
                    # logger.info(f'transfer_set as json={transfer_set.json(ensure_ascii=True, indent=2)}')

                    transfer.transfer_set = transfer_set
                    transfers.append(transfer)

                settopapp.set_transfers(transfers)

                await send_command(websocket, ResponseStbAddTransfer(id=id))

                await send_command(websocket, RequestStbTransferList(id=id, fileVer=settopapp.getFileVer(),
                                                                     files=settopapp.get_nofiles()))

            elif cmd == CmdEnum.stb_transfer_list:
                o = ServerResponseStbTransferList(**data)
                logger.info(f'{o.__class__.__name__} | {o}')
            elif cmd == CmdEnum.keep_alive:
                await send_command(websocket, ResponseKeepAlive(id=id))
            else:
                logger.error(f"UNKNOWN CMD : cmd={cmd}|data={data}")


# DEFAULT_URL = 'ws://10.10.10.103:8000/ws/client'
# DEFAULT_URL = 'ws://127.0.0.1:8000/ws/client'

# DEFAULT_URL = 'ws://127.0.0.1:8000/ws/client'
# DEFAULT_URL = 'ws://106.248.161.215:8000/ws/client'
# DEFAULT_URL = 'ws://172.18.141.176:8000/ws/client'


DEV = True

# DEFAULT_ID = '2c:94:64:02:e1:5c'


if DEV:
    DEFAULT_ID = '2c:94:64:02:e1:62'
    DEFAULT_URL = 'ws://127.0.0.1:8000/ws/client'
    DEFAULT_URL = 'ws://castware.cf:8000/ws/client'

else:
    DEFAULT_ID = NicInfos.load().device_id
    DEFAULT_URL = 'ws://172.18.141.23:8000/ws/client'
    DEFAULT_URL = 'ws://castware.cf:8000/ws/client'


# DEFAULT_ID = 'aa:94:64:02:e1:5c'


@click.command()
# @click.option('--url', default='ws://127.0.0.1:8000/ws/client', help='Number of greetings.')
@click.option('--url', default=DEFAULT_URL, )
# @click.option('--url', default='ws://castware.cf:8000/ws/client', help='Number of greetings.')
@click.option('--id', default=DEFAULT_ID, help='The person to greet.')
def main(url: str, id: str):
    logger.info(f"start client : url={url} | id={id}")
    logger.info(f"CMD= stbc --url={url}  --id={id}")

    exit_when_error = False

    # url = 'ws://127.0.0.1:8000/ws/client'
    # id = "0"

    settopapp = SettopApp()

    while 1:
        # noinspection PyBroadException
        try:
            asyncio.get_event_loop().run_until_complete(client_loop(url=url, id=id, settopapp=settopapp))
        except (SystemExit, KeyboardInterrupt):
            logger.info('stop client')
            break
        except ConnectionRefusedError:
            logger.error('connection fail')
        except Exception:
            logger.exception('error while client')

        if exit_when_error:
            break
        else:

            # 오류 발생 시 재시작
            nsleep = 5
            logger.info(f'wait {nsleep} second')
            time.sleep(nsleep)


if __name__ == '__main__':
    main()

    # settopapp = SettopApp()
    # settopapp.json()
