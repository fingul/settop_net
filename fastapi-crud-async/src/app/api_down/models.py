from pydantic import BaseModel, Field


class DownSchema(BaseModel):
    url: str = Field(..., min_length=1, max_length=1024)
    path: str = Field(..., min_length=1, max_length=1024)
    size_confirm: int = 0
    try_count: int = 0
    progress: int = 0
    cancel: bool = False
    done: bool = False
    # description: str = Field(..., min_length=3, max_length=50)


class DownDB(DownSchema):
    id: int
