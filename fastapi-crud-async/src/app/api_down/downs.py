from typing import List

from fastapi import APIRouter, HTTPException, Path

from app.api_down import crud
from app.api_down.models import DownDB, DownSchema

router = APIRouter()


@router.post("/", response_model=DownDB, status_code=201)
async def create_item(payload: DownSchema):
    id = await crud.post(payload)
    item = await crud.get(id)
    return item


@router.get("/{id}/", response_model=DownDB)
async def read_item(id: int = Path(..., gt=0), ):
    note = await crud.get(id)
    if not note:
        raise HTTPException(status_code=404, detail="Note not found")
    return note


@router.get("/", response_model=List[DownDB])
async def read_all_items():
    return await crud.get_all()


# @router.put("/{id}/", response_model=DownDB)
# async def update_item(payload: DownSchema, id: int = Path(..., gt=0), ):
#     note = await crud.get(id)
#     if not note:
#         raise HTTPException(status_code=404, detail="Note not found")
#
#     note_id = await crud.put(id, payload)
#
#     response_object = {
#         "id": note_id,
#         "title": payload.title,
#         "description": payload.description,
#     }
#     return response_object


@router.delete("/{id}/", response_model=DownDB)
async def delete_note(id: int = Path(..., gt=0)):
    note = await crud.get(id)
    if not note:
        raise HTTPException(status_code=404, detail="Note not found")

    await crud.delete(id)

    return note
