from app.api_down.models import DownSchema
from app.db import downs, database


async def post(payload: DownSchema):
    query = downs.insert().values(payload.dict())
    return await database.execute(query=query)


async def get(id: int):
    query = downs.select().where(id == downs.c.id)
    return await database.fetch_one(query=query)


async def get_all():
    query = downs.select()
    return await database.fetch_all(query=query)


# async def put(id: int, payload: DownSchema):
#     query = (
#         downs
#         .update()
#         .where(id == notes.c.id)
#         .values(title=payload.title, description=payload.description)
#         .returning(notes.c.id)
#     )
#     return await database.execute(query=query)


async def delete(id: int):
    query = downs.delete().where(id == downs.c.id)
    return await database.execute(query=query)
