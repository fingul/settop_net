import os

from databases import Database
from sqlalchemy import (Column, DateTime, Integer, MetaData, String, Table,
                        create_engine, Boolean)
from sqlalchemy.sql import func

DATABASE_URL = os.getenv("DATABASE_URL", 'sqlite:////Users/m/1.sqlite')

# SQLAlchemy
engine = create_engine(DATABASE_URL)
metadata = MetaData()
notes = Table(
    "notes",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("title", String(50)),
    Column("description", String(50)),
    Column("created_date", DateTime, default=func.now(), nullable=False),
)

downs = Table(
    "downs",
    metadata,
    Column("id", Integer, primary_key=True),

    Column("url", String(1024)),
    Column("path", String(1024)),

    Column("size_confirm", Integer, default=0),
    Column("try_count", Integer, default=0),
    Column("progress", Integer, default=0),
    Column("cancel", Boolean, default=False, index=True),
    Column("done", Boolean, default=False, index=True),

    Column("created_date", DateTime, default=func.now(), nullable=False, index=True),
)

# databases query builder
database = Database(DATABASE_URL)
