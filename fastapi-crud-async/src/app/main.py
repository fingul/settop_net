import uvicorn
from fastapi import FastAPI

from app.api import notes, ping
from app.api_down import downs
from app.db import database, engine, metadata

metadata.create_all(engine)

app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


app.include_router(ping.router)
app.include_router(notes.router, prefix="/notes", tags=["notes"])
app.include_router(downs.router, prefix="/downs", tags=["downs"])


def entry():
    # await main2()
    uvicorn.run('main:app', host="0.0.0.0", port=8000, log_level="info", reload=True)

    # loop.close()


if __name__ == "__main__":
    # https://testdriven.io/blog/fastapi-crud/
    entry()
