import asyncio
import time
from typing import List, Any, Optional
from uuid import uuid4

import aioprocessing
import uvicorn
from fastapi import FastAPI
from loguru import logger
from pydantic import BaseModel

app = FastAPI()


class Job(BaseModel):
    id: str = ''
    n: int
    done: bool = False
    cancel: bool = False


class JobManager:
    jobs: List[Job] = []

    def __init__(__pydantic_self__, **data: Any) -> None:
        __pydantic_self__.q_command = aioprocessing.AioQueue()
        __pydantic_self__.q_response = aioprocessing.AioQueue()
        super().__init__(**data)

    def add_job(self, job: Job):
        self.jobs.append(job)

    async def run(self):

        running = False

        while 1:

            if not running:

                jj = None
                for j in self.jobs:
                    if not j.done:
                        jj = j
                        break
                if jj:
                    self.q_command.coro_put(jj)

            logger.info('sleep async')
            await asyncio.sleep(0.5)

        pass
        # run_controller(q_command, q_response)


def downloader(jm: JobManager):
    while True:
        logger.debug(f'wait')
        j: Optional[Job] = jm.q_command.get()
        logger.debug(f'get i={j}')
        if j:
            for n in range(j.n):
                r = jm.q_command.get(block=False, timeout=0)
                logger.debug(f'get r={r}')
                time.sleep(1)
            jm.q_response.put(0)
        else:
            logger.debug('exit downloader')
            break

    # q_command.close()


# async def run_downloader(jm: JobManager):
#     p = aioprocessing.AioProcess(target=downloader, args=[jm])
#     p.start()
#     # while True:
#     #     result = await queue.coro_get()
#     #     if result is None:
#     #         break
#     #     print("Got result {}".format(result))
#     await p.coro_join()


# async def run_controller(q_command, q_response):
#     # await event.coro_wait()
#     # async with lock:
#
#     await q_command.coro_put(1)
#     await q_command.coro_put(1)
#     await q_command.coro_put(1)
#     await q_command.coro_put(None)  # Shut down the worker


app = FastAPI()
jm = JobManager()


@app.post('/job/', response_model=Job)
async def job_add(j: Job) -> Job:
    j.id = str(uuid4())
    logger.info(f'job_add : j={j}')

    jm.add_job(j)
    return j


@app.get('/job/', response_model=List[Job])
async def job_list():
    return jm.jobs


# @app.on_event("startup")
# async def startup_event():
#     loop = asyncio.get_running_loop()
#     asyncio.ensure_future(run_downloader(jm), loop=loop)
#     asyncio.ensure_future(jm.run(), loop=loop)


# https://github.com/tiangolo/fastapi/issues/617#issuecomment-548385849
class Service(object):
    def __init__(self):
        self.counter = 0
        self.p = None

    def startup(self):
        logger.debug("service startup")
        self.task = asyncio.create_task(self.tick())
        self.p = aioprocessing.AioProcess(target=downloader, args=[jm])
        self.p.start()

    def shutdown(self):
        self.task.cancel()
        logger.debug("service shutdown")
        if self.p:
            self.p.terminate()
            self.p = None

    async def tick(self) -> None:
        while True:
            self.counter += 1
            await asyncio.sleep(1)
            logger.debug(f"counter is at: {self.counter}")


@app.on_event("startup")
async def startup():
    service = Service()
    service.startup()
    app.state.service = service
    logger.debug('end startup lifespan')


@app.on_event("shutdown")
async def shutdown():
    service = app.state.service
    service.shutdown()

    # await loop.gather(tasks)

    # await asyncio.wait(tasks)


def entry():
    # await main2()
    uvicorn.run('aio_run2:app', host="0.0.0.0", port=8000, log_level="info", reload=True)

    # loop.close()


if __name__ == "__main__":
    entry()
