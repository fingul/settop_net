import asyncio
import os
import platform
import sys
from pathlib import Path

import uvicorn
from PyQt5.QtCore import QPoint, QSize, QRect, QUrl
from PyQt5.QtWidgets import (QWidget, QHBoxLayout, QVBoxLayout, QApplication, QTextEdit)
from loguru import logger
from quamash import QEventLoop
from starlette.middleware.cors import CORSMiddleware

import glob_files
from InfinitePlayer import IPlayer
from mod_setup_signal import setup_signal
from patch_uvicorn_event_loop import patch_uvicorn_event_loop
from setup_environment_variable_display import setup_environment_variable_display

# QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
# QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
# QtWebEngine.initialize()
# QWebEngineView.init

patch_uvicorn_event_loop()

if platform.system() == "Darwin":  # for MacOS

    class QWebEngineView(QTextEdit):

        def __init__(self, *__args):
            logger.info(f'using mock video')
            super().__init__(*__args)

        def load(self, url: QUrl):
            self.setText(str(url))

        def setZoomFactor(self, factor: float):
            pass



else:
    from PyQt5.QtWebEngineWidgets import QWebEngineView


class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):
        ###########
        # TOP
        ###########
        self.top = QHBoxLayout()
        self.web_top = QWebEngineView()

        # Path().joinpath()

        self.web_top.load(QUrl(
            Path(__file__).parent.joinpath('./sample_web/digital-clock-with-vue-js/index.html').absolute().as_uri()))
        # lbl = QLabel()
        # lbl.setText('a')
        # lbl.setStyleSheet(f"QLabel {{ background-color : red; }}");
        self.top.addWidget(self.web_top)

        ###########
        # MID
        ###########
        self.mid = QHBoxLayout()

        # lbl = QLabel()
        # lbl.setText('a')
        # lbl.setStyleSheet(f"QLabel {{ background-color : green; }}");
        #

        #############################
        # MID - PLAYER
        #############################

        self.player = IPlayer()

        self.mid.addWidget(self.player.videoframe, stretch=3)

        ###############################################################################

        # QTimer.singleShot(1000, playme)

        self.web_mid = QWebEngineView()

        self.web_mid.load(
            QUrl('http://bstv.busanbank.co.kr/finance/finance.html?q=http://finance.naver.com/sise/sise_index.nhn'))
        # w.load(QUrl('http://bstv.busanbank.co.kr/weather/seoul/'))
        # w.load(QUrl('https://adrianiskander.github.io/ticker/'))
        # w.load(QUrl('http://chosun.com'))
        # w.load(QUrl('http://naver.com'))
        # self.web_mid.setZoomFactor(2)
        self.mid.addWidget(self.web_mid, stretch=1)

        ###########
        # BOTTOM
        ###########

        self.down = QHBoxLayout()
        self.web_down = QWebEngineView()
        # https://codepen.io/fingul/pen/XGoYrZ

        # self.web_down.load(QUrl(
        #      Path(__file__).parent.joinpath('./sample_web/news-ticker/index.html').absolute().as_uri()))
        # w.load(QUrl('about:blank'))
        self.web_down.load(QUrl(
            'http://bstv.busanbank.co.kr/news_ytn/news_ticker.html?q=http://news.einfomax.co.kr/rss/allArticle.xml'))

        # self.web_down.setZoomFactor(1.5)

        self.down.addWidget(self.web_down)

        # # okButton = QPushButton("OK")
        # # cancelButton = QPushButton("Cancel")
        # #
        # # lbl = QLabel(self)
        # # lbl.setText('a')
        # # lbl.setStyleSheet(f"QLabel {{ background-color : red; }}");
        #
        # hbox = QHBoxLayout()
        # hbox.addStretch(1)
        # hbox.addWidget(okButton)
        # hbox.addWidget(cancelButton)
        # hbox.addWidget(lbl)

        self.vbox = QVBoxLayout()
        # vbox.addStretch(1)
        self.vbox.addLayout(self.top, stretch=2)
        self.vbox.addLayout(self.mid, stretch=13)
        self.vbox.addLayout(self.down, stretch=2)

        self.vbox.setContentsMargins(0, 0, 0, 0)
        self.vbox.setSpacing(2)

        self.setLayout(self.vbox)

        self.setGeometry(QRect(QPoint(0, 0), QSize(1920 / 2, 1080 / 2)))
        self.setGeometry(QRect(QPoint(0, 0), QSize(1920, 1080)))
        self.setWindowTitle('Buttons')
        self.show()
        # self.showFullScreen()

        # path = str(Path('~/ad.mp4').expanduser())
        path = str(Path('~/media_settop/10.mp4').expanduser())
        # path="/Users/m/media_settop/3.mp4"
        self.player.open_file(path)

        # QTimer.singleShot(3000, self.play)


def make_fast_api(ex):

    from fastapi import FastAPI

    app = FastAPI()
    # app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=ALL_METHODS)
    app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['*'])

    @app.get("/")
    async def read_root(q: str = None):
        logger.info(f'q={q}')
        if q:
            # w.setUrl(QUrl(q))
            ex.player.open_file(q)
        return {"Hello": "World"}



    # @app.get("/vol/get")
    # async def vol():
    #     level, mute = control_volume.get_volume()
    #     return dict(level=level)
    #
    #
    # @app.get("/vol/set")
    # async def vol(level: int):
    #     control_volume.set_volume(level=level)
    #
    #
    # @app.get("/vol/play")
    # async def vol_play():
    #     control_volume.play_volume()

    @app.get("/system")
    async def system(cmd: str = None):
        logger.info(f'cmd={cmd}')
        if cmd:
            # w.setUrl(QUrl(q))
            os.system(cmd)
        return {"Hello": "World"}

    @app.get("/web/top")
    async def web_top(url: str):
        ex.web_top.load(QUrl(url))

    @app.get("/web/mid")
    async def web_top(url: str):
        ex.web_mid.load(QUrl(url))

    @app.get("/web/down")
    async def web_top(url: str):
        ex.web_down.load(QUrl(url))

    @app.get("/list_file")
    async def play_file(root: str = None):

        files = glob_files.get_file(root)
        return files

        # logger.info(f'q={q}')
        # if q:
        #     path = str(Path(q).expanduser())
        #     ex.player.open_file(path)
        # return {"Hello": "World"}

    @app.get("/play/file")
    async def play_file(path: str = None, root: str = None):

        if not root:
            logger.info(f'root_path not given, using default DEFAULT_MEDIA_ROOT={glob_files.DEFAULT_MEDIA_ROOT}')
            root = glob_files.DEFAULT_MEDIA_ROOT

        if path:
            full_path = Path(root).joinpath(path).expanduser()
            if not full_path.is_file():
                logger.error(f'full_path={full_path} is not file')
                return
            else:
                logger.info(f'ex.player.open_file={full_path}')
                ex.player.open_file(str(full_path))

        return {"Hello": "World"}

    @app.get("/play/url")
    async def play_url(q: str = None):
        logger.info(f'q={q}')
        if q:
            # path = str(Path(q).expanduser())
            ex.player.open_file(q)
        return {"Hello": "World"}

    @app.get("/play/youtube")
    async def play_youtube(q: str = None):
        logger.info(f'q={q}')
        if q:
            # path = str(Path(q).expanduser())
            ex.player.open_youtube(q)
        return {"Hello": "World"}

    @app.get("/show/full")
    async def fullscreen():
        ex.showFullScreen()
        return {"Hello": "World"}

    @app.get("/show/normal")
    async def fullscreen():
        ex.showNormal()
        return {"Hello": "World"}

    @app.get("/show/toggle")
    async def toggle():
        if ex.isFullScreen():
            ex.showNormal()
        else:
            ex.showFullScreen()
        return {"Hello": "World"}

    return app


if __name__ == '__main__':



    qapp = QApplication(sys.argv)
    # QtWebEngine.QtWebEngine.initialize()
    # qapp.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
    # qapp.setAttribute(QtCore.Qt.AA_UseSoftwareOpenGL)

    loop = QEventLoop(qapp)
    asyncio.set_event_loop(loop)  # NEW must set the event loop

    ex = Example()

    # ex2 = Example()
    # sys.exit(app.exec_())

    app = make_fast_api(ex)  #

    setup_environment_variable_display()
    try:

        port = int(os.environ.get('PORT', 8000))
        uvicorn.run(app, host='0.0.0.0', port=port, reload=False, reload_dirs=str(Path(__file__).parent.absolute()))
    except RuntimeError:
        logger.exception('ignore runtime error exit')
        pass

    '''
    http http://0.0.0.0:8000/play/file?path=3.mp4 
    
    '''
