from pathlib import Path

from loguru import logger

DEFAULT_MEDIA_ROOT = '~/media_settop'


def toRelative(i: Path, root):
    return str(i.relative_to(root))
    # return i


def validFile(i: Path):
    if i.name.startswith('.'):
        logger.info(f'SKIP path={i} : start with .')
        return False
    if not i.is_file():
        logger.info(f'SKIP path={i} : not file')
        return False

    try:

        if i.stat().st_size <= 0:
            logger.info(f'SKIP path={i} : file size zero')
            return False
    except Exception:
        logger.exception(f'SKIP path={i} : fail stat')

        return False
    return True


def get_file(root_path=None):
    if not root_path:
        logger.info(f'root_path not given, using default DEFAULT_MEDIA_ROOT={DEFAULT_MEDIA_ROOT}')
        root_path = DEFAULT_MEDIA_ROOT

    root: Path = Path(root_path).expanduser()

    files = root.rglob('*.*')
    files = filter(validFile, files)
    files = map(lambda i: toRelative(i, root), files)
    files = list(files)

    logger.info(f'get_file={files}')

    return files


if __name__ == '__main__':
    get_file()
