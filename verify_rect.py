from functools import reduce
from itertools import combinations
from typing import List

from PyQt5.QtCore import QRect, QPoint, QSize
from PyQt5.QtGui import QColor


from loguru import logger

# https://doc.qt.io/qtforpython/PySide2/QtCore/QRect.html


def _check_collision(r1: QRect, r2: QRect) -> bool:
    r_i = r1.intersected(r2)

    if r_i.isEmpty():
        logger.info(f'COLLISION OK : r1={r1}|r2={r2}')
        return True
    else:
        logger.error(f'COLLISION ERROR: r1={r1}|r2={r2}')
        return False


def check_collisions(rs: List[QRect]):
    logger.info(f'>> check_collisions begin')

    united = reduce((lambda x, y: x.united(y)), rs, QRect())
    logger.info(f'united={united}')

    rr_combinations = list(combinations(rs, 2))

    [_check_collision(*rr_combination) for rr_combination in rr_combinations]

    logger.info(f'>> check_collisions end')


# for x in rs:
#     for y in rs:
#         check_collision(x, y)

if __name__ == '__main__':
    logger.info(list(combinations(range(0, 3), 2)))

    r1 = QRect(QPoint(0, 0), QSize(1, 1))
    r2 = QRect(QPoint(1, 1), QSize(1, 1))
    r3 = QRect(QPoint(2, 2), QSize(1, 1))

    r_i = r1.intersected(r2)

    logger.info(f'r_i={r_i}|r_i.isEmpty()={r_i.isEmpty()}')

    rs = [r1, r2, r3]

    check_collisions(rs)

    check_collisions([r1, r1, r2, r3])

    logger.info(QColor.colorNames())

    # logger.info(f'rs={rs}')
