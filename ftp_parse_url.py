from pathlib import Path
from typing import Optional
from urllib.parse import quote_plus, urljoin

from loguru import logger
from pydantic import BaseModel
from urllib3.util import parse_url


class FtpPath(BaseModel):
    host: str
    port: int = 21
    path: str = ''
    user: Optional[str] = None
    password: Optional[str] = None

    @classmethod
    def from_url(cls, url: str, path: Optional[str] = None, user: Optional[str] = None, password: Optional[str] = None):
        parsed = parse_url(url)
        host = parsed.host
        port = parsed.port or 21
        path = path or parsed.path

        return FtpPath(host=host, port=port, path=path, user=user, password=password)

    @property
    def name(self):
        return Path(self.path).name

    def to_url(self):
        user_password_part = ''
        if self.user and self.password:
            user_password_part = f'{quote_plus(self.user)}:{quote_plus(self.password)}@'

        base = f'ftp://{user_password_part}{self.host}:{self.port}'

        return urljoin(base, self.path)

        # return f'{self.path}'


if __name__ == '__main__':
    # logger.debug(FtpPath.from_url(url='ftp://castware.cf:30005', user='0', password='0'))
    # logger.debug(FtpPath.from_url(url='ftp://0.0.0.0', user='0', password='0'))
    # logger.debug(FtpPath.from_url(url='ftp://0.0.0.0:21000', user='0', password='0'))
    logger.debug(
        FtpPath.from_url(url='ftp://cast@castware.cf:30005/2020/01/02/1199040.MP4', user='cast',
                         password='cast123!@#').to_url())

    logger.debug(
        FtpPath.from_url(url='ftp://cast@castware.cf:30005', path='2020/01/02/1199040.MP4', user='cast',
                         password='cast123!@#').to_url())

    logger.debug(
        FtpPath.from_url(url='ftp://cast@castware.cf:30005/', path='/2020/01/02/1199040.MP4', user='cast',
                         password='cast123!@#').to_url())

    ftp_path = FtpPath.from_url(url='ftp://cast@castware.cf:30005', path='/2020/01/02/1199040.MP4', user='cast',
                     password='cast123!@#')


    logger.info(f'ftp_path.name={ftp_path.name}')
    logger.info(quote_plus('@'))
