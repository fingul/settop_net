import datetime
from enum import Enum
from pathlib import Path
from typing import List, Optional, Dict

from loguru import logger
from pydantic import BaseModel


class FTPConnection(BaseModel):
    # FTP 접속 정보
    address: str
    passive: Optional[bool] = True
    id: Optional[str]
    password: Optional[str]
    # base_path: str
    priority: int = 0  # 높은 순으로 먼저 적용합니다.


class DownloadTime(BaseModel):
    # 다운로드 제한 시간
    start: datetime.time  # 다운로드 시작 시간
    end: datetime.time  # 다운로드 종료 시간
    limit_byte_per_second = 0  # 0 이면 속도 제한을 하지 않습니다.
    priority: int = 0  # 높은 순으로 먼저 적용합니다.


class DownloadLimit(BaseModel):
    comment: str = ''
    start_time: datetime.time
    end_time: datetime.time
    limit_byte_per_sec: int


class DayEnum(str, Enum):
    MONDAY = 'monday'
    TUESDAY = 'tuesday'
    WEDNESDAY = 'wednesday'
    THURSDAY = 'thursday'
    FRIDAY = 'friday'
    SATURDAY = 'saturday'
    SUNDAY = 'sunday'


class DownloadPolicy(BaseModel):
    unlimited_dates: List[datetime.date]
    limited_days: Dict[DayEnum, DownloadLimit]


# @@
class Config(BaseModel):
    timezone: str  # 타임존
    ftp_connections_cms: List[FTPConnection] = []  # ftp 접속 정보 목록 = CMS용
    ftp_connections_sch: List[FTPConnection] = []  # ftp 접속 정보 목록 = 송출용
    download_policy: DownloadPolicy  # 다운로드 제한 시간 목록
    cleanup_time: Optional[datetime.time] = None  # 시스템 정리 시간

    comment: Optional[str] = None  # comment (필수 아님)


def get_sample_config() -> Config:
    ftp_connections_cms = [
        FTPConnection(address='ftps://1.2.3.4:5789', id='username1', password='supersecret', base_path='/storage1',
                      priority=1),
        # FTPConnection(address='ftp://4.5.6.7:1234', id='username2', password='supersecret', base_path='/storage2',
        #               priority=2, passive=False),
        # FTPConnection(address='sftp://4.5.6.7:1234', id='username2', password='supersecret', base_path='/storage2',
        #               priority=2),

    ]

    ftp_connections_sch = [
        FTPConnection(address='ftps://1.2.3.4:5789', id='username1', password='supersecret', base_path='/storage1',
                      priority=1),
        # FTPConnection(address='ftp://4.5.6.7:1234', id='username2', password='supersecret', base_path='/storage2',
        #               priority=2, passive=False),
        # FTPConnection(address='sftp://4.5.6.7:1234', id='username2', password='supersecret', base_path='/storage2',
        #               priority=2),

    ]

    comment = "config example"

    limit_normal_date = DownloadLimit(start_time=datetime.time(hour=9, minute=0, second=0),
                                      end_time=datetime.time(hour=18, minute=0, second=0),
                                      limit_byte_per_sec=1024 * 1024
                                      )

    download_policy = DownloadPolicy(
        unlimited_dates=[
            datetime.date(year=2020, month=1, day=1),
            datetime.date(year=2020, month=1, day=2),
        ],
        limited_days={
            DayEnum.MONDAY: limit_normal_date,
            DayEnum.TUESDAY: limit_normal_date,
            DayEnum.WEDNESDAY: limit_normal_date,
            DayEnum.THURSDAY: limit_normal_date,
            DayEnum.FRIDAY: limit_normal_date,
        }
    )

    config = Config(timezone='Asia/Seoul', download_policy=download_policy, comment=comment,
                    ftp_connections_cms=ftp_connections_cms, ftp_connections_sch=ftp_connections_sch,
                    cleanup_time=datetime.time(hour=19, minute=0, second=0))

    return config


if __name__ == '__main__':
    logger.info('timezone이 현재 시스템의 타임존과 다를 경우 타임존을 변경한 후 시스템을 재시작합니다. (필수)')
    logger.info('download_times 는 비어 있을 수 있으며, 그럴 경우 시간 및 속도를 제어하지 않습니다.')
    logger.info('cleanup_time 은 비어 있을 수 있으며, 그럴 경우 시스템 정리를 하지 않습니다.')

    sample_dir: Path = Path(__file__).parent.joinpath('sample')

    sample_dir.mkdir(parents=True, exist_ok=True)

    #############
    # 예제
    #############

    config = get_sample_config()

    str_config_json = config.json(indent=2)

    logger.info(f'str_config_json={str_config_json}')

    config_sample_path: Path = sample_dir.joinpath('config_sample.json')

    logger.info(f'write sample to config_sample_path={config_sample_path}')

    config_sample_path.write_text(str_config_json, encoding='utf8')

    # logger.info(f'config={config.json(indent=2)}')
    #
    # comment = "cleanup_time example"
    # config = Config(timezone='Asia/Seoul', cleanup_time=datetime.time(hour=6, minute=7, second=8), comment=comment,
    #                 ftp_connections=ftp_connections)
    #
    # logger.info(f'config={config.json(indent=2)}')
