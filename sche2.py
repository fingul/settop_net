import datetime
import time

from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.jobstores.memory import MemoryJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from loguru import logger

jobstores = {
    # 'mongo': MongoDBJobStore(),
    # 'default': SQLAlchemyJobStore(url='sqlite:///jobs.sqlite')
    'default': MemoryJobStore()
}
executors = {
    'default': ThreadPoolExecutor(1),
    # 'processpool': ProcessPoolExecutor(5)
}
job_defaults = {
    'coalesce': False,
    'max_instances': 1
}


def cb(*args, **kwargs):
    logger.debug("cb")
    pass


class A():

    def __init__(self) -> None:
        super().__init__()
        self.scheduler: BackgroundScheduler = None
        self.run()

    def run(self):
        logger.info('run')

        self.scheduler = BackgroundScheduler(jobstores=jobstores, executors=executors, job_defaults=job_defaults)

        for i in range(5):
            self.scheduler.add_job(cb, args=[], kwargs=dict(a=1), trigger='date',
                                   next_run_time=datetime.datetime.now() + datetime.timedelta(seconds=i), )

        self.scheduler.start()

    def close(self):
        logger.info('close')
        if self.scheduler:
            logger.info('remove')
            self.scheduler.shutdown(wait=True)
            self.scheduler = None

    def __del__(self):
        self.close()


r1 = A()
r1.close()
r2 = A()
r2.close()

time.sleep(3)
