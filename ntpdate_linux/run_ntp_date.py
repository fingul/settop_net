import shlex
from pathlib import Path
from subprocess import STDOUT, check_output

from loguru import logger
from pandas.compat import is_platform_mac


def update_npt(time_server: str = 'time.google.com'):
    if is_platform_mac():
        logger.info("MAC SKIP update_npt")
        return

    # noinspection PyBroadException
    try:

        logger.info(f"TRY update_npt : time_server={time_server}")

        path_bin = Path(__file__).parent.joinpath('ntpdate')

        path_bin.chmod(777)

        logger.info(f"path_bin={path_bin}")

        cmd = f'sudo {path_bin} {time_server}'

        logger.info(f"cmd={cmd}")

        cmds = shlex.split(cmd)

        output = check_output(cmds, stderr=STDOUT, timeout=30)

        logger.info(f'DONE : output={output}')
    except:
        logger.error(f"FAIL update_npt : time_server={time_server}")


if __name__ == '__main__':
    update_npt('time.google.com1')
