import sys
from functools import reduce
from pathlib import Path
from typing import Optional, Dict

import PyQt5
from PyQt5 import QtCore
from PyQt5.QtCore import Qt, QUrl
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QWidget
from loguru import logger

from mod_setup_signal import setup_signal
from models_file import ContentTypeEnum

name_to_color = dict(
    main='grey',
    side='blue',
    bottom='green',
    top='orange',
)

result_name_rect = {'top': PyQt5.QtCore.QRect(0, 0, 480, 45), 'main': PyQt5.QtCore.QRect(0, 45, 360, 180),
                    'bottom': PyQt5.QtCore.QRect(0, 225, 360, 45), 'side': PyQt5.QtCore.QRect(360, 45, 120, 225)}


# result_name_rect={'top': PyQt5.QtCore.QRect(0, 0, 1920, 180), 'main': PyQt5.QtCore.QRect(0, 180, 1440, 720), 'bottom': PyQt5.QtCore.QRect(0, 900, 1440, 180), 'side': PyQt5.QtCore.QRect(1440, 180, 480, 900)}


class Content:
    type: ContentTypeEnum


# class VideoContent(Content):

class ImageContent(Content):

    def __init__(self, path: Path) -> None:
        super().__init__()
        self.type = ContentTypeEnum.IMAGE
        self.path = path


class URLContent(Content):

    def __init__(self, url: str) -> None:
        super().__init__()
        self.type = ContentTypeEnum.IMAGE
        self.url = url


class Region:

    def __init__(self, name, parent, rect) -> None:
        super().__init__()

        self.name = name
        self.parent = parent

        self.content_type: Optional[ContentTypeEnum] = None
        self.w: Optional[QWidget] = None
        self.rect: PyQt5.QtCore.QRect = rect

        self.w = self.create_label()

    def create_label(self) -> QWidget:
        w = QLabel(self.name, parent=self.parent)
        w.setAlignment(Qt.AlignCenter)
        w.setGeometry(self.rect)
        w.setStyleSheet(f"background: {name_to_color.get(self.name)}")
        self.content_type = None

    def create_url(self):

        if self.w:
            self.w.destroy()
            self.w = None

        w = QWebEngineView(parent=self.parent)
        # w.setAlignment(Qt.AlignCenter)
        w.setGeometry(self.rect)
        w.setUrl(QUrl('http://chosun.com'))
        self.w = w
        # w.setStyleSheet(f"background: {name_to_color.get(self.name)}")
        # return w

    def set_content(self, content: Content):

        if content:

            if content.type == ContentTypeEnum.IMAGE:
                self.create_label()
            else:
                self.create_url()
        else:
            self.create_url()

        # if self.content_type != content.type:

    def set_image(self):
        pass

    def set_url(self):
        pass

    def set_video(self, start_second: float):
        pass


class RegionManager:
    '''
    각 영역을 관리
    '''

    def __init__(self) -> None:
        super().__init__()
        self.regions: Dict[str, Region] = {}

    def add_region(self, name: str, region: Region):
        self.regions[name] = region

    def set_content(self, name: str, content: Content):
        region = self.regions.get(name)
        if region:
            region.set_content(content)
        else:
            logger.error(f'UNKNOWN REGION name={name} | content={content}')


class App(QMainWindow):

    def __init__(self):
        super().__init__()
        self.title = 'APP_TITLE'
        # self.left = 10
        # self.top = 10
        # self.width = 440
        # self.height = 280
        # self.rect = PyQt5.QtCore.QRect(0, 0, 100, 100)
        # self.region_manager = RegionManager()
        self.initUI()


    def keyPressEvent(self, e):
        logger.info(f'keyPressEvent : e={e} | e.key()={e.key()}| e.text()={e.text()}')
        text = e.text()
        if text == '1':
            w = QLabel("a", self)
            w.setAlignment(Qt.AlignCenter)
            w.setGeometry(PyQt5.QtCore.QRect(0, 0, 100, 100))
            w.setStyleSheet(f"background: grey")
            self.w = w

        elif text == '2':
            self.region_manager.set_content('main', None)
        self.show()

    def initUI(self):


        self.setWindowTitle(self.title)
        self.setGeometry(PyQt5.QtCore.QRect(0, 0, 500, 500))
        # self.fs = []

        # for name, rect in result_name_rect.items():
        #     # RegionManager(parent=self, rect=rect)
        #     self.region_manager.add_region(name, Region(name=name, parent=self, rect=rect))


        self.show()

        QtCore.QTimer.singleShot(0, self.after)

    def after(self):
        logger.info('boom')
        # self.showFullScreen()


if __name__ == '__main__':
    import os

    os.environ['DISPLAY'] = ':0.0'

    setup_signal()

    # def tick():
    #     print('tick')
    #
    #
    # timer = QTimer()
    # timer.timeout.connect(tick)
    # timer.start(1000)

    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
