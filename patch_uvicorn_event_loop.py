from loguru import logger
from uvicorn import Config


def patch_uvicorn_event_loop():
    logger.info('uvicon setup_event_loop patched')

    def setup_event_loop(self):
        logger.info('setup_event_loop : not running')

    Config.setup_event_loop = setup_event_loop
