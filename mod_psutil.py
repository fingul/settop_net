from pathlib import Path
from typing import List

import humanfriendly
import psutil
from loguru import logger

from models import DataResourceUsage, DataUsedTotal


def dir_size(path: str) -> int:
    # noinspection PyBroadException
    try:
        root_directory = Path(path)
        return sum(f.stat().st_size for f in root_directory.glob('**/*') if f.is_file())
    except:
        logger.exception(f'fail dir_size : path={path}')
        return -1


def get_resource_usage(paths: List[str] = []) -> DataResourceUsage:
    cpu: float = psutil.cpu_percent(interval=None)

    vm = psutil.virtual_memory()
    # sm = psutil.swap_memory()

    logger.debug(
        f'cpu={cpu}|vm={humanfriendly.format_size(vm.used)}/{humanfriendly.format_size(vm.total)}')

    logger.debug(
        f'cpu={cpu}|memory={vm.used}/{vm.total}')

    # 192.168.1.

    disk_usage = psutil.disk_usage('/')
    # logger.info(f'disk_usage={humanfriendly.format_size(disk_usage.used)}/{humanfriendly.format_size(disk_usage.total)}')
    logger.debug(f'disk_usage={disk_usage.used}/{disk_usage.total}')

    resource_usage = DataResourceUsage()
    resource_usage.cpu = cpu
    resource_usage.memory = DataUsedTotal(used=vm.used, total=vm.total)
    resource_usage.disk = DataUsedTotal(used=disk_usage.used, total=disk_usage.total)

    resource_usage.dirSizes = {path: dir_size(path) for path in paths}

    return resource_usage


if __name__ == '__main__':
    resource_usage = get_resource_usage(paths=["/tmp"])

    logger.info(f'get_resource_usage()={resource_usage}')
    logger.info(f'resource_usage.__dict__={resource_usage.__dict__}')

    n = dir_size(str(Path('/tmp').expanduser()))
    logger.info(f'n={n}')
