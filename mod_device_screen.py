import os
from typing import List, Optional

from loguru import logger
from screeninfo import get_monitors

from mod_setup_signal import setup_signal
from models_file import Size


def run_cmd(cmd):
    logger.info(f"run cmd : cmd={cmd}")
    # noinspection PyBroadException
    try:
        os.system(cmd)
    except Exception:
        logger.error(f"fail run cmd : cmd={cmd}")


def get_monitor_names() -> List[str]:
    return list([m.name for m in get_monitors()])


def set_resolution(sizes: Optional[List[Size]] = None):
    if sizes is None:
        sizes = [Size(width=1920, height=1080)]
    monitor_names = get_monitor_names()

    n_monitor_names = len(monitor_names)
    n_sizes = len(sizes)

    if n_monitor_names != n_sizes:
        logger.warning("current monitor and size length mismatch")
        logger.warning(f"monitor_names={monitor_names}")
        logger.warning(f"sizes={sizes}")

    monitor_name_sizes = list(zip(monitor_names, sizes))
    for monitor_name, size in monitor_name_sizes:
        run_cmd(f'xrandr --output {monitor_name} --mode {size.width}x{size.height}')
    # for size in sizes:


'''
xrandr --output DP1 --mode 1920x1080
xrandr --output DP2 --mode 1920x1080
xrandr --output HDMI1 --mode 1920x1080
xrandr --output HDMI2 --mode 1920x1080

'''


def main():
    for m in get_monitors():
        logger.info(f"m={m}")


if __name__ == '__main__':
    main()
    setup_signal()
    logger.info(f'monitor_names()={get_monitor_names()}')

    logger.debug(list(zip([1, 2], [3, 4, 5])))
    set_resolution()
