import uvicorn
from fastapi import FastAPI

import db_api
from db_db import engine, metadata, database

metadata.create_all(engine)

app = FastAPI()


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


app.include_router(db_api.router)

def entry():
    # await main2()
    uvicorn.run('db_web:app', host="0.0.0.0", port=8000, log_level="info", reload=True)

    # loop.close()


if __name__ == "__main__":
    # https://testdriven.io/blog/fastapi-crud/
    entry()

