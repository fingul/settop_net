from loguru import logger
from tzlocal import get_localzone

# local_tz = get_localzone()

if __name__ == '__main__':
    localzone = get_localzone()
    logger.info(f'localzone={localzone}')
