import asyncio
from typing import Set

import uvicorn
from fastapi import Cookie, FastAPI, Header
from loguru import logger
from rx import operators as ops
from rx.scheduler.eventloop import AsyncIOScheduler
from rx.subject import Subject
from starlette.responses import HTMLResponse
from starlette.status import WS_1008_POLICY_VIOLATION
from starlette.websockets import WebSocket

app = FastAPI()

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>WebSocket Chat</h1>
        <form action="" onsubmit="sendMessage(event)">
            <label>Item ID: <input type="text" id="itemId" autocomplete="off" value="foo"/></label>
            <button onclick="connect(event)">Connect</button>
            <br>
            <label>Message: <input type="text" id="messageText" autocomplete="off"/></label>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <script>
        var ws = null;
            function connect(event) {
                var input = document.getElementById("itemId")
                ws = new WebSocket("ws://localhost:8000/items/" + input.value + "/ws");
                ws.onmessage = function(event) {
                    var messages = document.getElementById('messages')
                    var message = document.createElement('li')
                    var content = document.createTextNode(event.data)
                    message.appendChild(content)
                    messages.appendChild(message)
                };
            }
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""


@app.get("/")
async def get():
    return HTMLResponse(html)


async def get_cookie_or_client(
        websocket: WebSocket, session: str = Cookie(None), x_client: str = Header(None)
):
    if session is None and x_client is None:
        await websocket.close(code=WS_1008_POLICY_VIOLATION)
    return session or x_client


@app.websocket("/")
async def websocket_endpoint(
        websocket: WebSocket,
        # item_id: int,
        # q: str = None,
        # cookie_or_client: str = Depends(get_cookie_or_client),
):
    await websocket.accept()

    scheduler = AsyncIOScheduler(asyncio.get_running_loop())

    # scheduler.schedule_periodic(1,lambda i:logger.info('xxx'))

    def scanner(state: Set, i) -> Set:
        n = state.copy()
        n.add(i)

        logger.info(f'reducer:n={n}')

        return n

    subject = Subject()

    composed = subject.pipe(
        # ops.map(lambda x: x["term"]),
        # ops.filter(lambda text: len(text) > 2),  # Only if the text is longer than 2 characters
        # ops.buffer_with_time(datetime.timedelta(seconds=1)),  # Pause for 750ms
        ops.scan(scanner, set()),
        # ops.distinct_until_changed(),            # Only if the value has changed
        # ops.flat_map_latest(search_wikipedia)
        ops.debounce(0.750),  # Pause for 750ms
        # ops.distinct_until_changed(),  # Only if the value has changed
        # ops.flat_map_latest(search_wikipedia)

    )

    def on_error(ex):
        logger.info(f'on_error={ex}')

    def send_response(x):
        logger.info(f'send_response={x}')

    composed.subscribe(on_next=send_response, on_error=on_error, scheduler=scheduler)

    while True:
        data = await websocket.receive_text()
        logger.info(data)
        subject.on_next(data)
        # await websocket.send_text(
        #     f"Session Cookie or X-Client Header value is: {cookie_or_client}"
        # )
        # if q is not None:
        #     await websocket.send_text(f"Query parameter q is: {q}")
        # await websocket.send_text(f"Message text was: {data}, for item ID: {item_id}")


def entry():
    uvicorn.run('fast_ws:app', host="0.0.0.0", port=8000, log_level="info", reload=True)


if __name__ == "__main__":
    entry()
