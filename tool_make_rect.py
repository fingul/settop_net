from functools import reduce
from typing import List

from PyQt5.QtCore import QSize, QRect, QPoint
from loguru import logger
from pandas.compat import is_platform_mac

from models_file import Layout, Rect, Size, Point, Content, RegionTypeEnum, ContentTypeEnum, TicketURLEnum

result = [
    ["top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top",
     "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top"],
    ["top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top",
     "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top"],
    ["top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top",
     "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top", "top"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "main",
     "main", "main", "main", "main", "main", "main", "main", "main", "main", "main", "side", "side", "side", "side",
     "side", "side", "side", "side"],
    ["bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom",
     "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom",
     "bottom", "bottom", "side", "side", "side", "side", "side", "side", "side", "side"],
    ["bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom",
     "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom",
     "bottom", "bottom", "side", "side", "side", "side", "side", "side", "side", "side"],
    ["bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom",
     "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom", "bottom",
     "bottom", "bottom", "side", "side", "side", "side", "side", "side", "side", "side"]]

import numpy


def result_to_name_rect(target_w, target_h, result):
    shape = numpy.array(result).shape

    logger.info(f'shape={shape}')

    col, row = shape

    logger.info(f'row={row}|col={col}')

    w = target_w / row
    h = target_h / col

    # logger.info(f'each_w={each_w}|each_h={each_h}')

    name_to_rect = {}
    for r in range(0, row):
        for c in range(0, col):
            v = result[c][r]
            l = name_to_rect.setdefault(v, [])
            l.append(QRect(QPoint(w * r, h * c), QSize(w, h)))

    # logger.info(f'name_to_rect={name_to_rect}')

    result_name_rect = {}
    for k, v in name_to_rect.items():
        rs: List[QRect] = v
        rect: QRect = reduce(lambda x, y: x.united(y), rs)
        result_name_rect[k] = rect

    logger.info(f'result_name_rect={result_name_rect}')

    return result_name_rect


def name_rect_to_layout(name: str, qrect: QRect) -> Layout:
    size = Size(width=qrect.width(), height=qrect.height())

    point = Point(x=qrect.left(), y=qrect.top())

    rect = Rect(size=size, point=point)

    NAME_TO_CONTENT = {
        "top": Content(file="0.jpg", type=ContentTypeEnum.IMAGE),
        "main": Content(file="60.mp4", type=ContentTypeEnum.VIDEO),
        "side": Content(file="http://google.com?q=side", type=ContentTypeEnum.URL),
        "bottom": Content(file="http://google.com?q=bottom", type=ContentTypeEnum.URL),
        # "side": Content(file="about:blank", type=ContentTypeEnum.URL),
        # "bottom": Content(file="about:blank", type=ContentTypeEnum.URL),
    }

    NAME_TO_REGIONTYPE = {
        "top": RegionTypeEnum.TOP,
        "main": RegionTypeEnum.MAIN,
        "side": RegionTypeEnum.SIDE,
        "bottom": RegionTypeEnum.BOTTOM,
    }

    content = NAME_TO_CONTENT[name]

    type = NAME_TO_REGIONTYPE[name]

    layout = Layout(name=name, rect=rect, defaultContent=content, type=type)

    logger.info(f'layout={layout}')

    return layout


def name_rect_to_layouts(name_rect: dict) -> List[Layout]:
    layouts = [name_rect_to_layout(k, v) for k, v in name_rect.items()]
    # logger.debug(f'r={r}')
    return layouts


def get_sample_ticket_layouts() -> List[Layout]:
    layouts = [
        Layout(name="top", rect=Rect(size=Size(width=1920, height=191), point=Point(x=0, y=0)),
               defaultContent=Content(title="ticket_top", file=TicketURLEnum.URL_TICKET_TOP.value,
                                      type=ContentTypeEnum.APP_TICKET), type=RegionTypeEnum.TOP),

        Layout(name="side", rect=Rect(size=Size(width=556, height=889), point=Point(x=1364, y=191)),
               defaultContent=Content(title="ticket_right", file=TicketURLEnum.URL_TICKET_RIGHT.value,
                                      type=ContentTypeEnum.APP_TICKET), type=RegionTypeEnum.SIDE),
        Layout(name="main", rect=Rect(size=Size(width=1364, height=833), point=Point(x=0, y=191)),
               defaultContent=Content(file="60.mp4", type=ContentTypeEnum.VIDEO), type=RegionTypeEnum.MAIN
               ),

    ]
    return layouts


'''
2020-04-10 08:35:36.308 | INFO     | __main__:__init__:114 - QTicketTop:qrect=PyQt5.QtCore.QRect(0, 0, 1920, 219):rect=size=Size(width=1920, height=219) point=Point(x=0, y=0)
2020-04-10 08:35:36.319 | INFO     | __main__:__init__:143 - QTicketRight:qrect=PyQt5.QtCore.QRect(1419, 219, 501, 861):rect=size=Size(width=501, height=861) point=Point(x=1419, y=219)
2020-04-10 08:35:36.320 | DEBUG    | __main__:__init__:221 - rectAll=PyQt5.QtCore.QRect(0, 219, 1419, 861)
2020-04-10 08:35:36.320 | DEBUG    | __main__:__init__:222 - mayBeVideoRect=PyQt5.QtCore.QRect(0, 219, 1419, 861)
'''


def get_sample_layouts() -> List[Layout]:
    if is_platform_mac():
        target_w = 640
        target_h = 480
    else:
        target_w = 1920
        target_h = 1080

    name_rect = result_to_name_rect(target_w, target_h, result)

    sample_layouts = name_rect_to_layouts(name_rect)
    logger.info(f'sample_layouts={sample_layouts}')
    return sample_layouts


if __name__ == '__main__':
    target_w = 1920
    target_h = 1080
    name_rect = result_to_name_rect(target_w, target_h, result)
    name_rect_to_layouts(name_rect)

    get_sample_layouts()

    get_sample_ticket_layouts()
