import json
from pathlib import Path
from struct import unpack
from typing import List

from loguru import logger
from pydantic import BaseModel

'''

전체 패킷 
=> 필터 : ip.addr == 10.100.42.254 and udp
=> 메뉴 : export packet dissections => as json =>  selected packets only => filename : 222.json

분석결과 : 똑같은 스트링 : 494e42530100cd000000cc0000000001000000000000

494e42530100cd000000cc0000000001000000000000 [a1:2] 00000000000000 [a1:2] 00010104000 [b:1] 0000000b0104000000000002010400 [c:8] 03010400 [d:8] 04010400 [e:2] 00000006010400 [g:6] 0007010400 [h:6] 00080104000000000009010400 [i:12] [j:8] ???

494e42530100cd000000cc0000000001000000000000 00 00000000000000 00 00010104000 0 0000000b0104000000000002010400 12345678 03010400 12345678

494e42530100cd000000cc00000000010000000000000000000000000000000001010400000000000b0104000000000002010400 12345678 03010400 12345678

c = x point
d = y point
j = length
j+length = message


'''


#


def debug_msg(param):
    # logger.debug(param)
    pass


class MSG(BaseModel):
    x: int
    y: int
    msg: str


class Gate(BaseModel):
    name: str = ''  # 창구번호
    call: str = ''  # 호출번호


class TicketData(BaseModel):
    store: str = '대기'
    wait: str = '0'  # 대기수
    gates: List[Gate] = [Gate(), Gate(), Gate(), Gate(), ]
    msg: str = ''  # 출력 메시지 (xxx번 고객님 yyy번 창구로 오십시오)

    def update(self):
        logger.info(f'UPDATED!!! => TicketData=>{self.json()}')

    def add(self, msg: MSG):

        if msg.x == 1095 and msg.y == 165:  # 지점명
            self.store = msg.msg

        elif msg.x == 1150 and msg.y == 237:  # 대기고객
            self.wait = msg.msg

        elif msg.x == 990 and msg.y == 380:  # 1번째 창구번호
            self.gates[0].name = msg.msg
        elif msg.x == 1155 and msg.y == 380:  # 1번째 호출번호
            self.gates[0].call = msg.msg

        elif msg.x == 990 and msg.y == 462:  # 2번째 창구번호
            self.gates[1].name = msg.msg
        elif msg.x == 1155 and msg.y == 462:  # 2번째 호출번호
            self.gates[1].call = msg.msg

        elif msg.x == 990 and msg.y == 544:  # 3번째 창구번호
            self.gates[2].name = msg.msg
        elif msg.x == 1155 and msg.y == 544:  # 3번째 호출번호
            self.gates[2].call = msg.msg

        elif msg.x == 990 and msg.y == 626:  # 4번째 창구번호
            self.gates[3].name = msg.msg
        elif msg.x == 1155 and msg.y == 626:  # 4번째 호출번호
            self.gates[3].call = msg.msg

        elif msg.x == 460 and msg.y == 60:  # 출력 메시지 (xxx번 고객님 yyy번 창구로 오십시오)
            self.msg = msg.msg
        else:
            logger.error(f'unknown position : msg={msg}')

        # wait_ms = 500
        #
        # pydash.debounce(self.update, wait=wait_ms)





def get_sample_ticket_data() -> TicketData:
    store: str = '성서공단 기업금융사랑 지점'
    wait: str = 'W1234'  # 대기수
    gates: List[Gate] = [
        Gate(name="G001", call="C001"),
        Gate(name="G002", call="C002"),
        Gate(name="G003", call="C003"),
        Gate(name="G004", call="C004"),
    ]
    msg: str = '1234번 고객님 5678번 창구로 오십시오'

    return TicketData(store=store, wait=wait, gates=gates, msg=msg)


def parse_msg(h: str) -> MSG:
    x = unpack('h', bytes.fromhex(h[104:104 + 4]))[0]
    debug_msg(f'x={x}')
    y = unpack('h', bytes.fromhex(h[120:120 + 4]))[0]
    debug_msg(f'y={y}')

    hex_length = h[212:212 + 4]

    debug_msg(f'hex_length={hex_length}')

    hex_length = unpack('h', bytes.fromhex(hex_length))[0] * 2
    debug_msg(f'hex_length={hex_length}')

    hex_msg = h[216:216 + hex_length]

    msg = bytes.fromhex(hex_msg).decode('euckr')

    debug_msg(f'msg={msg}')

    # logger.info(f'x={x}|y={y}|msg={msg}')

    return MSG(x=x, y=y, msg=msg)


def test_packet_dump_json():
    p = Path('~/Downloads/222.json').expanduser()

    l = json.loads(p.read_text())

    p_out = Path('~/Downloads/out.txt').expanduser()

    ss = []

    for j in l:
        i = j['_source']['layers']['data']['data.data']
        i: str = i.replace(':', '')
        b = bytes.fromhex(i)
        h = b.hex()
        debug_msg(f'len(b)={len(b)}|h={h}\nb={b}')
        parse_msg(h)

    p_out.write_text('\n\n'.join(ss))


if __name__ == '__main__':
    pass
