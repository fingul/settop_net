import datetime
import json
from enum import Enum
from typing import Dict, Set, Any, Union
from typing import List

from loguru import logger
from pydantic import BaseModel
from pytz import timezone

from models_file import PlaySet, TransferSet, TVStatus, Size, TVControlMethodEnum, NoFile

SAMPLE_GROUP = "a"
SAMPLE_GROUPS = ["a", "b"]
SAMPLE_FTP = "ftp://a.com/sub_path"
SAMPLE_FTPS = [SAMPLE_FTP, "ftp://b.com/sub_path", ]
SAMPLE_STB_NAME = "SAMPLE_STB_NAME"
SAMPLE_SCHEDULE_URL = 'http://127.0.0.1:8000/schedule/a'
SAMPLE_TRANSFER_URL1 = 'http://127.0.0.1:8000/transfer/a'
SAMPLE_TRANSFER_URL2 = 'http://127.0.0.1:8000/transfer/b'
SAMPLE_VERSION_URL = 'http://127.0.0.1:8000/update/version.json'
SAMPLE_CONFIG_URL = 'http://127.0.0.1:8000/config'


class PerGroup(BaseModel):
    ids: Set[str] = None
    default_schedule: dict = None


class GroupConfig(BaseModel):
    group_dict: Dict[str, PerGroup] = None


class CmdEnum(str, Enum):
    stb_start = 'stb_start'

    stb_add_config = 'stb_add_config'
    stb_add_transfer = 'stb_add_transfer'

    stb_stop = 'stb_stop'
    stb_off = 'stb_off'
    stb_reboot = 'stb_reboot'
    stb_auto_on_off = 'stb_auto_on_off'

    stb_set_volume = 'stb_set_volume'
    stb_caption = 'stb_caption'

    tv_on = 'tv_on'
    tv_off = 'tv_off'
    stb_play_stat = 'stb_play_stat'

    stb_screen_image = 'stb_screen_image'
    stb_log = 'stb_log'
    stb_update_sw = 'stb_update_sw'
    stb_live_on = 'stb_live_on'
    stb_live_off = 'stb_live_off'
    stb_add_schedule = 'stb_add_schedule'
    keep_alive = 'keep_alive'

    stb_transfer_list = 'stb_transfer_list'


class Command(BaseModel):
    cmd: CmdEnum = None
    date: datetime.datetime = None

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)
        if not self.date:
            self.date = timezone("Asia/Seoul").localize(datetime.datetime.utcnow())

    # def __init__(self):
    #     super().__init__()
    #     self.date = timezone("Asia/Seoul").localize(datetime.datetime.utcnow())


class DataResult(BaseModel):
    result: bool = True
    reason: str = ''


class Response(Command, DataResult):
    id: str = ''


class RequestStbStart(Command):
    cmd = CmdEnum.stb_start
    id: str = "a"
    version: str = "20200305_144400"  # @TODO : 프로그램 목록의 가장 최신 배포 버전을 보내준다.


class ServerResponseStbStart(Command):
    cmd = CmdEnum.stb_start
    stb_name: str = 'stb_name'
    branch_name: str = 'branch_name'
    weather_params: str = '?nm=seoul'
    version_url: str = SAMPLE_VERSION_URL


class RequestStbAddConfig(Command):
    cmd = CmdEnum.stb_add_config
    url: str = SAMPLE_CONFIG_URL


class ResponseStbAddConfig(Response):
    cmd = CmdEnum.stb_add_config


class RequestStbOff(Command):
    cmd = CmdEnum.stb_off


class ResponseStbOff(RequestStbOff, Response):
    pass


class RequestStbReboot(Command):
    cmd = CmdEnum.stb_reboot


class ResponseStbReboot(RequestStbReboot, Response):
    pass


class AutoOnOff(BaseModel):
    start_time: datetime.time = datetime.time(hour=9)
    end_time: datetime.time = datetime.time(hour=18)


class RequestStbAutoOnOff(Command, AutoOnOff):
    cmd = CmdEnum.stb_auto_on_off


class ResponseStbAutoOnOff(Response):
    cmd = CmdEnum.stb_auto_on_off


class DataSetVolume(BaseModel):
    screen_no: int = 0  # 화면 번호
    volume: int = 100  # 0~100


class RequestStbSetVolume(Command, DataSetVolume):
    cmd = CmdEnum.stb_set_volume


class ResponseStbSetVolume(Response):
    cmd = CmdEnum.stb_set_volume


class RequestTVOn(Command):
    cmd = CmdEnum.tv_on


class ResponseTVOn(RequestTVOn, Response):
    pass


class RequestTVOff(Command):
    cmd = CmdEnum.tv_off


class ResponseTVOff(RequestTVOff, Response):
    pass


class RequestPlayStat(Command):
    cmd = CmdEnum.stb_play_stat
    dirSizes: List[str] = []


class DataUsedTotal(BaseModel):
    used: int
    total: int


class DataMemory(BaseModel):
    ram: DataUsedTotal
    swap: DataUsedTotal


class DataTVStat(BaseModel):
    status: TVStatus = TVStatus.NONE
    title: str = ''  # sch 일 경우 스케쥴의 제목이 있으면 넣는다.
    screen_no: int = 0  # 화면 번호
    tv_on: bool = True
    resolution: Size = Size(width=1920, height=1080)
    volume: int = 100  # 마지막으로 설정한 볼륨
    controlMethod: TVControlMethodEnum = TVControlMethodEnum.SERIAL


class DataResourceUsage(BaseModel):
    cpu: float = 23.3
    memory: DataUsedTotal = DataUsedTotal(used=1024, total=4096)
    disk: DataUsedTotal = DataUsedTotal(used=1024 * 1024 * 1024 * 1, total=1024 * 1024 * 1024 * 10)
    dirSizes: Dict[str, int] = {}


class ResponsePlayStat(Response, DataResourceUsage):
    cmd = CmdEnum.stb_play_stat
    tv: List[DataTVStat] = [DataTVStat()]


class DownloadStatusEnum(Enum):
    DOWNLOADING = 'downloading'
    COMPLETE = 'complete'


class TransferStatusItem(BaseModel):
    fileName: str
    fileSize: int = 0
    status: DownloadStatusEnum


class RequestStbScreenImage(Command):
    cmd = CmdEnum.stb_screen_image
    ratio: float = 50.0  # 백분율(float) %


class DataScreenShotResult(BaseModel):
    path: str = ''
    size: int = 0


class ResponseStbScreenImage(Response, DataScreenShotResult):
    cmd = CmdEnum.stb_screen_image


class RequestStbLog(Command):
    cmd = CmdEnum.stb_log
    url: str = SAMPLE_FTP


class ResponseStbLog(Response):
    cmd = CmdEnum.stb_log


class RequestStbUpdateSW(Command):
    cmd = CmdEnum.stb_update_sw
    url: str = SAMPLE_FTP


class ResponseStbUpdateSW(Response):
    cmd = CmdEnum.stb_update_sw


class DataLiveOn(BaseModel):
    screen_no: int = 0
    url: str = 'udp://@1.2.3.4:8089'


class RequestStbLiveOn(Command, DataLiveOn):
    cmd = CmdEnum.stb_live_on
    screen_no: int = 0


class ResponseStbLiveOn(Response):
    cmd = CmdEnum.stb_live_on


class RequestStbLiveOff(Command):
    cmd = CmdEnum.stb_live_off


class DataStbLiveOff(BaseModel):
    screen_no: int = 0
    beforeEndState: TVStatus = TVStatus.NONE


class ResponseStbLiveOff(Response, DataStbLiveOff):
    cmd = CmdEnum.stb_live_off


class Schedule(BaseModel):
    date: datetime.date = None
    screen_no: int = 0
    url = SAMPLE_SCHEDULE_URL
    playset: PlaySet = None

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)
        if self.date is None:
            self.date = datetime.date.today()


class Transfer(BaseModel):
    name: str = 'transfer_name'
    url: str
    transfer_set: TransferSet = None


class RequestStbAddSchedule(Command):
    cmd = CmdEnum.stb_add_schedule
    schedules: List[Schedule] = []

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)
        if not self.schedules:
            self.schedules = list([Schedule(screen_no=i) for i in range(2)])


class ResponseStbAddSchedule(Response):
    cmd = CmdEnum.stb_add_schedule


class RequestStbAddTransfer(Command):
    cmd = CmdEnum.stb_add_transfer
    transfers: List[Transfer] = []

    def __init__(self, **data: Any) -> None:
        super().__init__(**data)
        if not self.transfers:
            self.transfers = [Transfer(name="t2", url=SAMPLE_TRANSFER_URL1, ),
                              Transfer(name="t2", url=SAMPLE_TRANSFER_URL2, )]


class ResponseStbAddTransfer(Response):
    cmd = CmdEnum.stb_add_transfer


class RequestKeepAlive(Command):
    cmd = CmdEnum.keep_alive


class ResponseKeepAlive(Response):
    cmd = CmdEnum.keep_alive


class RequestStbTransferList(Command):
    cmd = CmdEnum.stb_transfer_list
    id: str = "a"
    fileVer: str = '000000'
    files: List[NoFile] = []


class ServerResponseStbTransferList(Response):
    cmd = CmdEnum.stb_transfer_list


####################################################################################################


class GroupSendCommand(BaseModel):
    group_name: str = None
    command: Command = None


# class ClientContext:
#     def __init__(self):
#         self.id: str = None
#         self.ws: WebSocket = None
#         self.uuid: UUID = uuid.uuid4()
#
#
# class GroupStatus(BaseModel):
#     id_online: List[str] = None
#     id_offline: List[str] = None


# class Status(BaseModel):
#     groups: Dict[str, GroupStatus] = None


class TDate(BaseModel):
    date: datetime.datetime = None


_a = [RequestStbOff, RequestStbReboot, RequestStbAutoOnOff, RequestTVOn, RequestTVOff, RequestPlayStat,
      RequestStbScreenImage,
      RequestStbLog, RequestStbUpdateSW, RequestStbLiveOn, RequestStbLiveOff, RequestStbAddSchedule,
      RequestKeepAlive]


def _make_sample_json(cls):
    i: BaseModel = cls(**{})
    j = dict(command=json.loads(i.json()))

    # logger.debug(
    #     curlify.to_curl(
    #         Request(method='post', url=f'http://127.0.0.1:8000/send_cmd_all', data=json.dumps(j)).prepare()))

    return json.dumps(j)


samples = [_make_sample_json(a) for a in _a]

ALL_REQUEST_SAMPLE = '\n\n'.join([_make_sample_json(a) for a in _a])


def show_all_samples():
    print('-' * 80)
    print('show_all_samples')
    print('-' * 80)

    for sample in samples:
        print(f'{sample}')

    print('-' * 80)

    print('== FOR LINUX/MAC ==')

    for sample in samples:
        print(f"echo '{sample}' | http post http://127.0.0.1:8000/send_cmd_all")

    print('-' * 80)

    SHOW_BY_GROUP = False
    SHOW_FOR_WINDOWS = False

    if SHOW_BY_GROUP:
        for sample in samples:
            print(f"echo '{sample}' | http post http://127.0.0.1:8000/send_cmd_by_group/a")

    if SHOW_FOR_WINDOWS:
        print('== FOR WINDOWS ==')

        for sample in samples:
            print(f'echo {sample} | http post http://127.0.0.1:8000/send_cmd_all')

        print('-' * 80)

    print('== BOTH ==')

    print('http http://127.0.0.1:8000/connections')
    print('http http://127.0.0.1:8000/config')
    print('http post http://127.0.0.1:8000/config @sample.json')


class ServerRequest(BaseModel):
    # __doc__ = ALL_REQUEST_SAMPLE

    command: Union[
        RequestStbOff, RequestStbReboot, RequestStbAutoOnOff, RequestTVOn, RequestTVOff, RequestPlayStat, RequestStbScreenImage,
        RequestStbLog, RequestStbUpdateSW, RequestStbLiveOn, RequestStbLiveOff, RequestStbAddSchedule,
        RequestKeepAlive]


# ServerRequestsType = Union[*SERVER_REQUESTS]


def dump_sample(ins):
    logger.info(f'{ins.__class__.__name__} = {ins.json()}')


def dump_protocol():
    # c = Command(cmd=CmdEnum.login, payload={})
    #
    # logger.info(f'date={TDate(date=timezone("Asia/Seoul").localize(datetime.utcnow())).json()}')

    sample_id = "SAMPLE_ID"

    dump_sample(RequestStbStart(id="a"))

    logger.warning(f'stb_start => 스케쥴은 별도 명령(stb_add_schedule) 이용(중복됨). (접속 또는 스케쥴 갱신 시)')

    dump_sample(ServerResponseStbStart(stb_name="stb_name", branch_name='branch_name'))

    dump_sample(RequestStbOff())
    dump_sample(ResponseStbOff(id=sample_id))
    dump_sample(RequestStbReboot())
    dump_sample(ResponseStbReboot(id=sample_id))

    dump_sample(RequestStbAutoOnOff())
    dump_sample(ResponseStbAutoOnOff(id=sample_id))

    logger.warning(f'stb_remote_connect, stb_remote_disconnect <= 뭘 해야하는지 모르겠음. 처리 안함')

    dump_sample(RequestTVOn())
    dump_sample(ResponseTVOn(id=sample_id))

    dump_sample(RequestTVOff())
    dump_sample(ResponseTVOff(id=sample_id))

    dump_sample(RequestPlayStat())
    dump_sample(ResponsePlayStat(id=sample_id))

    logger.warning(f'stb_screen_image <= 협의 필요. 멀티스크린캡쳐 어떻게 할지...')
    dump_sample(RequestStbScreenImage())
    dump_sample(ResponseStbScreenImage(id=sample_id))

    logger.warning(f'stb_log <= 협의 필요. 업로드 URL 처리 어떻게 할지...')
    dump_sample(RequestStbLog())
    dump_sample(ResponseStbLog(id=sample_id))

    dump_sample(RequestStbUpdateSW())
    dump_sample(ResponseStbUpdateSW(id=sample_id))

    logger.warning(f'주의: 재접속 시 라이브온 상태일 경우, 라이브온 메시지 다시 보내기...')
    dump_sample(RequestStbLiveOn())
    dump_sample(ResponseStbLiveOn(id=sample_id))

    dump_sample(RequestStbLiveOff())
    dump_sample(ResponseStbLiveOff(id=sample_id))

    logger.warning(f'이름변경/payload도 조금 변경, stb_add_sch => stb_add_schedule')
    dump_sample(RequestStbAddSchedule())
    dump_sample(ResponseStbAddSchedule(id=sample_id))

    dump_sample(RequestKeepAlive())
    dump_sample(ResponseKeepAlive(id=sample_id))

    dump_sample(RequestPlayStat())
    dump_sample(ResponsePlayStat(id=sample_id))

    logger.warning(f'stb_caption <= 협의 필요.')

    # logger.info(f'ALL_REQUEST_SAMPLE=> {ALL_REQUEST_SAMPLE}')


##################################################

if __name__ == '__main__':
    dump_protocol()
    show_all_samples()
    #
    RequestStbAddSchedule()

    logger.debug(RequestStbLiveOn().json())

    #{"screen_no": 0, "url": "udp://1.2.3.4:8089", "cmd": "stb_live_on", "date": "2020-04-03T21:10:09.026874+09:00"}

    # screen_no=0 url=udp://1.2.3.4:8089 cmd=stb_live_on date=2020-04-03T21:10:09.026874+09:00

