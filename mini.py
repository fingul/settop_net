import asyncio
import sys

import uvicorn
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from fastapi import FastAPI
from loguru import logger
from quamash import QEventLoop
from starlette.middleware.cors import CORSMiddleware

from mod_setup_signal import setup_signal

app = FastAPI()
# app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=ALL_METHODS)
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['*'])


@app.get("/")
async def read_root(q: str = None):
    logger.info(f'q={q}')
    # if q:
    #     # w.setUrl(QUrl(q))
    #     ex.player.open_file(q)
    return {"Hello": "World"}


setup_signal()

qapp = QApplication(sys.argv)
loop = QEventLoop(qapp)
asyncio.set_event_loop(loop)  # NEW must set the event loop

w = QMainWindow()
# w.resize(100, 100)
# w.move(300, 300)
# w.setWindowTitle('Simple')
w.show()
# w.showFullScreen()
uvicorn.run(app, host='0.0.0.0', port=8000)


# with loop:
#     loop
#
#     sys.exit(loop.run_forever())

# sys.exit(qapp.exec_())
