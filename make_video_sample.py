import os
from pathlib import Path

from loguru import logger

p = Path('/Users/m/ad/23.mp4')

filename = p.name

out_dir = Path('~/out').expanduser()
out_dir.mkdir(parents=True, exist_ok=True)

out_path = out_dir.joinpath(filename)

bitrate = '5M'
start_second = 0
duration_second = 3
label_duration = duration_second
cmd_start_duration = f'-ss {start_second} -t {duration_second}'
framerate = '30000/1001'
pix_fmt = 'yuv420p'
gop = 30

cmd_h265_29_970fps_8bit = f"ffmpeg {cmd_start_duration} -y -i  '{p}' -s 1920x1080 -r {framerate} -map_metadata -1 -tune zerolatency -tune fastdecode -preset medium -c:v libx265 -tag:v hvc1 " \
                          f"-x265-params keyint={gop}:min-keyint={gop}:scenecut=0:bframes=2:b-adapt=0:open-gop=0 -b:v {bitrate} -minrate {bitrate} -maxrate {bitrate} -bufsize {bitrate} -pix_fmt {pix_fmt}  -c:a aac -b:a 128k -ac 2  '{out_path}.hevc.29.970fps.8bit.aac.{label_duration}.{pix_fmt}.mp4'"

logger.info(f"cmd = {cmd_h265_29_970fps_8bit}")


os.system(cmd_h265_29_970fps_8bit)
os.system(f'open {out_dir}')
