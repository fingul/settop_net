from PyQt5.QtNetwork import QNetworkInterface, QHostAddress
from loguru import logger


def get_ips():
    """Get all ip addresses from computer
    :rtype: list
    """
    ip_list = []

    for _interface in QNetworkInterface().allInterfaces():

        interface: QNetworkInterface = _interface

        name = interface.name()

        hw = interface.hardwareAddress()
        logger.info(f'name={name}|hw={hw}')

        flags = interface.flags()
        is_loopback = bool(flags & QNetworkInterface.IsLoopBack)
        is_p2p = bool(flags & QNetworkInterface.IsPointToPoint)
        is_running = bool(flags & QNetworkInterface.IsRunning)
        is_up = bool(flags & QNetworkInterface.IsUp)

        if not is_running:
            continue
        if not interface.isValid() or is_loopback or is_p2p:
            continue

        for addr in interface.allAddresses():
            if addr == QHostAddress.LocalHost:
                continue
            if not addr.toIPv4Address():
                continue
            ip = addr.toString()
            # addr.net

            logger.info(f'ip={ip}')
            if ip == '':
                continue

            if ip not in ip_list:
                ip_list.append(ip)

    return ip_list


if __name__ == '__main__':
    logger.info(get_ips())
