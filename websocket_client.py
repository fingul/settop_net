import functools
import sys

from PyQt5 import QtCore, QtWebSockets
from PyQt5.QtCore import QUrl, QCoreApplication, QTimer
from PyQt5.QtNetwork import QAbstractSocket, QNetworkConfigurationManager
from PyQt5.QtWidgets import QApplication
from loguru import logger

from mod_setup_signal import setup_signal


class WebSocketClient(QtCore.QObject):
    def __init__(self, parent):
        super().__init__(parent)

        self.connect_count = 0

        self.client = QtWebSockets.QWebSocket("", QtWebSockets.QWebSocketProtocol.Version13, None)
        self.client.stateChanged.connect(self.onStateChanged)

        self.client.connected.connect(self.onConnected)
        self.client.disconnected.connect(self.onDisconnected)
        self.client.error.connect(self.onError)
        self.client.textMessageReceived.connect(self.onMessage)

        # self.client.open(QUrl("ws://echo.websocket.org"))

        self.connect()
        # self.client.pong.connect(self.onPong)

    def onStateChanged(self, state: QAbstractSocket.SocketState):
        logger.info(f"onStateChanged:state={state}")

    def onConnected(self):
        logger.info(f"onConnected")

        self.send_message("hi!")
        self.send_message("bye")
        # self.client.sendTextMessage("bye")

    def onError(self, error_code):
        errorString = self.client.errorString()
        logger.error(f"onError : error_code={error_code}|errorString={errorString}")

    def onDisconnected(self):
        logger.info(f"onDisconnected")
        QTimer.singleShot(1000, self.connect)

    # @pyqtSlot(object)
    def onMessage(self, msg):
        logger.info(f"onMessage={msg}")

    def checkTimeOut(self, connect_count):
        if connect_count == self.connect_count:
            if self.client.state() != QAbstractSocket.ConnectedState:
                self.client.abort()
                # self.client.close()
                self.connect()

    def connect(self):
        self.connect_count += 1

        timerCallback = functools.partial(self.checkTimeOut, connect_count=self.connect_count)

        QTimer.singleShot(1000, timerCallback)
        url: str = "ws://0.0.0.0:8765"
        logger.info(f"try connecting : url={url}")
        self.client.open(QUrl(url))

    # def do_ping(self):
    #     logger.info("client: do_ping")
    #     self.client.ping(b"foo")

    def send_message(self, msg):
        logger.info(f"client: send_message={msg}")
        self.client.sendTextMessage(msg)

    # def onPong(self, elapsedTime, payload):
    #     logger.info("onPong - time: {} ; payload: {}".format(elapsedTime, payload))

    def close(self):
        self.client.close()


def quit_app():
    logger.info("timer timeout - exiting")
    QCoreApplication.quit()


def ping():
    client.do_ping()


def send_message():
    client.send_message()


if __name__ == '__main__':
    setup_signal()
    global client

    app = QApplication(sys.argv)

    # # manager.
    # # manager.conf
    #
    # QNetworkConfiguration
    #
    # QNetworkConfiguration.setConnectTimeout(5000)
    #
    # # QTimer.singleShot(2000, ping)
    # # QTimer.singleShot(3000, send_message)
    # QTimer.singleShot(5000, quit_app)

    client = WebSocketClient(app)

    app.exec_()
