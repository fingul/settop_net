# ##############################
# # 스피커 소리
# ##############################
# sudo modprobe pcspkr
# sudo apt-get install -y beep
# #https://github.com/ShaneMcC/beeps
#
# # https://www.cyberciti.biz/faq/how-to-use-sed-to-find-and-replace-text-in-files-in-linux-unix-shell/
# sudo sed -i 's/blacklist pcspkr/#blacklist pcspkr/g' /etc/modprobe.d/blacklist.conf
#
# beep

# beep -f 1000 -l 100 -r 3
# https://wiki.archlinux.org/index.php/PC_speaker#Beep

import os

from loguru import logger

'''

- 얇은 소리
beep -f 5000 -l 50 -r 2
beep -f 5000 -l 50 -r 1

- 큰소리
beep -f 1000 -l 50 -r 2

'''


def beep(frequncy: int = 5000, length_ms: int = 50, repeat: int = 1):
    command = f'beep -f {frequncy} -l {length_ms} -r {repeat}'
    logger.debug(f'command={command}')

    os.system(command)


def beep_fail():
    beep(frequncy=5000, repeat=2)
    pass


def beep_ok():
    beep(frequncy=5000)
    pass


if __name__ == '__main__':
    beep()
    beep_fail()
    beep_ok()
    pass
