import asyncio
import pathlib
import time
from typing import Optional, Callable
from urllib.parse import urljoin

import aioftp
from aioftp import DEFAULT_USER, DEFAULT_PASSWORD
from etaprogress.progress import ProgressBarYum
from fastapi import FastAPI
from loguru import logger

from ftp_parse_url import FtpPath
from models_file import File, ContentTypeEnum, DownloadModeEnum

app = FastAPI()


def progress(nprogress: int, str_rate: str, str_eta: str):
    logger.debug(f'nprogress={nprogress}|str_rate={str_rate}|str_eta={str_eta}')


def is_abort():
    logger.debug(f'is_abort')
    return False


ABORT_CHECK_INTERVAL_SECOND = 3


async def download(self: aioftp.Client, source, destination="", *,
                   block_size=1024 * 512, f_progress: Optional[Callable[[int, str], None]],
                   f_is_abort: Optional[Callable[[], bool]]):
    source = pathlib.PurePosixPath(source)
    destination = pathlib.Path(destination)

    info = await self.stat(source)
    logger.debug(f'info={info}')

    ntotal: int = int(info['size'])
    percent_last: int = 0
    abort_check_last = time.time()

    bar = ProgressBarYum(ntotal, filename='')

    if await self.is_file(source):
        await self.path_io.mkdir(destination.parent,
                                 parents=True, exist_ok=True)

        async with self.path_io.open(destination, mode="wb") as file_out, \
                self.download_stream(source) as stream:
            async for block in stream.iter_by_block(block_size):

                abort_check = time.time()
                if abort_check - abort_check_last > ABORT_CHECK_INTERVAL_SECOND:
                    if f_is_abort and f_is_abort():
                        raise Exception("aborted")
                    abort_check_last = abort_check

                await file_out.write(block)

                # update progress
                nsize = len(block)
                bar.numerator += nsize

                percent = int(bar.percent)

                if percent != percent_last:
                    if f_progress:
                        str_eta = bar.str_eta.replace('ETA', '').replace(' ', '')

                        f_progress(percent, bar.str_rate, str_eta)
                    percent_last = percent

    else:
        raise Exception("source is not file : source={source}")


async def main():
    # f = FtpPath.from_url(url='ftp://0.0.0.0:21000', user='0', password='0')
    f = FtpPath.from_url(url='ftp://cast@castware.cf:30005/2020/01/02/1199040.MP4', user='cast',
                         password='cast123!@#')

    f = FtpPath.from_url(url='ftp://10.10.10.103/2020/02/18/10232.MP4', user='ibksch',
                         password='test')

    logger.info(f'url={f.to_url()}')



    # files = [File(type=ContentTypeEnum.VIDEO, subPath='/', fileName='1b.txt', cid='0', mode=DownloadModeEnum.EPG)]
    # files = [File(type=ContentTypeEnum.VIDEO, subPath='/', fileName='1000mb.txt', cid='0', mode=DownloadModeEnum.EPG)]
    files = [File(type=ContentTypeEnum.VIDEO, subPath='/2020/02/18', fileName='10232.MP4', cid='0',
                  mode=DownloadModeEnum.EPG)]

    file = files[0]

    destination = f'/tmp/{file.type}/{file.fileName}'

    logger.info(f'destination={destination}')

    read_speed_limit = 1024 * 1024 * 6 / 8

    async with aioftp.ClientSession(host=f.host, port=f.port, user=(f.user or DEFAULT_USER),
                                    password=(f.password or DEFAULT_PASSWORD),
                                    read_speed_limit=read_speed_limit) as client:
        p = urljoin(file.subPath, file.fileName)
        logger.debug(f'p={p}')
        await download(client, p, destination=destination, f_progress=progress, f_is_abort=is_abort)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())

    # # client.download()
    # s = await client.download_stream(file.fileName)

#
# @app.websocket("/")
# async def websocket_endpoint(
#         websocket: WebSocket,
#         # item_id: int,
#         # q: str = None,
#         # cookie_or_client: str = Depends(get_cookie_or_client),
# ):
#     await websocket.accept()
#
#     scheduler = AsyncIOScheduler(asyncio.get_running_loop())
#
#     # scheduler.schedule_periodic(1,lambda i:logger.info('xxx'))
#
#     def scanner(state: Set, i) -> Set:
#         n = state.copy()
#         n.add(i)
#
#         logger.info(f'reducer:n={n}')
#
#         return n
#
#     subject = Subject()
#
#     composed = subject.pipe(
#         # ops.map(lambda x: x["term"]),
#         # ops.filter(lambda text: len(text) > 2),  # Only if the text is longer than 2 characters
#         # ops.buffer_with_time(datetime.timedelta(seconds=1)),  # Pause for 750ms
#         ops.scan(scanner, set()),
#         # ops.distinct_until_changed(),            # Only if the value has changed
#         # ops.flat_map_latest(search_wikipedia)
#         ops.debounce(0.750),  # Pause for 750ms
#         # ops.distinct_until_changed(),  # Only if the value has changed
#         # ops.flat_map_latest(search_wikipedia)
#
#     )
#
#     def on_error(ex):
#         logger.info(f'on_error={ex}')
#
#     def send_response(x):
#         logger.info(f'send_response={x}')
#
#     composed.subscribe(on_next=send_response, on_error=on_error, scheduler=scheduler)
#
#     while True:
#         data = await websocket.receive_text()
#         logger.info(data)
#         subject.on_next(data)
#         # await websocket.send_text(
#         #     f"Session Cookie or X-Client Header value is: {cookie_or_client}"
#         # )
#         # if q is not None:
#         #     await websocket.send_text(f"Query parameter q is: {q}")
#         # await websocket.send_text(f"Message text was: {data}, for item ID: {item_id}")
#
#
# def entry():
#     uvicorn.run('download_list:app', host="0.0.0.0", port=8000, log_level="info", reload=True)
#
#
# if __name__ == "__main__":
#     entry()
