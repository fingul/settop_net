import datetime
import sys
from enum import Enum
from typing import List, Union, Optional

import arrow
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QApplication
from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.jobstores.memory import MemoryJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from loguru import logger
from pydantic import BaseModel
from tzlocal import get_localzone

from mod_setup_signal import setup_signal
from models import Schedule
from models_file import PlaySet, Size, PlayItem, RegionTypeEnum, ContentTypeEnum, TVOnOffTime, Layout, TVOnEnum
from tool_make_rect import get_sample_layouts, get_sample_ticket_layouts


def get_sample_ticket_playset() -> PlaySet:
    layouts = get_sample_ticket_layouts()

    playItemsByLayout = {}

    now = arrow.now()

    logger.info(f'now={now}')

    tvOnOffTimes = [
        TVOnOffTime(type=TVOnEnum.ON, time=datetime.time(hour=8)),
        TVOnOffTime(type=TVOnEnum.OFF, time=datetime.time(hour=12 + 6)),
    ]

    playSet = PlaySet(updateTime=datetime.datetime.now(), screenSize=Size(width=1920, height=1080), layouts=layouts,
                      playItemsByLayout=playItemsByLayout, tvOnOffTime=tvOnOffTimes)

    logger.info(f'playSet={playSet.json(indent=2, ensure_ascii=False)}')

    return playSet


def get_sample_playset() -> PlaySet:
    layouts = get_sample_layouts()

    playItemsByLayout = {}

    now = arrow.now() - datetime.timedelta(seconds=5)

    logger.info(f'now={now}')

    for i in range(0, 2):
        next_run_time = now + datetime.timedelta(seconds=10 * (i + 1))
        # next_run_time = now + datetime.timedelta(seconds=0.1 * i)
        playItem = PlayItem(type=ContentTypeEnum.VIDEO, fileName=f'{i}.mp4', start=now.time(), end=next_run_time.time())
        logger.info(f'playItem={playItem.json(indent=2, ensure_ascii=False)}')
        playItemsByLayout.setdefault(RegionTypeEnum.MAIN.value, []).append(playItem)
        now = next_run_time

    # playItemsByLayout[RegionTypeEnum.MAIN.name] = []

    tvOnOffTimes = [
        TVOnOffTime(type=TVOnEnum.ON, time=datetime.time(hour=8)),
        TVOnOffTime(type=TVOnEnum.OFF, time=datetime.time(hour=12 + 6)),
    ]

    playSet = PlaySet(updateTime=datetime.datetime.now(), screenSize=Size(width=1920, height=1080), layouts=layouts,
                      playItemsByLayout=playItemsByLayout, tvOnOffTime=tvOnOffTimes)

    logger.info(f'playSet={playSet.json(indent=2, ensure_ascii=False)}')

    return playSet


def get_sample_schedules() -> List[Schedule]:
    day_0 = arrow.now().date()
    day_1 = day_0 + datetime.timedelta(days=1)
    day_minus_1 = day_0 + datetime.timedelta(days=-1)
    playset = get_sample_playset()

    schedules = [Schedule(date=day_minus_1, playset=playset), Schedule(date=day_0, playset=playset),
                 Schedule(date=day_1, playset=playset)]

    return schedules


class FullPlayItem(BaseModel):
    layoutName: str
    start_second: float = 0
    playItem: Optional[PlayItem] = None


class FireItemEnum(str, Enum):
    LAYOUTS = 'LAYOUTS'
    TVONOFF = 'TVONOFF'
    FULLPLAYITEM = 'FULLPLAYITEM'


class FireItem(BaseModel):
    start: datetime.datetime
    endTemp: Optional[datetime.datetime] = None
    item: Union[TVOnOffTime, List[Layout], FullPlayItem]

    @property
    def type(self) -> FireItemEnum:
        if isinstance(self.item, TVOnOffTime):
            return FireItemEnum.TVONOFF
        elif isinstance(self.item, FullPlayItem):
            return FireItemEnum.FULLPLAYITEM
        else:
            return FireItemEnum.LAYOUTS

    def order(self) -> int:
        if self.type == FireItemEnum.TVONOFF:
            return 2

        if self.type == FireItemEnum.FULLPLAYITEM:
            return 1

        if self.type == FireItemEnum.LAYOUTS:
            return 3

        logger.error(f'order:UNKNOWN TYPE:{self}')

    def get_miss_fire_grace_time(self) -> int:

        if self.type == FireItemEnum.TVONOFF:
            return 3600 * 24

        if self.type == FireItemEnum.FULLPLAYITEM:
            return 10

        if self.type == FireItemEnum.LAYOUTS:
            return 3600 * 24

        logger.error(f'get_miss_fire_grace_time:UNKNOWN TYPE:{self}')

    def __str__(self):
        return f'[{self.type}]{arrow.get(self.start)}|item={self.item}'


def sort_fire_items(fireItems: List[FireItem]) -> List[FireItem]:
    def compare(a: FireItem):

        if isinstance(a.item, FullPlayItem):
            order = 1
        elif isinstance(a.item, TVOnOffTime):
            order = 2
        else:
            order = 2
        return (a.start, order)

    return sorted(fireItems, key=compare)


def combine_date_time(date, time):
    local_zone = get_localzone()
    return local_zone.localize(datetime.datetime.combine(date, time))


def serializeFullPlayItems(preFireItems: List[FireItem]) -> List[FireItem]:
    # [{[]: preFireItem} for preFireItem in preFireItems]
    # logger.debug(f'preFireItems={preFireItems}')

    dictLayoutNameStartTimeToPreFireItem = {(preFireItem.item.layoutName, preFireItem.start): preFireItem for
                                            preFireItem in preFireItems}

    fireItems = []
    for preFireItem in preFireItems:
        # preFireItem.start
        fireItems.append(preFireItem)

        endItem = dictLayoutNameStartTimeToPreFireItem.get((preFireItem.item.layoutName, preFireItem.endTemp))
        if not endItem:
            newEndItem = FireItem(start=preFireItem.endTemp,
                                  item=FullPlayItem(layoutName=preFireItem.item.layoutName, playItem=None))
            fireItems.append(newEndItem)
    return fireItems


def layoutsToFireItem(date: datetime.date, layouts: List[Layout]):
    start = combine_date_time(date, datetime.time(0, 0, 0))
    return FireItem(start=start, item=layouts)


def tvOnOffTimeToFireItem(date: datetime.date, tVOnOffTime: TVOnOffTime) -> FireItem:
    start = combine_date_time(date, tVOnOffTime.time)
    return FireItem(start=start, item=tVOnOffTime)


# import setup_log


class Plan(QObject):
    on_fire = pyqtSignal(object)

    def __init__(self, schedules: List[Schedule]) -> None:
        super().__init__()
        self.schedules = schedules
        self.fireitems: List[FireItem] = []
        self.scheduler = None

    def build(self):
        current_datetime = arrow.now()
        current_date = current_datetime.date()

        schedules = [schedule for schedule in self.schedules if schedule.date >= current_date]

        preFireItems: List[FireItem] = []

        allFireItems: List[FireItem] = []

        for schedule in schedules:

            if schedule.screen_no != 0:
                logger.error("schedule.screen_no!=0 NOT SUPPORTED YET!!!!!")
                continue

            scheduleDate = schedule.date

            # 빌드 레이아웃
            layouts = schedule.playset.layouts

            allFireItems.append(layoutsToFireItem(scheduleDate, layouts))

            tvOnOffTimes = schedule.playset.tvOnOffTime
            for tvOnOffTime in tvOnOffTimes:
                fireItem = tvOnOffTimeToFireItem(scheduleDate, tvOnOffTime)
                allFireItems.append(fireItem)

            for layoutName, playItems in schedule.playset.playItemsByLayout.items():

                for playItem in playItems:
                    start = combine_date_time(scheduleDate, playItem.start)
                    end = combine_date_time(scheduleDate, playItem.end)

                    if end < current_datetime:
                        continue

                    if start <= current_datetime <= end:
                        start_second = (current_datetime - start).total_seconds()
                    else:
                        start_second = 0.0

                    preFullPlayItem = FullPlayItem(layoutName=layoutName, start_second=start_second, playItem=playItem)

                    # preFullPlayItems.append(preFullPlayItem)

                    fireItem = FireItem(start=start, endTemp=end, item=preFullPlayItem)
                    preFireItems.append(fireItem)
                    # logger.info(f'fullPlayItem={fullPlayItem}')

        fireItems = serializeFullPlayItems(preFireItems)

        # for fireItem in fireItems:
        #     logger.info(f'fireItem={fireItem}')

        # logger.info('-' * 80)
        finalFireItems = list(sort_fire_items(fireItems + allFireItems))

        # logger.warning('-' * 80)
        # for fireItem in finalFireItems:
        #     logger.info(f'fireItem={fireItem}')
        # logger.warning('-' * 80)

        self.fireitems = finalFireItems

    def _fire_item(self, fireitem):

        # logger.info(f'on_fire={fireitem}')

        self.on_fire.emit(fireitem)
        logger.debug("_fire_item|||||||||_fire_item|||||||||_fire_item|||||||||_fire_item|||||||||")

    def play(self) -> int:
        '''

        :return: 스케쥴 카운트
        '''

        logger.warning('-' * 80)

        self.close()

        self.build()

        jobstores = {
            # 'mongo': MongoDBJobStore(),
            # 'default': SQLAlchemyJobStore(url='sqlite:///jobs.sqlite')
            'default': MemoryJobStore()
        }
        executors = {
            'default': ThreadPoolExecutor(5),
            # 'processpool': ProcessPoolExecutor(5)
        }
        job_defaults = {
            'coalesce': False,
            'max_instances': 3
        }

        self.scheduler = BackgroundScheduler(jobstores=jobstores, executors=executors, job_defaults=job_defaults)

        now = arrow.now()

        for fireitem in self.fireitems:

            if fireitem.start - now < datetime.timedelta(hours=1):
                logger.info(f"WILL RUN={fireitem}")

            self.scheduler.add_job(self._fire_item, args=[], kwargs=dict(fireitem=fireitem), trigger='date',
                                   next_run_time=fireitem.start, misfire_grace_time=fireitem.get_miss_fire_grace_time())

        self.scheduler.start()
        # self.scheduler.print_jobs()
        logger.debug("self.scheduler.start()")

        return len(self.fireitems)

    def close(self):
        if self.scheduler:
            logger.debug("begin shutdown scheduler")

            self.scheduler.shutdown(wait=True)
            logger.debug("done shutdown scheduler")

            self.scheduler = None

    def __del__(self):
        self.close()


if __name__ == '__main__':
    setup_signal()

    schedules = get_sample_schedules()
    logger.info(f'schedules={schedules}')


    class Watcher(QObject):
        @pyqtSlot(object)
        def on_fired(self, fireItem: FireItem):
            logger.info(f'fire!!!!!=>{fireItem}')


    watcher = Watcher()


    def a():
        logger.info("a--------------------------------------")
        plan = Plan(schedules)
        plan.on_fire.connect(watcher.on_fired)
        plan.play()
        plan.close()


    def b():
        logger.info("b--------------------------------------")
        plan = Plan(schedules)
        plan.on_fire.connect(watcher.on_fired)
        plan.play()
        plan.close()


    a()
    b()

    app = QApplication(sys.argv)
    # ex = App()
    app.exec()
