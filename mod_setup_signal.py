import os
import signal


def setup_signal():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    os.environ['DISPLAY'] = ':0.0'

    # request hang when get proxy | https://stackoverflow.com/a/53047403
    os.environ['no_proxy'] = '*'

