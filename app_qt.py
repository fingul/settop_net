import asyncio
import os
import platform
import sys
from pathlib import Path

import uvicorn
from PyQt5.QtCore import QPoint, QSize, QRect, QUrl
from PyQt5.QtWidgets import (QWidget, QHBoxLayout, QVBoxLayout, QApplication, QTextEdit)
from loguru import logger
from quamash import QEventLoop
from starlette.middleware.cors import CORSMiddleware

import glob_files
from InfinitePlayer import IPlayer
from patch_uvicorn_event_loop import patch_uvicorn_event_loop
from setup_environment_variable_display import setup_environment_variable_display

# QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
# QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
# QtWebEngine.initialize()
# QWebEngineView.init

patch_uvicorn_event_loop()

if platform.system() == "Darwin":  # for MacOS

    class QWebEngineView(QTextEdit):

        def __init__(self, *__args):
            logger.info(f'using mock video')
            super().__init__(*__args)

        def load(self, url: QUrl):
            self.setText(str(url))

        def setZoomFactor(self, factor: float):
            pass



else:
    from PyQt5.QtWebEngineWidgets import QWebEngineView


