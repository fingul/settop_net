import datetime
import os
import sys
from functools import reduce
from pathlib import Path
from typing import Dict, List

from PyQt5 import QtCore
from PyQt5.QtCore import QObject
from PyQt5.QtCore import QUrl, Qt, QRect
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWidgets import QWidget, QLabel, QStackedWidget
from loguru import logger
from pandas.compat import is_platform_mac

from InfinitePlayer import IPlayer
from mod_setup_signal import setup_signal
from models_file import Layout, RegionTypeEnum, PlayItem, ContentTypeEnum
from tool_make_rect import get_sample_layouts

# result_name_rect={'top': PyQt5.QtCore.QRect(0, 0, 1920, 180), 'main': PyQt5.QtCore.QRect(0, 180, 1440, 720), 'bottom': PyQt5.QtCore.QRect(0, 900, 1440, 180), 'side': PyQt5.QtCore.QRect(1440, 180, 480, 900)}

# from app import App
name_to_color = dict(
    main='grey',
    side='blue',
    bottom='green',
    top='orange',
)


class AnyCon(QStackedWidget):

    def __init__(self, parent: QWidget, layout: Layout) -> None:

        logger.debug(f'anycon:layout={layout}')

        super().__init__(parent)

        rect: QRect = layout.rect.qrect

        self.setGeometry(rect)

        self.is_support_video = layout.type == RegionTypeEnum.MAIN

        defaultContent = layout.defaultContent

        self.defaultPlayItem = PlayItem(no=-1, title='', type=defaultContent.type, fileName=defaultContent.file,
                                        start=datetime.time(0, 0, 0),
                                        end=datetime.time(0, 0, 0))

        ###########################
        # video
        ###########################
        self.player = None
        if self.is_support_video:
            self.player = IPlayer()
            self.video = self.player.videoframe
            self.addWidget(self.video)

        ###########################
        # web
        ###########################
        w = QWebEngineView(parent=self)

        # disable key input
        w.setEnabled(False)
        # w.setAlignment(Qt.AlignCenter)
        w.setGeometry(rect)
        # w.setUrl(QUrl('http://google.com'))
        w.setHtml('hi!')

        self.web = w
        self.addWidget(w)

        ###########################
        # label
        ###########################

        label = f'{layout.name}|type={layout.type}'

        w = QLabel(label, parent=self)
        w.setAlignment(Qt.AlignCenter)
        w.setGeometry(rect)
        # w.setStyleSheet(f"background: green")

        color = name_to_color.get(layout.name, 'red')
        logger.debug(f'color={color}')

        w.setStyleSheet(f"background: {color}")

        self.label = w
        self.addWidget(w)

        # self.setCurrentWidget(self.label)
        self.set_playItem(self.defaultPlayItem)

    def check_path(self, path: Path, size: int = 0):
        if path.exists():
            if size:
                if path.stat().st_size == size:
                    return True
                else:
                    self.set_text(f'다운로드 중:{path}')
            else:
                return True
        else:
            self.set_text(f'다운로드 대기 중:{path}')

    def set_playItem(self, playItem: PlayItem):

        logger.info(f'set_playItem : playItem={playItem}')

        if not playItem:
            playItem = self.defaultPlayItem

        if playItem.type == ContentTypeEnum.VIDEO:
            path = Path('~/settop/media/epg').joinpath(playItem.fileName).expanduser()
            if self.check_path(path, playItem.fileSize):
                self.set_video(path)
            else:
                self.set_text(f'다운로드 중 {playItem}')
        elif playItem.type == ContentTypeEnum.IMAGE:
            path = Path('~/settop/media/epg').joinpath(playItem.fileName).expanduser()
            if self.check_path(path, playItem.fileSize):
                self.set_image(path)
            else:
                self.set_text(f'다운로드 중 {playItem}')
        elif playItem.type == ContentTypeEnum.URL:
            url = playItem.fileName
            self.set_url(url)
        elif playItem.type == ContentTypeEnum.TEXT:
            text = playItem.fileName
            self.set_text(text)
        else:
            self.set_text(f'알수없는형식:{playItem}')

    def video_stop(self):
        if self.player:
            self.player.stop()

    def set_video(self, path: Path):

        if self.is_support_video:
            self.setCurrentWidget(self.video)
            self.video_stop()
            self.player.open_file(str(path))
        else:
            logger.warning("video not supported")

    def set_text(self, text: str):
        self.video_stop()
        self.setCurrentWidget(self.label)
        self.label.setText(text)

    def set_image(self, path: Path):
        self.video_stop()
        # pixmap = QPixmap(str(path))
        # self.label.setScaledContents(False)
        # self.label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)

        self.label.setPixmap(QPixmap.fromImage(QImage(str(path))))
        self.setCurrentWidget(self.label)

    def set_url(self, url: str = 'about:blank'):
        self.video_stop()
        self.setCurrentWidget(self.web)
        self.web.setUrl(QUrl(url))


class LayoutManager(QObject):

    def __init__(self, layouts: List[Layout]):
        super().__init__()
        self.layouts = {layout.name: layout for layout in layouts}
        self.anycons: Dict[str, AnyCon] = {}

    def __str__(self):
        return f'LayoutManager({self.layouts})'

    def init_ui(self, parent: QWidget):
        parent.setGeometry(self.get_boundary_rect())

        layout: Layout
        for layout in self.layouts.values():
            name = layout.name
            self.anycons[name] = AnyCon(parent, layout)

    def set_play_item(self, name, play_item: PlayItem = None):

        anycon: AnyCon = self.anycons.get(name)
        if anycon:
            anycon.set_playItem(play_item)
        else:
            logger.error("fail find anycon : name={name}")

    def get_boundary_rect(self) -> QRect:
        qrects = [layout.rect.qrect for layout in self.layouts.values()]

        boundary_rect = reduce(lambda x, y: x.united(y), qrects)
        logger.info(f'boundary_rect={boundary_rect}')
        return boundary_rect


# logger.info(f'lm={lm}')

class App(QMainWindow):

    def __init__(self):
        super().__init__()

        logger.debug('init app')

        self.title = 'APP_TITLE'
        layouts = get_sample_layouts()

        self.lm = LayoutManager(layouts=layouts)
        self.initUI()

    def keyPressEvent(self, e):
        logger.info(f'keyPressEvent : e={e} | e.key()={e.key()}| e.text()={e.text()}')
        text = e.text()
        if text == '1':
            # self.lm.set_play_item("main", PlayItem(type=ContentTypeEnum.IMAGE, fileName="0.jpg"))

            p = PlayItem(no=-1, title='', type=ContentTypeEnum.IMAGE, fileName="0.jpg",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("top", play_item=p)

            # self.region_manager.set_content('main', ImageContent(path='~/0.png'))
        elif text == '2':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.VIDEO, fileName="10217.mp4",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '3':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.VIDEO, fileName="10216.mp4",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '4':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.VIDEO, fileName="60.mp4",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '5':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.URL, fileName="http://naver.com",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("bottom", play_item=p)
        elif text == '6':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.URL, fileName="https://noonnu.cc/",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '7':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.TEXT, fileName="가나다라마바사",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)

        # elif text == 'q':
        #     restart_app()

    def initUI(self):
        self.lm.init_ui(self)

        if is_platform_mac():
            self.show()
        else:
            self.showFullScreen()

        # QtCore.QTimer.singleShot(0, self.after)

    # def after(self):
    #     logger.info('boom')
    #     # self.showFullScreen()


if __name__ == '__main__':
    os.environ['DISPLAY'] = ':0.0'

    setup_signal()

    # def tick():
    #     print('tick')
    #
    #
    # timer = QTimer()
    # timer.timeout.connect(tick)
    # timer.start(1000)

    app = QApplication(sys.argv)

    # disable show scrollbar
    settings = QWebEngineSettings.globalSettings()
    settings.setAttribute(QWebEngineSettings.ShowScrollBars, False)

    ex = App()
    app.exec()
