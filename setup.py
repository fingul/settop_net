from setuptools import setup

setup(
    name='yourscript',
    version='0.1',
    py_modules=['yourscript'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        stbc=ws_client:main
        stbs=ws_server:entry
        stb_sample=models:show_all_samples
        stb_protocol=models:dump_protocol
    ''',
)
