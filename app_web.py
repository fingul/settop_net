import asyncio
import os
import platform
import sys
from pathlib import Path

import uvicorn
from PyQt5.QtCore import QPoint, QSize, QRect, QUrl
from PyQt5.QtWidgets import (QWidget, QHBoxLayout, QVBoxLayout, QApplication, QTextEdit)
from loguru import logger
from quamash import QEventLoop
from starlette.middleware.cors import CORSMiddleware

import glob_files
from InfinitePlayer import IPlayer
from patch_uvicorn_event_loop import patch_uvicorn_event_loop
from setup_environment_variable_display import setup_environment_variable_display

# QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
# QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)
# QtWebEngine.initialize()
# QWebEngineView.init

patch_uvicorn_event_loop()

if platform.system() == "Darwin":  # for MacOS

    class QWebEngineView(QTextEdit):

        def __init__(self, *__args):
            logger.info(f'using mock video')
            super().__init__(*__args)

        def load(self, url: QUrl):
            self.setText(str(url))

        def setZoomFactor(self, factor: float):
            pass



else:
    from PyQt5.QtWebEngineWidgets import QWebEngineView


from fastapi import FastAPI

app = FastAPI()
# app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=ALL_METHODS)
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['*'])


@app.get("/")
async def read_root(q: str = None):
    logger.info(f'q={q}')
    if q:
        # w.setUrl(QUrl(q))
        ex.player.open_file(q)
    return {"Hello": "World"}


# @app.get("/vol/get")
# async def vol():
#     level, mute = control_volume.get_volume()
#     return dict(level=level)
#
#
# @app.get("/vol/set")
# async def vol(level: int):
#     control_volume.set_volume(level=level)
#
#
# @app.get("/vol/play")
# async def vol_play():
#     control_volume.play_volume()

@app.get("/system")
async def system(cmd: str = None):
    logger.info(f'cmd={cmd}')
    if cmd:
        # w.setUrl(QUrl(q))
        os.system(cmd)
    return {"Hello": "World"}


@app.get("/web/top")
async def web_top(url: str):
    ex.web_top.load(QUrl(url))


@app.get("/web/mid")
async def web_top(url: str):
    ex.web_mid.load(QUrl(url))


@app.get("/web/down")
async def web_top(url: str):
    ex.web_down.load(QUrl(url))


@app.get("/list_file")
async def play_file(root: str = None):
    files = glob_files.get_file(root)
    return files

    # logger.info(f'q={q}')
    # if q:
    #     path = str(Path(q).expanduser())
    #     ex.player.open_file(path)
    # return {"Hello": "World"}


@app.get("/play/file")
async def play_file(path: str = None, root: str = None):
    if not root:
        logger.info(f'root_path not given, using default DEFAULT_MEDIA_ROOT={glob_files.DEFAULT_MEDIA_ROOT}')
        root = glob_files.DEFAULT_MEDIA_ROOT

    if path:
        full_path = Path(root).joinpath(path).expanduser()
        if not full_path.is_file():
            logger.error(f'full_path={full_path} is not file')
            return
        else:
            logger.info(f'ex.player.open_file={full_path}')
            ex.player.open_file(str(full_path))

    return {"Hello": "World"}


@app.get("/play/url")
async def play_url(q: str = None):
    logger.info(f'q={q}')
    if q:
        # path = str(Path(q).expanduser())
        ex.player.open_file(q)
    return {"Hello": "World"}


@app.get("/play/youtube")
async def play_youtube(q: str = None):
    logger.info(f'q={q}')
    if q:
        # path = str(Path(q).expanduser())
        ex.player.open_youtube(q)
    return {"Hello": "World"}


@app.get("/show/full")
async def fullscreen():
    ex.showFullScreen()
    return {"Hello": "World"}


@app.get("/show/normal")
async def fullscreen():
    ex.showNormal()
    return {"Hello": "World"}


@app.get("/show/toggle")
async def toggle():
    if ex.isFullScreen():
        ex.showNormal()
    else:
        ex.showFullScreen()
    return {"Hello": "World"}


#
#
# @app.get("/exit")
# async def exit():
#     w.close()
#     # if w.isFullScreen():
#     #     w.showNormal()
#     # else:
#     #     w.showFullScreen()
#     return {"Hello": "World"}
#
#
# @app.get("/items/{item_id}")
# def read_item(item_id: int, q: str = None):
#     # progress.setValue(item_id)
#
#     return {"item_id": item_id, "q": q}

setup_environment_variable_display()
try:

    port = int(os.environ.get('PORT', 8000))

    uvicorn.run(app, host='0.0.0.0', port=port, reload=False, reload_dirs=str(Path(__file__).parent.absolute()))
except RuntimeError:
    logger.info('ignore runtime error exit')
    pass
