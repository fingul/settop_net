https://bitbucket.org/fingul/settop_vod/src 참고


- "fileType": "VIDEO" 만 받을 수 잇도록 cms에서 적용
- 다운로드 요청 API 호출

  - 전달할것 :
    - settop id
    - contentId

- 완료 시 콜백은 생략
- 완료 결과 확인

  "contentId": "000000010793",
  "fileSize": 38103729,
  filePath => name => ex: 10965.MP4

- 업로드 경로 : /home/m/settop/media/vod
- 업로드 중에는 확장명을 .tmp 를 붙인다.
  - 예
    - /home/m/settop/media/vod/10965.MP4.tmp
- 업로드 완료 후에는 원래 확장명으로 변경한다.
  - 예
    -/home/m/settop/media/vod/10965.MP4



{
  "hitCnt": 0,
  "subject": "13123",
  "filePath": "\\\\192.168.1.110\\ShareFolder\\contents\\storage/proxy/video/2020/03/27/10965.MP4",
  "officeCode": "",
  "contentId": "000000010793",
  "useperiodStartdate": "2020-01-01 00:00:00.0000000",
  "content": "1231",
  "repImage": "http://192.168.1.110:9990/rest/video/thumbnail/000000010793",
  "regActDt": "2020-03-27 10:39:03.8500000",
  "useperiodEnddate": "2050-12-31 23:59:59.9990000",
  "fileSize": 38103729,
  "width": 1280,
  "regActId": "sysadmin",
  "ftpPath": "192.168.1.110\\ShareFolder\\contents\\storage/proxy/video/2020/03/27/10965.MP4",
  "contentPlaytime": "00:04:50.958",
  "keyword": "",
  "categoryId": "202002110004",
  "fileType": "VIDEO",
  "height": 720,
  "fileId": "10965",
  "imageURL": "http://25.14.29.174:9990/rest/video/thumbnail/000000010793"
}
