import logging
import time

from contexttimer import timer
from loguru import logger


class InterceptHandler(logging.Handler):
    def emit(self, record):
        logger_opt = logger.opt(depth=6, exception=record.exc_info)
        logger_opt.log(record.levelname, record.getMessage())


def setup_log():
    logging.getLogger(None).addHandler(InterceptHandler())


log_timer = timer(logger=logger, level='INFO',
                  fmt="function %(function_name)s execution time: %(execution_time).3f sec")

if __name__ == '__main__':
    
    @log_timer
    def test_sleep():
        time.sleep(0.1)


    test_sleep()
