import sys

from PyQt5 import QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QVBoxLayout, QStackedWidget, QLabel, QPushButton, QSizePolicy, \
    QMainWindow
from loguru import logger

from mod_setup_signal import setup_signal


#
# class App(QMainWindow):
#
#     def __init__(self):
#         super().__init__()
#
#         p1 =  QWidget()
#         p2 =  QWidget()
#
#
#         stackedWidget =  QStackedWidget()
#         stackedWidget.addWidget(p1)
#         stackedWidget.addWidget(p2)
#
#         layout =  QVBoxLayout()
#         layout.addWidget(stackedWidget)
#         self.setLayout(layout)
#
#
# if __name__ == '__main__':
#     os.environ['DISPLAY'] = ':0.0'
#
#     setup_signal()
#
#     # def tick():
#     #     print('tick')
#     #
#     #
#     # timer = QTimer()
#     # timer.timeout.connect(tick)
#     # timer.start(1000)
#
#     app = QApplication(sys.argv)
#
#     # disable show scrollbar
#     settings = QWebEngineSettings.globalSettings()
#     settings.setAttribute(QWebEngineSettings.ShowScrollBars, False)
#
#     ex = App()
#     app.exec()
# import sys
# from PyQt5.QtWidgets import (QWidget, QPushButton,
#                              QHBoxLayout, QVBoxLayout, QApplication)
#
#
# class Example(QWidget):
#
#     def __init__(self):
#         super().__init__()
#
#         self.initUI()
#
#     def initUI(self):
#         okButton = QPushButton("OK")
#         cancelButton = QPushButton("Cancel")
#
#         # hbox = QHBoxLayout()
#         # hbox.addStretch(1)
#         # hbox.addWidget(okButton)
#         # hbox.addWidget(cancelButton)
#
#         vbox = QVBoxLayout()
#         vbox.addStretch(1)
#         vbox.addWidget(okButton)
#         vbox.addWidget(cancelButton)
#
#         self.setLayout(vbox)
#
#         self.setGeometry(300, 300, 300, 150)
#         self.setWindowTitle('Buttons')
#         self.show()
#
#
# if __name__ == '__main__':
#     setup_signal()
#     app = QApplication(sys.argv)
#     ex = Example()
#     sys.exit(app.exec_())
class Main(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = "PyQt5 StackedWidget"
        self.top = 200
        self.left = 500
        self.width = 300
        self.height = 200
        # self.setWindowIcon(QtGui.QIcon("icon.png"))
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.init_ui()
        self.show()

    def init_ui(self):
        vbox = QVBoxLayout()
        self.stackedWidget = QStackedWidget()
        # self.stackedWidget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        # vbox.addWidget(self.stackedWidget)
        # vbox.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding))
        for x in range(0, 8):
            label = QLabel("Stacked Child: " + str(x))
            label.setFont(QtGui.QFont("sanserif", 15))
            label.setStyleSheet('color:red')
            label.setStyleSheet('background-color:blue')
            self.stackedWidget.addWidget(label)
            # self.button = QPushButton("Stack" + str(x))
            # self.button.setStyleSheet('background-color:green')
            # self.button.page = x
            # self.button.clicked.connect(self.btn_clicked)
            # vbox.addWidget(self.button)
        # self.setLayout(vbox)

        self.setCentralWidget(self.stackedWidget)

    def keyPressEvent(self, e):
        logger.info(f'keyPressEvent : e={e} | e.key()={e.key()}| e.text()={e.text()}')
        text = e.text()
        try:
            n = int(text)
            self.stackedWidget.setCurrentIndex(n)
        except:
            pass
        # if text == '1':


    def btn_clicked(self):
        self.button = self.sender()
        self.stackedWidget.setCurrentIndex(self.button.page - 1)


if __name__ == '__main__':
    setup_signal()
    App = QApplication(sys.argv)
    window = Main()
    sys.exit(App.exec())
