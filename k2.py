import asyncio
import sys

import uvicorn
from PyQt5.QtCore import QThread, pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QApplication
from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware


class Worker(QThread):
    sec_changed = pyqtSignal(str)

    def __init__(self, sec=0, parent=None):
        super().__init__()
        self.main = parent
        self.working = True
        self.sec = sec

        # self.main.add_sec_signal.connect(self.add_sec)   # 이것도 작동함. # custom signal from main thread to worker thread

    def __del__(self):
        print(".... end thread.....")
        self.wait()

    def run(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)
        app = FastAPI()
        # app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=ALL_METHODS)
        app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=['*'])

        @app.get("/")
        async def read_root(q: str = None):
            return {"Hello": "World"}

        uvicorn.run(app, host='0.0.0.0', port=8000)

        # while self.working:
        #     logger.info("run")
        #
        #     self.sec_changed.emit('time (secs)：{}'.format(self.sec))
        #     self.sleep(1)
        #     self.sec += 1

    @pyqtSlot()
    def add_sec(self):
        print("add_sec....")
        self.sec += 100

    @pyqtSlot("PyQt_PyObject")  # @pyqtSlot(object) 도 가능..
    def recive_instance_singal(self, inst):
        print(inst.name)


a = QApplication(sys.argv)
t = Worker()
t.start()
a.exec()
