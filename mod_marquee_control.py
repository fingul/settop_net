import functools

from PyQt5.QtCore import Qt, QTimer
# from PyQt5.QtGui import QLabel, QPainter, QApplication, QVBoxLayout, QFormLayout, QLineEdit, QSlider, \
#     QHBoxLayout, QWidget, QPushButton, QRadioButton, QFontDialog, QColorDialog, QPalette
from PyQt5.QtGui import QPainter, QPalette, QFont
from PyQt5.QtWidgets import QLabel, QApplication, QVBoxLayout, QFormLayout, QLineEdit, QSlider, \
    QHBoxLayout, QWidget, QPushButton, QRadioButton, QFontDialog, QColorDialog, QLCDNumber
from loguru import logger

from mod_setup_signal import setup_signal


class MarqueeLabel(QLabel):
    def __init__(self, parent=None):

        QLabel.__init__(self, parent)

        sample_palette = QPalette()
        sample_palette.setColor(QPalette.Window, Qt.black)
        sample_palette.setColor(QPalette.WindowText, Qt.white)

        # sample_palette.setColor(QPalette::Window, Qt::white);
        # sample_palette.setColor(QPalette::WindowText, Qt::blue);

        font = QFont()
        font.fromString('Nanum Gothic,48,-1,5,75,0,0,0,0,0,Bold')
        # font    .fromString('Nanum Gothic,36,-1,5,81,0,0,0,0,0,ExtraBold')
        self.setFont(font)

        self.setAutoFillBackground(True);
        self.setPalette(sample_palette);

        self.px = 0
        self.py = 14
        self._direction = Qt.LeftToRight
        self.setWordWrap(True)
        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update)
        self.timer.start(40)
        self._speed = 2
        self.textLength = 0
        self.fontPointSize = 0
        self.setAlignment(Qt.AlignVCenter)
        self.setFixedHeight(self.fontMetrics().height() + 15)

    def setFont(self, font):
        QLabel.setFont(self, font)
        self.setFixedHeight(self.fontMetrics().height() + 15)

    def updateCoordinates(self):
        align = self.alignment()
        if align == Qt.AlignTop:
            self.py = 10
        elif align == Qt.AlignBottom:
            self.py = self.height() - 10
        elif align == Qt.AlignVCenter:
            self.py = self.height() / 2
        self.fontPointSize = self.font().pointSize() / 2
        self.textLength = self.fontMetrics().width(self.text())

    def setAlignment(self, alignment):
        self.updateCoordinates()
        QLabel.setAlignment(self, alignment)

    def resizeEvent(self, event):
        self.updateCoordinates()
        QLabel.resizeEvent(self, event)

    def paintEvent(self, event):
        painter = QPainter(self)
        if self._direction == Qt.RightToLeft:
            self.px -= self.speed()
            if self.px <= -self.textLength:
                self.px = self.width()
        else:
            self.px += self.speed()
            if self.px >= self.width():
                self.px = -self.textLength
        painter.drawText(self.px, self.py + self.fontPointSize, self.text())
        painter.translate(self.px, 0)

    def speed(self):
        return self._speed

    def setSpeed(self, speed):
        self._speed = speed

    def setDirection(self, direction):
        self._direction = direction
        if self._direction == Qt.RightToLeft:
            self.px = self.width() - self.textLength
        else:
            self.px = 0
        self.update()

    def pause(self):
        self.timer.stop()

    def unpause(self):
        self.timer.start()


class Example(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setWindowTitle("Marquee Effect")
        self.setLayout(QVBoxLayout())
        self.marqueeLabel = MarqueeLabel(self)
        flayout = QFormLayout()
        self.layout().addLayout(flayout)

        le = QLineEdit(self)
        le.textChanged.connect(self.marqueeLabel.setText)
        le.setText("""아들아 너에게는 계획이 다 있었구나""")

        speed = QLCDNumber(self)


        slider = QSlider(Qt.Horizontal, self)
        slider.valueChanged.connect(self.marqueeLabel.setSpeed)
        slider.valueChanged.connect(speed.display)
        slider.setValue(10)

        # speed.valueChanged.connect(speed.setText)
        # le.setText("""아들아 너에게는 계획이 다 있었구나""")



        self.marqueeLabel.setDirection(Qt.RightToLeft)

        rtl = QRadioButton("Right to Left", self)
        ltr = QRadioButton("Left to Rigth", self)
        rtl.setChecked(True)
        rtl.toggled.connect(lambda state: self.marqueeLabel.setDirection(Qt.RightToLeft if state else Qt.LeftToRight))

        directionWidget = QWidget(self)
        directionWidget.setLayout(QHBoxLayout())
        directionWidget.layout().setContentsMargins(0, 0, 0, 0)
        directionWidget.layout().addWidget(rtl)
        directionWidget.layout().addWidget(ltr)
        fontBtn = QPushButton("Font...", self)
        fontBtn.clicked.connect(self.changeFont)
        colorBtn = QPushButton("Color...", self)
        colorBtn.clicked.connect(self.changeColor)
        pauseBtn = QPushButton("Pause", self)
        pauseBtn.setCheckable(True)
        pauseBtn.toggled.connect(lambda state: self.marqueeLabel.pause() if state else self.marqueeLabel.unpause())
        pauseBtn.toggled.connect(lambda state: pauseBtn.setText("Resume") if state else pauseBtn.setText("Pause"))

        flayout.addRow("Change Text", le)
        flayout.addRow("Change Speed", slider)
        flayout.addRow("Current Speed", speed)
        flayout.addRow("Direction", directionWidget)
        flayout.addRow("fontBtn", fontBtn)
        flayout.addRow("colorBtn", colorBtn)
        flayout.addRow("Animation", pauseBtn)
        self.layout().addWidget(self.marqueeLabel)

    def changeColor(self):
        palette = self.marqueeLabel.palette()
        color = QColorDialog.getColor(palette.brush(QPalette.WindowText).color(), self)
        if color.isValid():
            palette.setBrush(QPalette.WindowText, color)
            self.marqueeLabel.setPalette(palette)

    def changeFont(self):
        font: QFont
        font, ok = QFontDialog.getFont(self.marqueeLabel.font(), self)
        logger.debug(f"font={font.toString()}")

        if ok:
            self.marqueeLabel.setFont(font)


if __name__ == '__main__':
    import sys

    setup_signal()

    app = QApplication(sys.argv)

    # j = QJsonDocument(dict(a=font))
    # pass

    # font.pointSize()
    # logger.info(f'QJsonValue(font).toString()={QJsonValue(font).toString()}')

    w = Example()
    w.show()
    sys.exit(app.exec_())
