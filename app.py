# import asyncio
import asyncio
import datetime
import multiprocessing
import sys
import time
from functools import reduce
from pathlib import Path
from typing import Dict, List, Optional

from PyQt5 import QtGui, QtWidgets
from PyQt5.QtCore import QObject, QThread, pyqtSignal, QMargins
from PyQt5.QtCore import QUrl, Qt, QRect
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtWidgets import QWidget, QLabel, QStackedWidget
from loguru import logger
from pandas.compat import is_platform_mac

import aioprocessing
from InfinitePlayer import IPlayer
from mod_get_mac import NicInfos
from mod_setup_signal import setup_signal
from models import DataLiveOn, DataStbLiveOff
from models_file import Layout, RegionTypeEnum, PlayItem, ContentTypeEnum, TVOnOffTime, TicketURLEnum, \
    URL_CONTENTS_TYPES
from scheduler import Plan, FireItem, FireItemEnum, FullPlayItem
from ticket_server import QTicketTop, QTicketRight, TicketManager, set_background_color
from tool_make_rect import get_sample_layouts
# from app import App
from ws_client import SettopApp, client_loop, QItemSchedule, QItemLiveOn, QItemLiveOff

# result_name_rect={'top': PyQt5.QtCore.QRect(0, 0, 1920, 180), 'main': PyQt5.QtCore.QRect(0, 180, 1440, 720), 'bottom': PyQt5.QtCore.QRect(0, 900, 1440, 180), 'side': PyQt5.QtCore.QRect(1440, 180, 480, 900)}

name_to_color = dict(
    main='grey',
    side='blue',
    bottom='green',
    top='orange',
)

PROPERTY_NAME_TICKETMANAGER = 'TicketManager'


def get_ticket_manger() -> TicketManager:
    return QApplication.instance().property(PROPERTY_NAME_TICKETMANAGER)


class LiveWidget(QWidget):

    def __init__(self, parent: QWidget = None) -> None:
        super().__init__(parent)
        self.player = IPlayer()
        self.vboxlayout = QtWidgets.QVBoxLayout()
        self.vboxlayout.addWidget(self.player.videoframe)
        self.vboxlayout.setContentsMargins(QMargins())
        self.setLayout(self.vboxlayout)

    def play(self, url: str):
        self.player.open_file(url)


class AnyCon(QStackedWidget):

    def __init__(self, parent: QWidget, layout: Layout) -> None:

        logger.debug(f'anycon:layout={layout}')

        super().__init__(parent)

        rect: QRect = layout.rect.qrect

        self.setGeometry(rect)

        self.is_support_video = layout.type == RegionTypeEnum.MAIN

        defaultContent = layout.defaultContent

        self.defaultPlayItem = PlayItem(no=-1, title='', type=defaultContent.type, fileName=defaultContent.file,
                                        start=datetime.time(0, 0, 0),
                                        end=datetime.time(0, 0, 0))

        ###########################
        # ticket top
        ###########################

        if layout.type == RegionTypeEnum.TOP:
            self.ticket_top = QTicketTop(parent=self)
            self.addWidget(self.ticket_top)
        else:
            self.ticket_top = None

        ###########################
        # ticket right
        ###########################

        if layout.type == RegionTypeEnum.SIDE:
            self.ticket_right = QTicketRight(parent=self)
            self.addWidget(self.ticket_right)
        else:
            self.ticket_right = None

        ###########################
        # video
        ###########################
        self.player = None
        if self.is_support_video:
            self.player = IPlayer()
            self.video = self.player.videoframe
            self.addWidget(self.video)

        ###########################
        # web
        ###########################
        w = QWebEngineView(parent=self)

        # disable key input
        w.setEnabled(False)
        # w.setAlignment(Qt.AlignCenter)
        w.setGeometry(rect)
        # w.setUrl(QUrl('http://google.com'))
        w.setHtml('hi!')

        self.web = w
        self.addWidget(w)

        ###########################
        # label
        ###########################

        label = f'{layout.name}|type={layout.type}'

        w = QLabel(label, parent=self)
        w.setAlignment(Qt.AlignCenter)
        w.setGeometry(rect)
        # w.setStyleSheet(f"background: green")

        color = name_to_color.get(layout.name, 'red')
        logger.debug(f'color={color}')

        w.setStyleSheet(f"background: {color}")

        self.label = w
        self.addWidget(w)

        # self.setCurrentWidget(self.label)
        self.set_playItem(self.defaultPlayItem)

    def check_path(self, path: Path, size: int = 0) -> bool:

        size_ok = None
        exist = path.exists()
        if exist:

            if size:
                size_ok = path.stat().st_size == size
            else:
                logger.info(f"path={path}|SKIP SIZE CHECK")
                size_ok = True
        result = exist and size_ok

        logger.debug(f"check_path:path={path}|result={result}|exist={exist}|size_ok={size_ok}")
        return result

        # if path.exists():
        #     if size:
        #         if path.stat().st_size == size:
        #             return True
        #         else:
        #             self.set_text(f'다운로드 중:{path}')
        #     else:
        #         return True
        # else:
        #     self.set_text(f'다운로드 대기 중:{path}')

    def set_playItem(self, playItem: PlayItem, start_time_second: float = 0.0):

        logger.info(f'set_playItem : playItem={playItem}')

        if not playItem:
            playItem = self.defaultPlayItem

        if playItem.type == ContentTypeEnum.VIDEO:
            path = Path('~/settop/media/epg').joinpath(playItem.fileName).expanduser()
            if self.check_path(path, playItem.fileSize):
                self.set_video(path, start_time_second=start_time_second)
            else:
                self.set_text(f'다운로드 중 : playItem={playItem}')

        elif playItem.type == ContentTypeEnum.IMAGE:
            path = Path('~/settop/media/epg').joinpath(playItem.fileName).expanduser()
            if self.check_path(path, playItem.fileSize):
                self.set_image(path)
            else:
                self.set_text(f'다운로드 중 : playItem={playItem}')

        elif playItem.type == ContentTypeEnum.TEXT:
            text = playItem.fileName
            self.set_text(text)
        elif playItem.type == ContentTypeEnum.APP_TICKET:
            url = playItem.fileName
            if url == TicketURLEnum.URL_TICKET_TOP.value:
                self.set_ticket(TicketURLEnum.URL_TICKET_TOP)
            elif url == TicketURLEnum.URL_TICKET_RIGHT.value:
                self.set_ticket(TicketURLEnum.URL_TICKET_RIGHT)
            else:
                self.set_text(f'ticket url 이상 : 알수없는형식:{playItem}')
        elif playItem.type in URL_CONTENTS_TYPES:
            url = playItem.fileName
            self.set_url(url)
        else:
            self.set_text(f'알수없는형식:{playItem}')

    def video_stop(self):
        if self.player:
            self.player.stop()

    def set_ticket(self, ticket_url: TicketURLEnum):
        self.video_stop()

        ticket_widget = None
        if ticket_url == TicketURLEnum.URL_TICKET_TOP:
            if self.ticket_top:
                ticket_widget = self.ticket_top
        elif ticket_url == TicketURLEnum.URL_TICKET_RIGHT:
            if self.ticket_right:
                ticket_widget = self.ticket_right

        if ticket_widget:
            ticket_manager = get_ticket_manger()
            ticket_manager.on_ticket_data.connect(ticket_widget.set_ticket_data)
            ticket_widget.set_ticket_data(ticket_manager.get_ticket_data())
            self.setCurrentWidget(ticket_widget)
        else:
            self.set_text(f"{ticket_url} 위젯 없음")

    def set_video(self, path: Path, start_time_second: float = 0.0):

        if self.is_support_video:
            self.setCurrentWidget(self.video)
            self.video_stop()
            self.player.open_file(str(path), start_time_second=start_time_second)
        else:
            logger.warning("video not supported")

    def set_text(self, text: str):
        self.video_stop()
        self.setCurrentWidget(self.label)
        self.label.setText(text)

    def set_image(self, path: Path):
        self.video_stop()
        # pixmap = QPixmap(str(path))
        # self.label.setScaledContents(False)
        # self.label.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)

        self.label.setPixmap(QPixmap.fromImage(QImage(str(path))))
        self.setCurrentWidget(self.label)

    def set_url(self, url: str = 'about:blank'):
        self.video_stop()
        self.setCurrentWidget(self.web)
        self.web.setUrl(QUrl(url))


class LayoutManager(QObject):

    def __init__(self, layouts: List[Layout]):
        super().__init__()
        self.layouts = {layout.name: layout for layout in layouts}
        self.anycons: Dict[str, AnyCon] = {}

    def __str__(self):
        return f'LayoutManager({self.layouts})'

    def init_ui(self, parent: QWidget):
        parent.setGeometry(self.get_boundary_rect())

        layout: Layout
        for layout in self.layouts.values():
            name = layout.name
            self.anycons[name] = AnyCon(parent, layout)

    def set_play_item(self, name, play_item: PlayItem = None):

        anycon: AnyCon = self.anycons.get(name)
        if anycon:
            anycon.set_playItem(play_item)
        else:
            logger.error("fail find anycon : name={name}")

    def set_full_play_item(self, fullPlayItem: FullPlayItem):

        play_item: PlayItem = fullPlayItem.playItem
        name = fullPlayItem.layoutName
        start_time_second = fullPlayItem.start_second

        logger.error(f">>>>>>>>>>>START_TIME_SECOND={start_time_second}")

        anycon: AnyCon = self.anycons.get(name)
        if anycon:
            anycon.set_playItem(play_item, start_time_second=start_time_second)
        else:
            logger.error(f"fail find anycon : name={name}")

    def get_boundary_rect(self) -> QRect:
        qrects = [layout.rect.qrect for layout in self.layouts.values()]

        boundary_rect = reduce(lambda x, y: x.united(y), qrects)
        logger.info(f'boundary_rect={boundary_rect}')
        return boundary_rect


# logger.info(f'lm={lm}')

class MainWindow(QMainWindow):

    def __init__(self):
        super().__init__()
        self.plan = None
        self.schedules = None

        self.stackedWidget = QStackedWidget()
        self.setCentralWidget(self.stackedWidget)

        ##################
        # LABEL
        ##################
        label = QLabel('수신 대기 중입니다.')
        label.setAlignment(Qt.AlignCenter)
        # label.setFont(QtGui.QFont("sanserif", 15))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        font.setPointSize(70)
        label.setFont(font)
        label.setStyleSheet('background-color:black;color:white')
        self.labelWidget = label

        self.stackedWidget.addWidget(self.labelWidget)

        ##################
        # SCHEDULE
        ##################

        self.scheduleWidget = QWidget()
        self.stackedWidget.addWidget(self.scheduleWidget)
        self.lm: Optional[LayoutManager] = None

        self.liveWidget: LiveWidget = None
        # self.stackedWidget.addWidget(self.liveWidget)

        logger.debug('init app')

        if is_platform_mac():
            self.show()
        else:
            self.showFullScreen()

    def show_status(self, text: str):
        self.labelWidget.setText(text)
        self.stackedWidget.setCurrentWidget(self.labelWidget)

    def close_plan(self):
        if self.plan:
            self.plan.close()
            self.plan = None

    def close_liveWidget(self):
        if self.liveWidget:
            self.liveWidget.deleteLater()
            self.liveWidget = None

    def run_live(self, url: str):
        self.close_plan()
        self.removeScheduleWidget()
        self.close_liveWidget()

        self.liveWidget: LiveWidget = LiveWidget()
        self.liveWidget.play(url=url)
        self.stackedWidget.addWidget(self.liveWidget)
        self.stackedWidget.setCurrentWidget(self.liveWidget)

    def run_schedule(self):
        logger.debug(f"self.schedules={self.schedules}")
        self.close_liveWidget()
        if not self.schedules:
            self.show_status("스케쥴이 없습니다.")
            return
        self.close_plan()
        self.plan = Plan(self.schedules)
        self.plan.on_fire.connect(self.on_fired)
        nplay: int = self.plan.play()
        if not nplay:
            self.show_status("스케쥴이 없습니다.")

    @pyqtSlot(object)
    def message(self, msg):
        logger.info(f'MainWindow:msg={msg}')
        if isinstance(msg, QItemSchedule):
            self.schedules = msg.schedules
            logger.debug(f'self.schedules={self.schedules}')
            self.run_schedule()
        elif isinstance(msg, QItemLiveOn):
            url = msg.dataLiveOn.url
            self.run_live(url)
        elif isinstance(msg, QItemLiveOff):
            self.run_schedule()

    @pyqtSlot(object)
    def on_fired(self, fireItem: FireItem):
        logger.info(f'>>on_fired={fireItem}')

        if fireItem.type == FireItemEnum.LAYOUTS:
            layouts: List[Layout] = fireItem.item
            logger.debug("begin buildLayouts")
            self.buildLayouts(layouts)
            logger.debug("done buildLayouts")
        if fireItem.type == FireItemEnum.FULLPLAYITEM:
            fullPlayItem: FullPlayItem = fireItem.item
            if self.lm:
                self.lm.set_full_play_item(fullPlayItem)
        if fireItem.type == FireItemEnum.TVONOFF:
            tvOnOfftime: TVOnOffTime = fireItem.item

    def removeScheduleWidget(self):
        if self.scheduleWidget:
            if self.lm:
                del self.lm
                self.lm = None
            self.stackedWidget.removeWidget(self.scheduleWidget)
            self.scheduleWidget.deleteLater()
            self.scheduleWidget = None

    def buildLayouts(self, layouts: List[Layout]):
        self.removeScheduleWidget()
        self.scheduleWidget = QWidget()
        set_background_color(self.scheduleWidget)
        self.stackedWidget.addWidget(self.scheduleWidget)

        # self.title = 'APP_TITLE'

        self.lm = LayoutManager(layouts=layouts)
        self.lm.init_ui(self.scheduleWidget)
        self.stackedWidget.setCurrentWidget(self.scheduleWidget)

    def keyPressEvent(self, e):
        logger.info(f'keyPressEvent : e={e} | e.key()={e.key()}| e.text()={e.text()}')
        text = e.text()

        if text == '.':
            self.message(QItemLiveOn(dataLiveOn=DataLiveOn(url="udp://@239.0.0.1:1234")))
        if text == '/':
            self.message(QItemLiveOff(dataLiveOff=DataStbLiveOff()))
        if text == '1':
            self.removeScheduleWidget()
            self.stackedWidget.setCurrentIndex(0)
        if text == '2':
            self.run_schedule()
        if text == '3':
            layouts = get_sample_layouts()
            self.buildLayouts(layouts)

        if text == 'f':
            if self.isFullScreen():
                self.showNormal()
            else:
                self.showFullScreen()
        return

        if text == '1':
            # self.lm.set_play_item("main", PlayItem(type=ContentTypeEnum.IMAGE, fileName="0.jpg"))

            p = PlayItem(no=-1, title='', type=ContentTypeEnum.IMAGE, fileName="0.jpg",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("top", play_item=p)

            # self.region_manager.set_content('main', ImageContent(path='~/0.png'))
        elif text == '2':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.VIDEO, fileName="10217.mp4",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '3':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.VIDEO, fileName="10216.mp4",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '4':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.VIDEO, fileName="60.mp4",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '5':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.URL, fileName="http://naver.com",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("bottom", play_item=p)
        elif text == '6':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.URL, fileName="https://noonnu.cc/",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)
        elif text == '7':
            p = PlayItem(no=-1, title='', type=ContentTypeEnum.TEXT, fileName="가나다라마바사",
                         start=datetime.time(0, 0, 0),
                         end=datetime.time(0, 0, 0))

            self.lm.set_play_item("main", play_item=p)

        # elif text == 'q':
        #     restart_app()

        # QtCore.QTimer.singleShot(0, self.after)

    # def after(self):
    #     logger.info('boom')
    #     # self.showFullScreen()


class WebSocketThread(QThread):
    on_message = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.done = False
        self.q_in = aioprocessing.AioQueue()
        self.q_out = aioprocessing.AioQueue()
        self.process = multiprocessing.Process(target=WebSocketThread.start_web_process, args=(self.q_in, self.q_out))
        self.process.start()

    def run(self):

        logger.info(f'Web::run begin')
        while not self.done:

            time.sleep(.01)
            message = self.q_out.get()
            try:
                logger.info(f'Web::run message={message}')
                self.on_message.emit(message)
            except Exception as e:
                logger.exception("error while Web::run")

    @pyqtSlot()
    def close(self):
        self.done = True
        if self.process:
            logger.debug("kill WebSocketThread")
            self.process.kill()
            self.process = None
            logger.debug("kill WebSocketThread done")

    def __del__(self):
        self.close()

    def start_web_process(q_in: aioprocessing.AioQueue, q_out: aioprocessing.AioQueue):

        DEV = is_platform_mac()

        # DEFAULT_ID = '2c:94:64:02:e1:5c'

        if DEV:
            DEFAULT_ID = '2c:94:64:02:e1:62'
            DEFAULT_URL = 'ws://castware.cf:8000/ws/client'
            # DEFAULT_URL = 'ws://127.0.0.1:8000/ws/client'
            # DEFAULT_URL = 'ws://127.0.0.1:8000/ws/client'
        else:
            DEFAULT_ID = NicInfos.load().device_id
            DEFAULT_URL = 'ws://172.18.141.23:8000/ws/client'
            # DEFAULT_URL = 'ws://127.0.0.1:8000/ws/client'
        # DEFAULT_URL = 'ws://castware.cf:8000/ws/client'
        DEFAULT_URL = 'ws://127.0.0.1:8000/ws/client'

        DEFAULT_URL = 'ws://172.18.141.23:8000/ws/client'
        url = DEFAULT_URL

        id = NicInfos.load().device_id

        exit_when_error = False

        # url = 'ws://127.0.0.1:8000/ws/client'
        # id = "0"

        settopapp = SettopApp(q_in=q_in, q_out=q_out)
        loop = asyncio.get_event_loop()

        while 1:
            # noinspection PyBroadException
            try:
                loop.run_until_complete(client_loop(url=url, id=id, settopapp=settopapp))
            except (SystemExit, KeyboardInterrupt):
                logger.info('stop client')
                break
            except ConnectionRefusedError:
                logger.error('connection fail')
            except Exception:
                logger.exception('error while client')

            if exit_when_error:
                break
            else:

                # 오류 발생 시 재시작
                nsleep = 5
                logger.info(f'wait {nsleep} second')
                time.sleep(nsleep)


if __name__ == '__main__':
    setup_signal()

    # def tick():
    #     print('tick')
    #
    #
    # timer = QTimer()
    # timer.timeout.connect(tick)
    # timer.start(1000)

    # app = QApplication(sys.argv)
    # window = MainWindow()
    # window.show()

    app = QApplication(sys.argv)

    # disable show scrollbar
    settings = QWebEngineSettings.globalSettings()
    settings.setAttribute(QWebEngineSettings.ShowScrollBars, False)

    #######################
    # SET TICKET MANAGER GLOBALLY
    #######################
    tm = TicketManager()
    app.setProperty(PROPERTY_NAME_TICKETMANAGER, tm)

    mainWindow = MainWindow()

    # https://stackoverflow.com/a/6789205
    web = WebSocketThread()
    web.finished.connect(app.exit)
    web.on_message.connect(mainWindow.message)
    web.start()

    app.exec()

    web.close()
