from typing import List

from loguru import logger

from config.net._nmcli import getEthernetInterface, getInterfaceDetail
from config.net.model import NetStatus
from config.net.share import is_macos


def get_net_statuses() -> List[NetStatus]:
    interfaces = getEthernetInterface().keys()
    net_statuses: List[NetStatus] = []
    for interface in interfaces:
        detail = getInterfaceDetail(interface)
        # logger.info(f'interface={interface}|detail={detail}')
        net_status = NetStatus.load(interface=interface, detail=detail)
        net_statuses.append(net_status)
        # logger.info(f'i={i}')
    return net_statuses


def get_macs() -> List[str]:
    net_statuses = get_net_statuses()
    macs = [net_status.mac for net_status in net_statuses]
    return macs


def get_device_id() -> str:
    if is_macos():
        fake_device_id = '2c:94:64:02:a6:3a'
        logger.debug(f'return fake device_id', fake_device_id)
        return fake_device_id

    # 주어진 맥주소 : ['1C:1B:0D:1E:AB:C2', '00:50:56:C0:00:01', '00:50:56:C0:00:08']
    # 대표 맥주소 : 00:50:56:c0:00:01

    # macs = ['1C:1B:0D:1E:AB:C2', '00:50:56:C0:00:01', '00:50:56:C0:00:08']
    macs = get_macs()
    device_id = min(macs)

    logger.info(f'macs={macs}')
    logger.info(f'device_id={device_id}')

    return device_id


def get_net_status_str():
    net_statuses = get_net_statuses()
    macs = [net_status.mac for net_status in net_statuses]
    device_id = min(macs)

    return f'device_id={device_id} | macs={macs} | net_statuses={net_statuses}'


if __name__ == '__main__':
    net_statuses = get_net_statuses()
    logger.info(f'get_net_statuses()={net_statuses}')
    logger.info(f'get_device_id()={get_device_id()}')
