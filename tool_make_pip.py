from pathlib import Path

from loguru import logger

s = Path('./requirements.in').read_text()

lines = s.split('\n')
lines = [line.strip() for line in lines]
lines = [line.strip() for line in lines if not line.startswith('#')]
lines = [line.strip() for line in lines if line]

lines = list(set(lines))
logger.info(f'lines={lines}')

ss = 'ipython uvicorn psutil pyqt pillow pyzmq netifaces openpyxl pyserial pandas pycurl'
ss = set(list(ss.split()))

logger.info(f'set(lines)-set(ss)={ss.intersection(lines)}')

logger.info('#'*20)

logger.info('\n\n'+'\n'.join(lines))

# for i in items:
#     print(f"{i.upper()}='{i.lower()}'")
