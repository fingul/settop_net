import sys
from pathlib import Path

from PyQt5 import QtGui
from PyQt5.QtCore import QObject, pyqtSignal, QTimer, QRect, QPoint, QSize, pyqtSlot
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QColor
from PyQt5.QtNetwork import QUdpSocket, QHostAddress
from PyQt5.QtWidgets import *
from loguru import logger
from pandas.compat import is_platform_mac

from mod_setup_signal import setup_signal
from models_file import Rect
from ticket_parse import parse_msg, TicketData, get_sample_ticket_data

GLOBAL_DEV = False


class TicketManager(QObject):
    on_packet_receive = pyqtSignal()
    on_ticket_data = pyqtSignal(TicketData)

    def __init__(self, parent=None):
        super(TicketManager, self).__init__(parent)
        if GLOBAL_DEV:
            self.ticket_data = get_sample_ticket_data()

        else:
            self.ticket_data = TicketData()

        self.udpSocket = QUdpSocket(self)

        self.debounce = QTimer()
        self.debounce.setInterval(500)
        self.debounce.setSingleShot(True)
        self.debounce.timeout.connect(self.on_ticket)

        self.on_packet_receive.connect(self.debounce.start)

        port = 6101
        logger.info(f'run udp @ port={port}')

        self.udpSocket.bind(QHostAddress.AnyIPv4, port)
        self.udpSocket.readyRead.connect(self.processPendingDatagrams)

    def get_ticket_data(self) -> TicketData:
        return self.ticket_data

    def on_ticket(self):
        logger.debug(
            f'on_ticket:ticket_data={self.ticket_data.json(ensure_ascii=False, indent=2)}')
        self.on_ticket_data.emit(self.ticket_data)

    def processPendingDatagrams(self):
        while self.udpSocket.hasPendingDatagrams():

            # logger.debug("loop!!!!!!")

            data, host, port = self.udpSocket.readDatagram(self.udpSocket.pendingDatagramSize())

            # noinspection PyBroadException
            try:

                h = data.hex()

                msg = parse_msg(h)

                # logger.debug(f'msg={msg}')

                self.ticket_data.add(msg)

                self.on_packet_receive.emit()

                # logger.debug(
                #     f'h={h}|host={host}|port={port}|msg={msg}|ticket_manager={self.ticket_manager.json(ensure_ascii=False, indent=2)}')
            except Exception:
                logger.exception("error while parsing")


def create_label(parent, rect: QRect, text: str = "", fontSizePx: int = 50, color="white") -> QLabel:
    label = QLabel(parent=parent, text=text)
    label.setGeometry(rect)
    label.setAlignment(Qt.AlignCenter)

    # font = QFont()
    # font.fromString(f'Nanum Gothic,{fontSizePx},-1,5,75,0,0,0,0,0,Bold')
    # # font    .fromString('Nanum Gothic,36,-1,5,81,0,0,0,0,0,ExtraBold')
    # label.setFont(font)
    # label.setPalette()

    s_style = f'font-family:"Nanum Gothic";font-size:{fontSizePx}px;color: {color};font-weight:Bold;'
    if GLOBAL_DEV:
        s_style += "border-style: solid;border-width: 2px;border-color: #FA8072;border-radius: 3px"

    label.setStyleSheet(
        s_style
    )
    return label


class QTicketTop(QLabel):

    def __init__(self, parent, ticket_data: TicketData = None, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        if not ticket_data:
            ticket_data = TicketData()

        pathImage = Path(__file__).parent.joinpath('images/ticket_top.png')
        pixmap = QPixmap(str(pathImage))
        self.setPixmap(pixmap)
        self.label_msg = create_label(parent=self,
                                      rect=QRect(QPoint(0, 0), QSize(1363, 190)),
                                      fontSizePx=60, text=ticket_data.msg)

        rect = QRect(QPoint(0, 0), pixmap.size())

        logger.info(f"QTicketTop:qrect={rect}:rect={Rect.fromQRect(rect)}")
        self.setGeometry(rect)

        self.set_ticket_data(ticket_data)

    @pyqtSlot(TicketData)
    def set_ticket_data(self, ticket_data: TicketData):
        self.label_msg.setText(ticket_data.msg)


class QTicketRight(QLabel):

    def __init__(self, parent, ticket_data: TicketData = None, *args, **kwargs):
        if not ticket_data:
            ticket_data = TicketData()
        super().__init__(parent, *args, **kwargs)

        rectAll = QRect(QPoint(0, 0), QSize(1920, 1080))

        pathImage = Path(__file__).parent.joinpath('images/ticket_right.png')
        pixmap = QPixmap(str(pathImage))
        self.setPixmap(pixmap)

        rect = QRect(
            QPoint(rectAll.width() - pixmap.size().width(), rectAll.height() - pixmap.size().height()),
            pixmap.size())

        self.setGeometry(rect)

        logger.info(f"QTicketRight:qrect={rect}:rect={Rect.fromQRect(rect)}")

        # 영업점 이름
        self.label_store = create_label(self, QRect(QPoint(32, 25), QSize(481, 74)), fontSizePx=30, color="#091f34")

        # 대기인수
        self.label_wait = create_label(self, QRect(QPoint(284, 122), QSize(158, 86)))

        # 창구 번호
        self.label_gate_name0 = create_label(self, QRect(QPoint(63, 342), QSize(117, 74)))
        self.label_gate_name1 = create_label(self, QRect(QPoint(63, 468), QSize(117, 68)))
        self.label_gate_name2 = create_label(self, QRect(QPoint(63, 591), QSize(118, 69)))
        self.label_gate_name3 = create_label(self, QRect(QPoint(63, 714), QSize(118, 73)))

        # 창구 내용
        self.label_gate_call0 = create_label(self, QRect(QPoint(284, 342), QSize(202, 74)))
        self.label_gate_call1 = create_label(self, QRect(QPoint(284, 467), QSize(202, 69)))
        self.label_gate_call2 = create_label(self, QRect(QPoint(284, 589), QSize(202, 71)))
        self.label_gate_call3 = create_label(self, QRect(QPoint(284, 713), QSize(202, 74)))

        self.set_ticket_data(ticket_data)

    @pyqtSlot(TicketData)
    def set_ticket_data(self, ticket_data: TicketData):
        # 영업점 이름
        self.label_store.setText(ticket_data.store)

        # 대기인수
        self.label_wait.setText(ticket_data.wait)

        # 창구 번호
        self.label_gate_name0.setText(ticket_data.gates[0].name)
        self.label_gate_name1.setText(ticket_data.gates[1].name)
        self.label_gate_name2.setText(ticket_data.gates[2].name)
        self.label_gate_name3.setText(ticket_data.gates[3].name)

        # 창구 내용
        self.label_gate_call0.setText(ticket_data.gates[0].call)
        self.label_gate_call1.setText(ticket_data.gates[1].call)
        self.label_gate_call2.setText(ticket_data.gates[2].call)
        self.label_gate_call3.setText(ticket_data.gates[3].call)


def set_background_color(widget: QWidget, qcolor: QColor = QtGui.QColor(49, 75, 102)):
    widget.palette = widget.palette()
    widget.palette.setColor(QtGui.QPalette.Window, qcolor)
    widget.setPalette(widget.palette)
    widget.setAutoFillBackground(True)


class MainWindow(QWidget):

    def keyPressEvent(self, e):
        logger.info(f'keyPressEvent : e={e} | e.key()={e.key()}| e.text()={e.text()}')
        text = e.text()

    def set_ticket_manager(self, ticket_manager: TicketManager):
        self.ticket_manager = ticket_manager

        self.ticket_manager.on_ticket_data.connect(self.ticket_top.set_ticket_data)
        self.ticket_top.set_ticket_data(self.ticket_manager.get_ticket_data())

        self.ticket_manager.on_ticket_data.connect(self.ticket_right.set_ticket_data)
        self.ticket_right.set_ticket_data(self.ticket_manager.get_ticket_data())

    def __init__(self):
        QWidget.__init__(self)

        set_background_color(self)

        # # color = QColor.fromRgb(49, 75, 102)
        # # pallette.setColor(color)
        # self.palette.setColor(QtGui.QPalette.Window, QtGui.QColor(0, 0, 0))
        # self.setAutoFillBackground(True)
        # self.setPalette(pallette)

        self.ticket_top = QTicketTop(parent=self)
        self.ticket_right = QTicketRight(parent=self)

        self.ticket_manager = None

        rectAll = QRect(QPoint(0, 0), QSize(1920, 1080))

        self.setGeometry(rectAll)

        logger.debug(f'rectAll={rectAll}')
        ########################
        # 샘플 비디오 좌표
        ########################
        mayBeVideoRect = rectAll

        mayBeVideoRect.setTop(self.ticket_top.height())
        mayBeVideoRect.setWidth(mayBeVideoRect.width() - self.ticket_right.width())

        logger.debug(f'mayBeVideoRect={mayBeVideoRect}')

        logger.info(f"mayBeVideoRect:qrect={mayBeVideoRect}:rect={Rect.fromQRect(mayBeVideoRect)}")

        ########################

        if is_platform_mac():
            self.show()
        else:
            self.showFullScreen()


if __name__ == "__main__":
    setup_signal()
    app = QApplication(sys.argv)
    tm = TicketManager()
    m = MainWindow()
    m.set_ticket_manager(tm)
    app.exec()
