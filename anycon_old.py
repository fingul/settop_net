import asyncio
import gc
import os
import pathlib
import sys
import typing

import PyQt5
from PyQt5 import QtCore
from PyQt5.QtCore import QUrl, Qt, QRect, QCoreApplication
from PyQt5.QtGui import QPixmap
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEngineSettings
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QStackedWidget
# from app import App
from loguru import logger
from quamash import QEventLoop

from InfinitePlayer import IPlayer
from mod_setup_signal import setup_signal

class AnyCon(QStackedWidget):

    def __init__(self, parent: typing.Optional[QWidget] = ..., rect: QRect = QRect(0, 0, 0, 0)) -> None:
        super().__init__(parent)
        self.s = QStackedWidget(self)

        ###########################
        # video
        ###########################
        self.player = IPlayer()
        self.video = self.player.videoframe
        self.addWidget(self.video)

        ###########################
        # web
        ###########################
        w = QWebEngineView(parent=parent)

        # disable key input
        w.setEnabled(False)
        # w.setAlignment(Qt.AlignCenter)
        w.setGeometry(rect)
        # w.setUrl(QUrl('http://google.com'))
        w.setHtml('hi!')

        self.web = w
        self.addWidget(w)

        ###########################
        # label
        ###########################

        w = QLabel('label', parent=parent)
        w.setAlignment(Qt.AlignCenter)
        w.setGeometry(rect)
        w.setStyleSheet(f"background: green")
        self.label = w
        self.addWidget(w)

        self.setCurrentWidget(self.label)

    # def __del__(self):
    #     self.player
    #     logger.debug("__del__")
    #     pass

    def set_v(self):
        self.setCurrentWidget(self.video)
        self.player.stop()
        self.player.open_file(str(pathlib.Path('~/0.mp4').expanduser()))

    def set_i(self, s_path: str):
        self.player.stop()

        p = pathlib.Path(s_path).expanduser()
        self.setCurrentWidget(self.label)
        pixmap = QPixmap(str(p))
        # pixmap.
        # self.label.setScaledContents(False)
        self.label.setPixmap(pixmap)

    def set_u(self, url: str = 'http://google.com'):
        self.player.stop()

        # self.s.setCurrentWidget(self.web)
        # self.web.setUrl(QUrl('about:blank'))

        self.setCurrentWidget(self.web)
        self.web.setUrl(QUrl(url))
        # self.web.setHtml('hi')
